package de.jabc.cinco.meta.plugin.generator.runtime;

import de.ls5.dywa.generated.controller.info.scce.pyro.core.*;
import de.ls5.dywa.generated.entity.info.scce.pyro.core.*;
import de.ls5.dywa.generated.util.DomainFileController;
import de.ls5.dywa.generated.util.FileReference;
import graphmodel.GraphModel;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;


/**
 * Author zweihoff
 */
public abstract class IGenerator<T extends GraphModel> {

    private List<GeneratedFile> files;
    
    private PyroFolder root;
    private String basePath;
    private PyroFolderController pyroFolderController;
    private PyroFileController pyroFileController;
    private PyroBinaryFileController pyroBinaryFileController;
    private DomainFileController domainFileController;
    private PyroElementController pyroElementController;


    public IGenerator() {
        files = new LinkedList<>();
    }

    public final void generateFiles(T graphModel,PyroFolder root, String basePath, PyroFolderController pyroFolderController, PyroFileController pyroFileController, PyroBinaryFileController pyroBinaryFileController,PyroURLFileController pyroURLFileController, DomainFileController domainFileController,PyroElementController pyroElementController,String staticResourceBase,java.util.Map<String,String[]> staticResources) throws IOException {
        this.root = root;
        this.basePath = basePath;
        this.pyroFolderController = pyroFolderController;
        this.pyroFileController = pyroFileController;
        this.pyroBinaryFileController = pyroBinaryFileController;
        this.domainFileController = domainFileController;
        this.pyroElementController = pyroElementController;
        
        generate(graphModel);
        //get generation base folder
        PyroFolder generationBaseFolder = getFolder(basePath,root);
        if(generationBaseFolder==null) {
            generationBaseFolder = createFolder(basePath,root,pyroFolderController);
        }
        //copy and overwrite with static resources
        for(java.util.Map.Entry<String,String[]> staticResource:staticResources.entrySet())
        {
        	for(String url:staticResource.getValue()) {
        		
        		String file = url;
        		String folder = "";
        		String extension = "";
        		String filename = file;
        		if(file.contains("/")) {
        			file = url.substring(url.lastIndexOf("/")+1);
        			folder = url.substring(0,url.lastIndexOf("/"));
        		}

                if(file.contains(".")) {
                    extension = file.substring(file.lastIndexOf(".")+1);
                    filename = file.substring(0,file.lastIndexOf("."));
                }

        		PyroFolder pf = createFolder(folder,generationBaseFolder,pyroFolderController);
        		
        		final String fFilename = filename;
        		final String fExtension = extension;
        		
        		//delete prior file
        		Optional<PyroFile> optFile = pf.getfiles_PyroFile().stream().filter(n->n.getfilename().equals(fFilename)&&n.getextension().equals(fExtension)).findAny();
        		if(optFile.isPresent()) {
        			PyroFile pyroFile = optFile.get();
        			pf.getfiles_PyroFile().remove(pyroFile);
        			
        			if(pyroFile instanceof PyroBinaryFile) {
        				FileReference fileReference = ((PyroBinaryFile) pyroFile).getfile();
        				((PyroBinaryFile) pyroFile).setfile(null);
        				domainFileController.deleteFile(fileReference);
        			}
        			if(pyroFile instanceof PyroModelFile) {
        				if(pyroFile instanceof de.ls5.dywa.generated.entity.info.scce.pyro.core.GraphModel) {
        					removeContainer((de.ls5.dywa.generated.entity.info.scce.pyro.core.GraphModel)pyroFile);
        					pyroFileController.delete(pyroFile);
        				} else {
        					//skip ecore files
        					continue;
        				}
        			}
                    pyroFileController.deleteWithIncomingReferences(pyroFile);

        		}
        		
        		PyroURLFile pyroURLFile = pyroURLFileController.create("PyroURL_"+filename);
        		pyroURLFile.setfilename(filename);
        		pyroURLFile.seturl(staticResourceBase+"/"+staticResource.getKey()+"/"+url);
        		pyroURLFile.setextension(extension);
        		pf.getfiles_PyroFile().add(pyroURLFile);
        	}

        }
        
        //generate files and overwrite existing
        for(GeneratedFile gf:files)
        {
            final String extension = gf.getFilename().substring(gf.getFilename().lastIndexOf(".")+1);
            final String filename = gf.getFilename().substring(0,gf.getFilename().lastIndexOf("."));
            

            PyroFolder pf = createFolder(gf.getPath(),generationBaseFolder,pyroFolderController);
            //delete prior file
            Optional<PyroFile> optFile = pf.getfiles_PyroFile().stream().filter(n->n.getfilename().equals(filename)&&n.getextension().equals(extension)).findAny();
            if(optFile.isPresent()) {
                PyroFile pyroFile = optFile.get();
                pf.getfiles_PyroFile().remove(pyroFile);

                if(pyroFile instanceof PyroBinaryFile) {
                    FileReference fileReference = ((PyroBinaryFile) pyroFile).getfile();
                    ((PyroBinaryFile) pyroFile).setfile(null);
                    domainFileController.deleteFile(fileReference);
                }
                if(pyroFile instanceof PyroModelFile) {
                    if(pyroFile instanceof de.ls5.dywa.generated.entity.info.scce.pyro.core.GraphModel) {
                        removeContainer((de.ls5.dywa.generated.entity.info.scce.pyro.core.GraphModel)pyroFile);
                        pyroFileController.delete(pyroFile);
                    } else {
                        //skip ecore files
                        continue;
                    }
                }
                pyroFileController.deleteWithIncomingReferences(pyroFile);
            }
            FileReference fr = null;
            if(gf.getContent()!=null) {

                InputStream stream = new ByteArrayInputStream(gf.getContent().getBytes(StandardCharsets.UTF_8));
                fr = domainFileController.storeFile(gf.getFilename(),stream);
            } else {
                fr = domainFileController.storeFile(gf.getFilename(),new FileInputStream(gf.getFile()));
            }
            
            PyroBinaryFile pyroBinaryFile = pyroBinaryFileController.create("PyroBinary_"+filename);
            pyroBinaryFile.setfilename(filename);
            pyroBinaryFile.setfile(fr);
            pyroBinaryFile.setextension(extension);
            pf.getfiles_PyroFile().add(pyroBinaryFile);

        }
        
    }

    private void removeContainer(ModelElementContainer container) {
        container.getmodelElements_ModelElement().forEach((n)->{
            if(n instanceof Container){
                removeContainer((Container) n);
                removeNode((Container)n);
            }
            if(n instanceof Node){
                removeNode((Node) n);
            }
        });
        container.getmodelElements_ModelElement().forEach((n)->{
            if(n instanceof Edge){
                removeEdge((Edge) n);
            }
        });
        container.getmodelElements_ModelElement().clear();
        if(container instanceof Node) {
            removeNode((Node)container);
        }
    }


    private void removeNode(Node node){
        node.getincoming_Edge().forEach(e->e.settargetElement(null));
        node.getincoming_Edge().clear();
        node.getoutgoing_Edge().forEach(e->e.setsourceElement(null));
        node.getoutgoing_Edge().clear();
        node.setcontainer(null);
        pyroElementController.delete(node);

    }

    private void removeEdge(Edge edge){
        edge.setsourceElement(null);
        edge.settargetElement(null);
        edge.setcontainer(null);
        edge.getbendingPoints_BendingPoint().forEach(n->pyroElementController.delete(n));
        edge.getbendingPoints_BendingPoint().clear();
        pyroElementController.delete(edge);
    }

    public final File generateFilesTemporal(T graphModel) throws IOException {
        generate(graphModel);
        //create Files in temp directory
        String sourceBasepath = System.getProperty("java.io.tmpdir")+"/"+graphModel.getId()+"/sources/";
        String compressedBasepath = System.getProperty("java.io.tmpdir")+"/"+graphModel.getId()+"/compressed/generated.zip";
        File compressed = new File(compressedBasepath);
        compressed.getParentFile().mkdirs();
        new File(sourceBasepath).mkdirs();
        for(GeneratedFile gf:files)
        {
            File f = new File(sourceBasepath+"/"+gf.getPath()+"/"+gf.getFilename());
            f.getParentFile().mkdirs();
            f.createNewFile();

            FileUtils.writeStringToFile(f,gf.getContent());
        }
        //create Archive
        ZipUtils zu = new ZipUtils();
        zu.zip(sourceBasepath,compressed);
        //return Archive
        return compressed;
    }

    protected abstract void generate(T graphModel);

    protected final void createFile(String filename,String content) {
        createFile(filename,"",content);
    }

    protected final void createFile(String filename,File file) {
        createFile(filename,"",file);
    }

    protected final void createFile(String filename,String path,String content) {
        if(filename==null||path==null||content==null) {
            throw new IllegalStateException("All parameters has to be not null");
        }
        if(filename.isEmpty()) {
            throw new IllegalStateException("Filename has to be given");
        }
        if(content.isEmpty()) {
            content = " ";
        }
        files.add(new GeneratedFile(filename,path,content));
    }

    protected final void createFile(String filename,String path,File file) {
        if(filename==null||path==null||file==null) {
            throw new IllegalStateException("All parameters has to be not null");
        }
        if(filename.isEmpty()) {
            throw new IllegalStateException("Filename has to be given");
        }

        files.add(new GeneratedFile(filename,path,file));
    }
    
    private PyroFolder createFolder(String newFolderPath,PyroFolder folder,PyroFolderController pyroFolderController) {
        if(newFolderPath==null||newFolderPath.isEmpty()) {
            return folder;
        }
        String[] folders = newFolderPath.split("/");
        PyroFolder current = folder;
        for (String folder1 : folders) {
            if(folder1.isEmpty()) {
                continue;
            }
            Optional<PyroFolder> pf = current.getinnerFolders_PyroFolder().stream().filter(n -> n.getname().equals(folder1)).findAny();
            if (!pf.isPresent()) {
                PyroFolder newFolder = pyroFolderController.create("PyroFolder_" + folder1);
                newFolder.setname(folder1);
                current.getinnerFolders_PyroFolder().add(newFolder);
                current = newFolder;

            } else {
                current = pf.get();
            }
        }
        return current;
    }
    
    private PyroFolder getFolder(String path,PyroFolder folder) {
        String[] folders = path.split("/");
        PyroFolder current = folder;
        for (String folder1 : folders) {
            Optional<PyroFolder> pf = current.getinnerFolders_PyroFolder().stream().filter(n -> n.getname().equals(folder1)).findFirst();
            if (pf.isPresent()) {
                current = pf.get();
            } else {
                return null;
            }
        }
        return current;
    }
    
}
