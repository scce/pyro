package graphmodel;


public interface IdentifiableElement extends PyroElement{
	de.ls5.dywa.generated.entity.info.scce.pyro.core.IdentifiableElement getDelegate();
}
