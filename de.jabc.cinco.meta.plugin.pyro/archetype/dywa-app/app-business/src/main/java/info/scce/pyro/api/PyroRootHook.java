package info.scce.pyro.api;

import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroSettingsController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroStyleController;
import info.scce.pyro.IPyroController;

import java.util.Set;

/**
 * Author zweihoff
 */
public abstract class PyroRootHook extends PyroHook {

    private PyroSettingsController settingsController;
    private PyroStyleController styleController;

    public final void init(
            Set<IPyroController> bundles,
            PyroSettingsController settingsController,
            PyroStyleController styleController
                           ) {
        this.settingsController = settingsController;
        this.styleController = styleController;
        super.init(bundles);

    }

    public abstract void execute(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSettings setting);

    public PyroSettingsController getSettingsController() {
        return settingsController;
    }

    public void setSettingsController(PyroSettingsController settingsController) {
        this.settingsController = settingsController;
    }
}

