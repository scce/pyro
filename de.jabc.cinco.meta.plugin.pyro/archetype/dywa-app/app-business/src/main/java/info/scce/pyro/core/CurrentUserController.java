package info.scce.pyro.core;

import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroUserController;
import info.scce.pyro.core.rest.types.FindPyroUser;
import info.scce.pyro.core.rest.types.PyroUser;
import info.scce.pyro.util.UserUtils;

import java.util.stream.Collectors;

@javax.transaction.Transactional
@javax.ws.rs.Path("/user/current")
public class CurrentUserController {
	
	@javax.inject.Inject
	private de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroUserController subjectController;
	
	@javax.inject.Inject
	private de.ls5.dywa.generated.util.DomainFileController fileController;

	@javax.inject.Inject
	private info.scce.pyro.rest.ObjectCache objectCache;
	
	@javax.ws.rs.GET
	@javax.ws.rs.Path("private")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public javax.ws.rs.core.Response getCurrentUser() {

		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());

		if(subject!=null) {
			PyroUser result = objectCache.getRestTo(subject);
			if(result==null){
				result = PyroUser.fromDywaEntity(subject, objectCache);
			}
			return javax.ws.rs.core.Response.ok(result).build();
		}

		return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
	
	@javax.ws.rs.PUT
	@javax.ws.rs.Path("update/private")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public javax.ws.rs.core.Response update(final PyroUser user) {
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());

		if(subject != null) {
			if (user.getemail() == null || user.getemail().trim().equals("")) {
				return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.BAD_REQUEST)
						.entity("email may not be empty")
						.build(); 
			}
			
			// update email and hash
			subject.setemail(user.getemail());
			subject.setemailHash(UserUtils.createEmailHash(user.getemail()));
			
			if (user.getprofilePicture() != null) {
				final de.ls5.dywa.generated.util.FileReference picture = fileController.getFileReference(user.getprofilePicture().getDywaId());
				subject.setprofilePicture(picture);
			} else {
				if (subject.getprofilePicture() != null) {
					fileController.deleteFile(subject.getprofilePicture());
				}
				subject.setprofilePicture(null);
			}
						
			return javax.ws.rs.core.Response.ok(PyroUser.fromDywaEntity(subject, objectCache)).build();
		}

		return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
}
