package info.scce.pyro.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroProjectController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroUserController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroOrganizationController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroOrganizationAccessRightVectorController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroEditorGridController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroEditorGridItemController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroEditorWidgetController;
import de.ls5.dywa.generated.util.Identifiable;
import info.scce.pyro.core.rest.types.*;
import info.scce.pyro.rest.PyroSelectiveRestFilter;
import info.scce.pyro.sync.ProjectWebSocket;
import info.scce.pyro.sync.UserWebSocket;
import info.scce.pyro.sync.WebSocketMessage;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
	
@javax.transaction.Transactional
@javax.ws.rs.Path("/project/{projectId}/editorGrid")
public class EditorGridController {
	
	@javax.inject.Inject
	private PyroUserController subjectController;
	
	@javax.inject.Inject
	private PyroProjectController projectController;
	
	@javax.inject.Inject
	private PyroEditorGridController editorGridController;
	
	@javax.inject.Inject
	private PyroEditorGridItemController editorGridItemController;
	
	@javax.inject.Inject
	private PyroEditorWidgetController editorWidgetController;
	
	@javax.inject.Inject
	private EditorLayoutService editorLayoutService;
	
	@javax.inject.Inject
	private info.scce.pyro.rest.ObjectCache objectCache;
	
	@javax.ws.rs.GET
	@javax.ws.rs.Path("/")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public javax.ws.rs.core.Response get(@javax.ws.rs.PathParam("projectId") final long projectId) {
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
				
		if (subject != null) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project = projectController.read(projectId);
			checkPermission(subject, project);
			
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid searchObject = editorGridController.createSearchObject(null);
			searchObject.setuser(subject);
			searchObject.setproject(project);
			final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid> result = editorGridController.findByProperties(searchObject);
			return javax.ws.rs.core.Response.ok(PyroEditorGrid.fromDywaEntity(result.get(0), objectCache)).build();
		}
		
        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
	
	@javax.ws.rs.PUT
	@javax.ws.rs.Path("/{gridId}")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public javax.ws.rs.core.Response update(
			@javax.ws.rs.PathParam("projectId") final long projectId,
			@javax.ws.rs.PathParam("gridId") final long gridId,
			final PyroEditorGrid grid) {
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
				
		if (subject != null) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project = projectController.read(projectId);
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid gridInDB = editorGridController.read(gridId);
			checkPermission(subject, project, gridInDB);
			
			final java.util.Map<Long, de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem> itemMap = 
					gridInDB.getitems_PyroEditorGridItem().stream()
						.collect(java.util.stream.Collectors.toMap((i) -> i.getDywaId(), i -> i));
			
			for (final PyroEditorGridItem item: grid.getitems()) {
				de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem itemInDB = itemMap.get(item.getDywaId());
				itemInDB.setx(item.getx());
				itemInDB.sety(item.gety());
				itemInDB.setwidth(item.getwidth());
				itemInDB.setheight(item.getheight());
			}
			
			return javax.ws.rs.core.Response.ok(PyroEditorGrid.fromDywaEntity(gridInDB, objectCache)).build();
		}
		
        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
	
	@javax.ws.rs.POST
	@javax.ws.rs.Path("/{gridId}/setLayout/{layout}")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public javax.ws.rs.core.Response setLayout(
			@javax.ws.rs.PathParam("projectId") final long projectId,
			@javax.ws.rs.PathParam("gridId") final long gridId,
			@javax.ws.rs.PathParam("layout") final EditorGridLayout layout) {
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
				
		if (subject != null) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project = projectController.read(projectId);
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid gridInDB = editorGridController.read(gridId);
			checkPermission(subject, project, gridInDB);
			
			editorLayoutService.setLayout(gridInDB, layout);
						
			return javax.ws.rs.core.Response.ok(PyroEditorGrid.fromDywaEntity(gridInDB, objectCache)).build();
		}
		
        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
	
	@javax.ws.rs.POST
	@javax.ws.rs.Path("/{gridId}/areas")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public javax.ws.rs.core.Response createWidgetArea(
			@javax.ws.rs.PathParam("projectId") final long projectId,
			@javax.ws.rs.PathParam("gridId") final long gridId) {
		
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
		
		if (subject != null) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project = projectController.read(projectId);
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid gridInDB = editorGridController.read(gridId);
			checkPermission(subject, project, gridInDB);
			
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem newArea = editorGridItemController.create("EditorGridItem_");
			
			Long y = 0L;
			for (de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem a: gridInDB.getitems_PyroEditorGridItem()) {
				y =  java.lang.Math.max(y, a.gety() + a.getheight());
			}
			
			newArea.sety(y);
			newArea.setx(0L);
			newArea.setwidth(3L);
			newArea.setheight(3L);
			
			gridInDB.getitems_PyroEditorGridItem().add(newArea);
			
			return javax.ws.rs.core.Response.ok(PyroEditorGridItem.fromDywaEntity(newArea, objectCache)).build();
		}
		
        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
	
	@javax.ws.rs.POST
	@javax.ws.rs.Path("/{gridId}/areas/{areaId}/remove")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public javax.ws.rs.core.Response removeWidgetArea(
			@javax.ws.rs.PathParam("projectId") final long projectId,
			@javax.ws.rs.PathParam("gridId") final long gridId,
			@javax.ws.rs.PathParam("areaId") final long areaId) {
		
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
		
		if (subject != null) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project = projectController.read(projectId);
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid gridInDB = editorGridController.read(gridId);
			checkPermission(subject, project, gridInDB);
			
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem areaInDB = editorGridItemController.read(areaId);
			if (!gridInDB.getitems_PyroEditorGridItem().contains(areaInDB)) return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
			
			// hide all widgets inside the area
			for (de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget w: areaInDB.getwidgets_PyroEditorWidget()) {
				w.setarea(null);
				gridInDB.getavailableWidgets_PyroEditorWidget().add(w);
			}
			areaInDB.getwidgets_PyroEditorWidget().clear();
			gridInDB.getitems_PyroEditorGridItem().remove(areaInDB);
			editorGridItemController.delete(areaInDB);
			
			return javax.ws.rs.core.Response.ok(PyroEditorGrid.fromDywaEntity(gridInDB, objectCache)).build();
		}
		
        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
	
	@javax.ws.rs.POST
	@javax.ws.rs.Path("/{gridId}/widgets/{widgetId}/remove")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public javax.ws.rs.core.Response removeWidget(
			@javax.ws.rs.PathParam("projectId") final long projectId,
			@javax.ws.rs.PathParam("gridId") final long gridId,
			@javax.ws.rs.PathParam("widgetId") final long widgetId) {
		
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
		
		if (subject != null) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project = projectController.read(projectId);
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid gridInDB = editorGridController.read(gridId);
			checkPermission(subject, project, gridInDB);
			
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget widgetInDB = editorWidgetController.read(widgetId);
			if (!widgetInDB.getgrid().equals(gridInDB)) return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
			
			// move widget to grid
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem area = widgetInDB.getarea();
			area.getwidgets_PyroEditorWidget().remove(widgetInDB);
			widgetInDB.setarea(null);
			widgetInDB.setposition(0L);
			gridInDB.getavailableWidgets_PyroEditorWidget().add(widgetInDB);
			
			// remove widget area if it does not contain any widgets
			if (area.getwidgets_PyroEditorWidget().isEmpty()) {
				gridInDB.getitems_PyroEditorGridItem().remove(area);
				editorGridItemController.delete(area);
			}
			
			return javax.ws.rs.core.Response.ok(PyroEditorGrid.fromDywaEntity(gridInDB, objectCache)).build();
		}
		
        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
	
	
	@javax.ws.rs.POST
	@javax.ws.rs.Path("/{gridId}/widgets/{widgetId}/moveTo/{areaId}")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public javax.ws.rs.core.Response moveWidget(
			@javax.ws.rs.PathParam("projectId") final long projectId,
			@javax.ws.rs.PathParam("gridId") final long gridId,
			@javax.ws.rs.PathParam("widgetId") final long widgetId,
			@javax.ws.rs.PathParam("areaId") final long areaId) {
		
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
		
		if (subject != null) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project = projectController.read(projectId);
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid gridInDB = editorGridController.read(gridId);
			checkPermission(subject, project, gridInDB);
			
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget widgetInDB = editorWidgetController.read(widgetId);
			if (!widgetInDB.getgrid().equals(gridInDB)) return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
			
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem areaInDB = editorGridItemController.read(areaId);
			if (!gridInDB.getitems_PyroEditorGridItem().contains(areaInDB)) return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
			
			// move to another area
			if (widgetInDB.getarea() == null) {
				gridInDB.getavailableWidgets_PyroEditorWidget().remove(widgetInDB);
			} else {
				widgetInDB.getarea().getwidgets_PyroEditorWidget().remove(widgetInDB);
				if (widgetInDB.getarea().getwidgets_PyroEditorWidget().isEmpty()) {
					gridInDB.getitems_PyroEditorGridItem().remove(widgetInDB.getarea());
					editorGridItemController.delete(widgetInDB.getarea());
				}
			}
			widgetInDB.setarea(areaInDB);
			widgetInDB.setposition(Long.valueOf(areaInDB.getwidgets_PyroEditorWidget().size()));
			areaInDB.getwidgets_PyroEditorWidget().add(widgetInDB);
			
			return javax.ws.rs.core.Response.ok(PyroEditorGrid.fromDywaEntity(gridInDB, objectCache)).build();
		}
		
        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
	
	
	public void checkPermission(
			de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
			de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project) {
		
		if (user == null) {
			throw new WebApplicationException(Response.Status.FORBIDDEN);
		} else if (project == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		} else if (!(project.getorganization().getmembers_PyroUser().contains(user) || project.getorganization().getowners_PyroUser().contains(user))) {
			throw new WebApplicationException(Response.Status.FORBIDDEN);	
		}
	}
	
	public void checkPermission(
			de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
			de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project,
			de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid grid) {
		
		checkPermission(user, project);
		
		if (grid == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		} else if (!grid.getproject().equals(project)) {
			throw new WebApplicationException(Response.Status.FORBIDDEN);
		}
	}
}	
