package info.scce.pyro.core;

public enum EditorGridLayout {
	DEFAULT,
	MINIMAL,
	MAXIMUM_CANVAS,
	COMPLETE
}