package info.scce.pyro.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroProjectController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroUserController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroOrganizationController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroOrganizationAccessRightVectorController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroGraphModelPermissionVectorController;
import de.ls5.dywa.generated.util.Identifiable;
import info.scce.pyro.core.rest.types.*;
import info.scce.pyro.rest.PyroSelectiveRestFilter;
import info.scce.pyro.sync.ProjectWebSocket;
import info.scce.pyro.sync.UserWebSocket;
import info.scce.pyro.sync.WebSocketMessage;

import javax.ws.rs.core.Response;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
	
@javax.transaction.Transactional
@javax.ws.rs.Path("/project/{projectId}/graphModelPermissions")
public class GraphModelPermissionVectorController {
	
	@javax.inject.Inject
	private PyroUserController subjectController;
	
	@javax.inject.Inject
    private PyroOrganizationController organizationController;
	
	@javax.inject.Inject
    private PyroProjectController projectController;
	
	@javax.inject.Inject
    private PyroGraphModelPermissionVectorController permissionController;
		
	@javax.inject.Inject
	private info.scce.pyro.rest.ObjectCache objectCache;
	
	@javax.ws.rs.GET
	@javax.ws.rs.Path("/")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public javax.ws.rs.core.Response getAll(@javax.ws.rs.PathParam("projectId") final long projectId) {
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
				
		if (subject != null) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project = projectController.read(projectId);
			if (project == null) {
				return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
			}
			
			if (mayAccess(subject, project)) {
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector searchObject = permissionController.createSearchObject(null);
				searchObject.setproject(project);
				final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector> result = permissionController.findByProperties(searchObject);
				
				final java.util.List<PyroGraphModelPermissionVector> permissions = new java.util.ArrayList<>();
				for (de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector permission: result) {
					permissions.add(PyroGraphModelPermissionVector.fromDywaEntity(permission, objectCache));
				}
				
				return javax.ws.rs.core.Response.ok(permissions).build();
			}
		}
		
        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
	
	@javax.ws.rs.GET
	@javax.ws.rs.Path("/my")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public javax.ws.rs.core.Response get(@javax.ws.rs.PathParam("projectId") final long projectId) {
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
				
		if (subject != null) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project = projectController.read(projectId);
			if (project == null) {
				return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
			}
			
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector searchObject = permissionController.createSearchObject(null);
			searchObject.setuser(subject);
			searchObject.setproject(project);
			
			final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector> result = permissionController.findByProperties(searchObject);
			final java.util.List<PyroGraphModelPermissionVector> permissions = new java.util.ArrayList<>();
			for (de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector permission: result) {
				permissions.add(PyroGraphModelPermissionVector.fromDywaEntity(permission, objectCache));
			}
			
			return javax.ws.rs.core.Response.ok(permissions).build();				
		}
		
        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
	
	@javax.ws.rs.PUT
	@javax.ws.rs.Path("/{permissionId}")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public javax.ws.rs.core.Response update(
			@javax.ws.rs.PathParam("projectId") final long projectId, 
			@javax.ws.rs.PathParam("permissionId") final long permissionId,
			final PyroGraphModelPermissionVector permissionVector) {
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
				
		if (subject != null) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project = projectController.read(projectId);
			if (project == null) return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
			
			if (mayAccess(subject, project)) {
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector permissionVectorInDb = permissionController.read(permissionId);
				if (permissionVectorInDb == null) return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
				
				permissionVectorInDb.getpermissions_PyroCrudOperation().clear();
				permissionVectorInDb.getpermissions_PyroCrudOperation().addAll(permissionVector.getpermissions());
				
				return javax.ws.rs.core.Response.ok(PyroGraphModelPermissionVector.fromDywaEntity(permissionVectorInDb, objectCache)).build();	
			}				
		}
		
        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
	
	private boolean mayAccess(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user, 
							  de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project) {
		return user.getsystemRoles_PyroSystemRole().contains(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSystemRole.ADMIN) 
				|| project.getorganization().getowners_PyroUser().contains(user)
				|| project.getowner().equals(user);	
	}
}	
