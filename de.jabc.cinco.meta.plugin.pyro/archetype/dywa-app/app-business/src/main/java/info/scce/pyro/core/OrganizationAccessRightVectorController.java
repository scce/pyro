package info.scce.pyro.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroProjectController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroUserController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroOrganizationController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroOrganizationAccessRightVectorController;
import de.ls5.dywa.generated.util.Identifiable;
import info.scce.pyro.core.rest.types.*;
import info.scce.pyro.rest.PyroSelectiveRestFilter;
import info.scce.pyro.sync.ProjectWebSocket;
import info.scce.pyro.sync.UserWebSocket;
import info.scce.pyro.sync.WebSocketMessage;

import javax.ws.rs.core.Response;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
	
@javax.transaction.Transactional
@javax.ws.rs.Path("/organization/{orgId}/accessRights")
public class OrganizationAccessRightVectorController {
	
	@javax.inject.Inject
	private PyroUserController subjectController;
	
	@javax.inject.Inject
    private PyroOrganizationController organizationController;
	
	@javax.inject.Inject
    private PyroOrganizationAccessRightVectorController orgArvController;
		
	@javax.inject.Inject
	private info.scce.pyro.rest.ObjectCache objectCache;
	
	@javax.ws.rs.GET
	@javax.ws.rs.Path("/")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public javax.ws.rs.core.Response getAll(@javax.ws.rs.PathParam("orgId") final long orgId) {
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
				
		if (subject != null) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org = organizationController.read(orgId);
			if (org == null) {
				return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
			}
			
			if (isOwnerOf(subject, org)) {
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector searchObject = orgArvController.createSearchObject(null);
				searchObject.setorganization(org);
				final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector> result = orgArvController.findByProperties(searchObject);
				
				final java.util.List<PyroOrganizationAccessRightVector> arvs = new java.util.ArrayList<>();
				for (de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector arv: result) {
					arvs.add(PyroOrganizationAccessRightVector.fromDywaEntity(arv, objectCache));
				}
				
				return javax.ws.rs.core.Response.ok(arvs).build();
			}
		}
		
        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
	
	@javax.ws.rs.GET
	@javax.ws.rs.Path("/my")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public javax.ws.rs.core.Response get(@javax.ws.rs.PathParam("orgId") final long orgId) {
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
				
		if (subject != null) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org = organizationController.read(orgId);
			if (org == null) {
				return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
			}
			
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector searchObject = orgArvController.createSearchObject(null);
			searchObject.setuser(subject);
			searchObject.setorganization(org);
			
			final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector> result = orgArvController.findByProperties(searchObject);
			if (result.size() == 1) {
				return javax.ws.rs.core.Response.ok(PyroOrganizationAccessRightVector.fromDywaEntity(result.get(0), objectCache)).build();
			}					
		}
		
        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
	
	@javax.ws.rs.PUT
	@javax.ws.rs.Path("/{arvId}")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public javax.ws.rs.core.Response update(
			@javax.ws.rs.PathParam("orgId") final long orgId, 
			@javax.ws.rs.PathParam("arvId") final long arvId,
			final PyroOrganizationAccessRightVector arv) {
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
				
		if (subject != null) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org = organizationController.read(orgId);
			if (org == null) return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
			
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector arvInDb = orgArvController.read(arvId);
			if (arvInDb == null) return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
			
			arvInDb.getaccessRights_PyroOrganizationAccessRight().clear();
			arvInDb.getaccessRights_PyroOrganizationAccessRight().addAll(arv.getaccessRights());
			
			return javax.ws.rs.core.Response.ok(PyroOrganizationAccessRightVector.fromDywaEntity(arvInDb, objectCache)).build();					
		}
		
        return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.FORBIDDEN).build();
	}
	
	private boolean isOwnerOf(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user, de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org) {
		return org.getowners_PyroUser().contains(user);
	}
}	
