package info.scce.pyro.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroProjectController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroUserController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroOrganizationController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroOrganizationAccessRightVectorController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroSettingsController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroStyleController;
import de.ls5.dywa.generated.entity.info.scce.pyro.core.*;
import de.ls5.dywa.generated.util.Identifiable;
import info.scce.pyro.core.rest.types.*;
import info.scce.pyro.core.rest.types.PyroProject;
import info.scce.pyro.core.rest.types.PyroUser;
import info.scce.pyro.core.rest.types.PyroOrganization;
import info.scce.pyro.core.rest.types.PyroSettings;
//import info.scce.pyro.core.rest.types.PyroSystemRole;
import info.scce.pyro.rest.PyroSelectiveRestFilter;
import info.scce.pyro.sync.ProjectWebSocket;
import info.scce.pyro.sync.UserWebSocket;
import info.scce.pyro.sync.WebSocketMessage;

import javax.ws.rs.core.Response;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@javax.transaction.Transactional
@javax.ws.rs.Path("/settings")
public class SettingsController {

	@javax.inject.Inject
	private PyroUserController subjectController;

	@javax.inject.Inject
	private PyroSettingsController settingsController;
	
	@javax.inject.Inject
	private PyroStyleController styleController;
	
	@javax.inject.Inject
	private de.ls5.dywa.generated.util.DomainFileController domainFileController;
	    
	@javax.inject.Inject
	private info.scce.pyro.rest.ObjectCache objectCache;
	
	@javax.ws.rs.GET
	@javax.ws.rs.Path("/public")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public javax.ws.rs.core.Response get() {				
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSettings searchObject = settingsController.createSearchObject(null);
		final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSettings> result = settingsController.findByProperties(searchObject);
		return javax.ws.rs.core.Response.ok(PyroSettings.fromDywaEntity(result.get(0), objectCache)).build();	
	}
	
	@javax.ws.rs.PUT
	@javax.ws.rs.Path("/")
	@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@org.jboss.resteasy.annotations.GZIP
	public javax.ws.rs.core.Response update(final PyroSettings settings) {
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
		
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSettings settingsInDb = settingsController.read(settings.getDywaId());
				
		if (subject != null && isAdmin(subject) && settingsInDb != null) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroStyle style = settingsInDb.getstyle();
			style.setnavBgColor(settings.getstyle().getnavBgColor());
			style.setnavTextColor(settings.getstyle().getnavTextColor());
			style.setbodyBgColor(settings.getstyle().getbodyBgColor());
			style.setbodyTextColor(settings.getstyle().getbodyTextColor());
			style.setprimaryBgColor(settings.getstyle().getprimaryBgColor());
			style.setprimaryTextColor(settings.getstyle().getprimaryTextColor());
			if (settings.getstyle().getlogo() != null) {
				final de.ls5.dywa.generated.util.FileReference logo = domainFileController.getFileReference(settings.getstyle().getlogo().getDywaId());
				style.setlogo(logo);
			} else {
				style.setlogo(null);
			}
			
			settingsInDb.setgloballyCreateOrganizations(settings.getgloballyCreateOrganizations());
			return javax.ws.rs.core.Response.ok(PyroSettings.fromDywaEntity(settingsInDb, objectCache)).build(); 
		}
						
		return javax.ws.rs.core.Response.status(Response.Status.FORBIDDEN).build(); 
	}
	
	private boolean isAdmin(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user) {
		return user.getsystemRoles_PyroSystemRole().contains(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSystemRole.ADMIN);
	}
}
