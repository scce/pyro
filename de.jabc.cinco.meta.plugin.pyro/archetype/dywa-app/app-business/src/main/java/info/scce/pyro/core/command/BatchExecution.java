package info.scce.pyro.core.command;

import de.ls5.dywa.generated.entity.info.scce.pyro.core.GraphModel;
import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser;
import info.scce.pyro.core.command.types.Command;

import java.util.LinkedList;
import java.util.List;

/**
 * Author zweihoff
 */
public class BatchExecution {
    final PyroUser user;
    final List<Command> commands;
    TransactionMode mode;
    final GraphModel graphModel;

    BatchExecution(PyroUser user,GraphModel graphModel){
        this.user = user;
        commands = new LinkedList<>();
        this.graphModel = graphModel;
    }

    void add(Command cmd){
        commands.add(cmd);
    }

    public PyroUser getUser() {
        return user;
    }

    public List<Command> getCommands() {
        return commands;
    }

    public TransactionMode getMode() {
        return mode;
    }

    public void setMode(TransactionMode mode) {
        this.mode = mode;
    }
    
    public GraphModel getGraphModel(){
        return graphModel;
    }
}

enum TransactionMode {
    PROPAGATE, SILENT
}
