package info.scce.pyro.core.command;

import de.ls5.dywa.generated.controller.info.scce.pyro.core.BendingPointController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.EdgeController;
import de.ls5.dywa.generated.controller.info.scce.pyro.core.NodeController;
import de.ls5.dywa.generated.util.DomainFileController;
import info.scce.pyro.core.FileReferenceController;

import javax.persistence.EntityManager;

/**
 * Author zweihoff
 */
public class ControllerBundle {
    NodeController nodeController;

    EdgeController edgeController;

    BendingPointController bendingPointController;

    DomainFileController domainFileController;
    
    EntityManager entityManager;
    

    ControllerBundle(NodeController nodeController,EdgeController edgeController,BendingPointController bendingPointController,DomainFileController domainFileController,EntityManager entityManager) {
        this.nodeController = nodeController;
        this.edgeController = edgeController;
        this.bendingPointController = bendingPointController;
        this.domainFileController = domainFileController;
        this.entityManager = entityManager;
    }

    public DomainFileController getDomainFileController() {
        return this.domainFileController;
    }

    public EntityManager getEntityManager() {
        return this.entityManager;
    }
}
