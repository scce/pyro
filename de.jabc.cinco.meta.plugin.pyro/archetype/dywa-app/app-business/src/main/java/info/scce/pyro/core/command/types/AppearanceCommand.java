package info.scce.pyro.core.command.types;


import info.scce.pyro.core.graphmodel.Appearance;

/**
 * Author zweihoff
 */
@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CLASS, property = info.scce.pyro.util.Constants.DYWA_RUNTIME_TYPE)
public class AppearanceCommand extends Command {

    private Appearance appearance;

    @com.fasterxml.jackson.annotation.JsonProperty("appearance")
    public Appearance getAppearance() {
        return appearance;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("appearance")
    public void setAppearance(Appearance appearance) {
        this.appearance = appearance;
    }

    @Override
    protected void rewrite(long oldId, long newId) {
       
    }
}
