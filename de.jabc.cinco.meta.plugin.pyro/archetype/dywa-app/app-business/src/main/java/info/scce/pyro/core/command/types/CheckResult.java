package info.scce.pyro.core.command.types;


/**
 * Author zweihoff
 */
@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CLASS, property = info.scce.pyro.util.Constants.DYWA_RUNTIME_TYPE)
public class CheckResult {

    private String message;

    @com.fasterxml.jackson.annotation.JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }
    
    private String type;

    @com.fasterxml.jackson.annotation.JsonProperty("type")
    public String getType() {
        return type;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }
}
