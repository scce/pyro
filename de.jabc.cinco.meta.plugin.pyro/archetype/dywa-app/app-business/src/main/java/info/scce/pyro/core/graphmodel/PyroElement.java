package info.scce.pyro.core.graphmodel;

/**
 * Author zweihoff
 */

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CLASS, property = info.scce.pyro.util.Constants.DYWA_RUNTIME_TYPE)
public interface PyroElement extends info.scce.pyro.rest.RESTBaseType
{

    @com.fasterxml.jackson.annotation.JsonProperty("__type")
    public String get__type();

    @com.fasterxml.jackson.annotation.JsonProperty("__type")
    public void set__type(final String __type);
}