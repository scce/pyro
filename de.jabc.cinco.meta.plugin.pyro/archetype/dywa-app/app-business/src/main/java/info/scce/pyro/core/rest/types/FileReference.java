package info.scce.pyro.core.rest.types;

public class FileReference {

	private long dywaId;
	private String fileName;
	private String contentType;

	public FileReference() {
	}

	public FileReference(final de.ls5.dywa.generated.util.FileReference delegate) {
		this.setDywaId(delegate.getDywaId());

		this.setFileName(delegate.getFileName());
		this.setContentType(delegate.getContentType());
	}

	@com.fasterxml.jackson.annotation.JsonProperty("dywaId")
	public long getDywaId() {
		return dywaId;
	}

	@com.fasterxml.jackson.annotation.JsonProperty("dywaId")
	public void setDywaId(long id) {
		this.dywaId = id;
	}

	@com.fasterxml.jackson.annotation.JsonProperty("fileName")
	public String getFileName() {
		return fileName;
	}

	@com.fasterxml.jackson.annotation.JsonProperty("fileName")
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@com.fasterxml.jackson.annotation.JsonProperty("contentType")
	public String getContentType() {
		return contentType;
	}

	@com.fasterxml.jackson.annotation.JsonProperty("contentType")
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	@java.lang.Override
	public boolean equals(final java.lang.Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof FileReference)) {
			return false;
		}

		final FileReference that = (FileReference) obj;
		if (this.getDywaId() == -1 && that.getDywaId() == -1) {
			return this == that;
		}

		return this.getDywaId() == that.getDywaId();
	}
}
