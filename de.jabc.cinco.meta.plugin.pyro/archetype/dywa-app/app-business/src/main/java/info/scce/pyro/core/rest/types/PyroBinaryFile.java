package info.scce.pyro.core.rest.types;

/**
 * Author zweihoff
 */

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
public class PyroBinaryFile extends PyroFile
{

   
    
    private FileReference file;

    @com.fasterxml.jackson.annotation.JsonProperty("file")
    public FileReference getfile() {
        return this.file;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("file")
    public void setfile(final FileReference file) {
        this.file = file;
    }




    public static PyroBinaryFile fromDywaEntity(final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroBinaryFile entity, info.scce.pyro.rest.ObjectCache objectCache) {

        if(objectCache.containsRestTo(entity)){
            return objectCache.getRestTo(entity);
        }
        final PyroBinaryFile result;
        result = new PyroBinaryFile();
        result.setDywaId(entity.getDywaId());
        result.setDywaName(entity.getDywaName());
        result.setDywaVersion(entity.getDywaVersion());
        result.set__type(entity.getClass().getSimpleName());

        result.setfilename(entity.getfilename());
        result.setextension(entity.getextension());
        result.setfile(new FileReference(entity.getfile()));

        objectCache.putRestTo(entity, result);

        return result;
    }
}
