package info.scce.pyro.core.rest.types;

/**
 * Author zweihoff
 */

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
public class PyroEditorGrid extends info.scce.pyro.rest.RESTBaseImpl implements info.scce.pyro.rest.RESTBaseType
{

    private PyroUser user = new PyroUser();

    @com.fasterxml.jackson.annotation.JsonProperty("user")
    public PyroUser geuser() {
        return this.user;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("user")
    public void setuser(final PyroUser user) {
        this.user = user;
    }
    
    
    
    private PyroProject project = new PyroProject();

    @com.fasterxml.jackson.annotation.JsonProperty("project")
    public PyroProject getproject() {
        return this.project;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("project")
    public void setproject(final PyroProject project) {
        this.project = project;
    }
    
    
    
    private java.util.List<PyroEditorGridItem> items = new java.util.LinkedList<>();

    @com.fasterxml.jackson.annotation.JsonProperty("items")
    public java.util.List<PyroEditorGridItem> getitems() {
        return this.items;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("items")
    public void setitems(final java.util.List<PyroEditorGridItem> items) {
        this.items = items;
    }
    
    
    
    private java.util.List<PyroEditorWidget> availableWidgets = new java.util.LinkedList<>();

    @com.fasterxml.jackson.annotation.JsonProperty("availableWidgets")
    public java.util.List<PyroEditorWidget> getavailableWidgets() {
        return this.availableWidgets;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("availableWidgets")
    public void setavailableWidgets(final java.util.List<PyroEditorWidget> availableWidgets) {
        this.availableWidgets = availableWidgets;
    }
    
    

    public static PyroEditorGrid fromDywaEntity(final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid entity, info.scce.pyro.rest.ObjectCache objectCache) {

        if(objectCache.containsRestTo(entity)){
            return objectCache.getRestTo(entity);
        }
        final PyroEditorGrid result;
        result = new PyroEditorGrid();
        result.setDywaId(entity.getDywaId());
        result.setDywaName(entity.getDywaName());
        result.setDywaVersion(entity.getDywaVersion());

        result.setuser(PyroUser.fromDywaEntity(entity.getuser(),objectCache));
        result.setproject(PyroProject.fromDywaEntity(entity.getproject(),objectCache));
        
        objectCache.putRestTo(entity, result);
        
        for(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem o : entity.getitems_PyroEditorGridItem()){
            result.getitems().add(PyroEditorGridItem.fromDywaEntity(o, objectCache));
        }

        for(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget o : entity.getavailableWidgets_PyroEditorWidget()){
            result.getavailableWidgets().add(PyroEditorWidget.fromDywaEntity(o, objectCache));
        }

        return result;
    }
}
