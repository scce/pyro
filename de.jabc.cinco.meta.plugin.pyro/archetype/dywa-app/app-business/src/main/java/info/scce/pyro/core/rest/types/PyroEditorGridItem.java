package info.scce.pyro.core.rest.types;

/**
 * Author zweihoff
 */

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
public class PyroEditorGridItem extends info.scce.pyro.rest.RESTBaseImpl implements info.scce.pyro.rest.RESTBaseType
{

    private Long x;

    @com.fasterxml.jackson.annotation.JsonProperty("x")
    public Long getx() {
        return this.x;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("x")
    public void setx(final Long x) {
        this.x = x;
    }
    
    
    
    private Long y;

    @com.fasterxml.jackson.annotation.JsonProperty("y")
    public Long gety() {
        return this.y;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("y")
    public void sety(final Long y) {
        this.y = y;
    }
    
    
    
    private Long width;

    @com.fasterxml.jackson.annotation.JsonProperty("width")
    public Long getwidth() {
        return this.width;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("width")
    public void setwidth(final Long width) {
        this.width = width;
    }
    
    
    
    private Long height;

    @com.fasterxml.jackson.annotation.JsonProperty("height")
    public Long getheight() {
        return this.height;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("height")
    public void setheight(final Long height) {
        this.height = height;
    }
    
    
    
    private java.util.List<PyroEditorWidget> widgets = new java.util.LinkedList<>();

    @com.fasterxml.jackson.annotation.JsonProperty("widgets")
    public java.util.List<PyroEditorWidget> getwidgets() {
        return this.widgets;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("widgets")
    public void setwidgets(final java.util.List<PyroEditorWidget> widgets) {
        this.widgets = widgets;
    }
    
    
    
    public static PyroEditorGridItem fromDywaEntity(final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem entity, info.scce.pyro.rest.ObjectCache objectCache) {

        if(objectCache.containsRestTo(entity)){
            return objectCache.getRestTo(entity);
        }
        
        final PyroEditorGridItem result;
        result = new PyroEditorGridItem();
        result.setDywaId(entity.getDywaId());
        result.setDywaName(entity.getDywaName());
        result.setDywaVersion(entity.getDywaVersion());

        result.setx(entity.getx());
        result.sety(entity.gety());
        result.setwidth(entity.getwidth());
        result.setheight(entity.getheight());

        objectCache.putRestTo(entity, result);
        
        for(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget o : entity.getwidgets_PyroEditorWidget()){
            result.getwidgets().add(PyroEditorWidget.fromDywaEntity(o, objectCache));
        }

        return result;
    }
}
