package info.scce.pyro.core.rest.types;

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
public class PyroEditorWidget extends info.scce.pyro.rest.RESTBaseImpl implements info.scce.pyro.rest.RESTBaseType {

	
	
	private PyroEditorGridItem area;

    @com.fasterxml.jackson.annotation.JsonProperty("area")
    public PyroEditorGridItem getarea() {
        return this.area;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("area")
    public void setarea(final PyroEditorGridItem area) {
        this.area = area;
    }
    
    
    
    private PyroEditorGrid grid;

    @com.fasterxml.jackson.annotation.JsonProperty("grid")
    public PyroEditorGrid getgrid() {
        return this.grid;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("grid")
    public void setgrid(final PyroEditorGrid grid) {
        this.grid = grid;
    }
	
	
	
    private String key;

    @com.fasterxml.jackson.annotation.JsonProperty("key")
    public String getkey() {
        return this.key;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("key")
    public void setkey(final String key) {
        this.key = key;
    }
    
    
    
    private String tab;

    @com.fasterxml.jackson.annotation.JsonProperty("tab")
    public String gettab() {
        return this.tab;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("tab")
    public void settab(final String tab) {
        this.tab = tab;
    }
    
    
    
    private Long position;

    @com.fasterxml.jackson.annotation.JsonProperty("position")
    public Long getposition() {
        return this.position;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("position")
    public void setposition(final Long position) {
        this.position = position;
    }
    
    
    
    public static PyroEditorWidget fromDywaEntity(final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget entity, info.scce.pyro.rest.ObjectCache objectCache) {

        if(objectCache.containsRestTo(entity)){
            return objectCache.getRestTo(entity);
        }
        
        final PyroEditorWidget result;
        result = new PyroEditorWidget();
        result.setDywaId(entity.getDywaId());
        result.setDywaName(entity.getDywaName());
        result.setDywaVersion(entity.getDywaVersion());

        if (entity.getarea() != null) {
        	result.setarea(PyroEditorGridItem.fromDywaEntity(entity.getarea(), objectCache));	
        }
        result.setgrid(PyroEditorGrid.fromDywaEntity(entity.getgrid(), objectCache));
        result.settab(entity.gettab());
        result.setkey(entity.getkey());
        result.setposition(entity.getposition());
        
        objectCache.putRestTo(entity, result);

        return result;
    }
}
