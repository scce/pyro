package info.scce.pyro.core.rest.types;

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
public class PyroGraphModelPermissionVector extends info.scce.pyro.rest.RESTBaseImpl implements info.scce.pyro.rest.RESTBaseType {
	
	private PyroUser user;

    @com.fasterxml.jackson.annotation.JsonProperty("user")
    public PyroUser getuser() {
        return this.user;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("user")
    public void setuser(final PyroUser user) {
        this.user = user;
    }
    
    
    
    private PyroProject project;    
    
    @com.fasterxml.jackson.annotation.JsonProperty("project")
    public PyroProject getproject() {
        return this.project;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("project")
    public void setproject(final PyroProject project) {
        this.project = project;
    }
    
    
    
    private de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelType graphModelType;
    
    @com.fasterxml.jackson.annotation.JsonProperty("graphModelType")
    public de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelType getgraphModelType() {
        return this.graphModelType;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("graphModelType")
    public void setgraphModelType(final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelType graphModelType) {
        this.graphModelType = graphModelType;
    }
    
    
    
    private java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroCrudOperation> permissions = new java.util.LinkedList<>();
    
    @com.fasterxml.jackson.annotation.JsonProperty("permissions")
    public java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroCrudOperation> getpermissions() {
        return this.permissions;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("permissions")
    public void setpermissions(final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroCrudOperation> permissions) {
        this.permissions = permissions;
    }
    
    
    
    public static PyroGraphModelPermissionVector fromDywaEntity(
    		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroGraphModelPermissionVector entity, 
    		final info.scce.pyro.rest.ObjectCache objectCache) {

        if(objectCache.containsRestTo(entity)){
            return objectCache.getRestTo(entity);
        }
        
        final PyroGraphModelPermissionVector result;
        result = new PyroGraphModelPermissionVector();
        result.setDywaId(entity.getDywaId());
        result.setDywaName(entity.getDywaName());
        result.setDywaVersion(entity.getDywaVersion());

        result.setuser(PyroUser.fromDywaEntity(entity.getuser(), objectCache));
        result.setproject(PyroProject.fromDywaEntity(entity.getproject(), objectCache));
        result.setgraphModelType(entity.getgraphModelType());

        for(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroCrudOperation ar : entity.getpermissions_PyroCrudOperation()){
            result.getpermissions().add(ar);
        }

        return result;
    }
}