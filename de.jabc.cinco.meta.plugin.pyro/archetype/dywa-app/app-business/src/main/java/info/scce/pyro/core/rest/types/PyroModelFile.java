package info.scce.pyro.core.rest.types;

import info.scce.pyro.rest.RESTBaseType;

/**
 * Author zweihoff
 */

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
public class PyroModelFile extends PyroFile
{

	private boolean isPublic;

    private String connector;

    private String router;

    @com.fasterxml.jackson.annotation.JsonProperty("isPublic")
    public boolean getisPublic() {
        return this.isPublic;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("isPublic")
    public void setisPublic(final boolean isPublic) {
        this.isPublic = isPublic;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("a_connector")
    public String getConnector() {
        return connector;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("a_connector")
    public void setConnector(String connector) {
        this.connector = connector;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("a_router")
    public String getRouter() {
        return router;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("a_router")
    public void setRouter(String router) {
        this.router = router;
    }

    public static RESTBaseType fromDywaEntity(final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroModelFile entity, info.scce.pyro.rest.ObjectCache objectCache) {

        if(objectCache.containsRestTo(entity)) {
            return objectCache.getRestTo(entity);
        }
        final PyroModelFile result;
        result = new PyroModelFile();
        result.setDywaId(entity.getDywaId());
        result.setDywaName(entity.getDywaName());
        result.setDywaVersion(entity.getDywaVersion());
        result.set__type(entity.getClass().getSimpleName());

        result.setfilename(entity.getfilename());
        result.setextension(entity.getextension());
        if(entity instanceof de.ls5.dywa.generated.entity.info.scce.pyro.core.GraphModel) {
        	
        	result.setisPublic(((de.ls5.dywa.generated.entity.info.scce.pyro.core.GraphModel)entity).getisPublic());
            result.setConnector(((de.ls5.dywa.generated.entity.info.scce.pyro.core.GraphModel)entity).getconnector());
            result.setRouter(((de.ls5.dywa.generated.entity.info.scce.pyro.core.GraphModel)entity).getrouter());
        }

        objectCache.putRestTo(entity, result);

        return result;
    }


}
