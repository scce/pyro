package info.scce.pyro.core.rest.types;

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
public class PyroOrganization extends info.scce.pyro.rest.RESTBaseImpl implements info.scce.pyro.rest.RESTBaseType {
	
	private java.lang.String name;

    @com.fasterxml.jackson.annotation.JsonProperty("name")
    public java.lang.String getname() {
        return this.name;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("name")
    public void setname(final java.lang.String name) {
        this.name = name;
    }

    
    
    private java.lang.String description;

    @com.fasterxml.jackson.annotation.JsonProperty("description")
    public java.lang.String getdescription() {
        return this.description;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("description")
    public void setdescription(final java.lang.String description) {
        this.description = description;
    }
    
    
      
    private java.util.List<PyroUser> owners = new java.util.LinkedList<>();;

    @com.fasterxml.jackson.annotation.JsonProperty("owners")
    public java.util.List<PyroUser> getowners() {
        return this.owners;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("owners")
    public void setowners(final java.util.List<PyroUser> owners) {
        this.owners = owners;
    }
    
    
    
    private java.util.List<PyroUser> members = new java.util.LinkedList<>();;

    @com.fasterxml.jackson.annotation.JsonProperty("members")
    public java.util.List<PyroUser> getmembers() {
        return this.members;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("members")
    public void setmembers(final java.util.List<PyroUser> members) {
        this.members = members;
    }
    
    
    
    private java.util.List<PyroProject> projects = new java.util.LinkedList<>();;

    @com.fasterxml.jackson.annotation.JsonProperty("projects")
    public java.util.List<PyroProject> getprojects() {
        return this.projects;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("projects")
    public void setprojects(final java.util.List<PyroProject> projects) {
        this.projects = projects;
    }
    
    
    
    private PyroStyle style;

    @com.fasterxml.jackson.annotation.JsonProperty("style")
    public PyroStyle getstyle() {
        return this.style;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("style")
    public void setstyle(final PyroStyle style) {
        this.style = style;
    }
    
    

    public static PyroOrganization fromDywaEntity(
    		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization entity, 
    		final info.scce.pyro.rest.ObjectCache objectCache) {

        if(objectCache.containsRestTo(entity)){
            return objectCache.getRestTo(entity);
        }
        
        final PyroOrganization result;
        result = new PyroOrganization();
        result.setDywaId(entity.getDywaId());
        result.setDywaName(entity.getDywaName());
        result.setDywaVersion(entity.getDywaVersion());

        result.setname(entity.getname());
        result.setdescription(entity.getdescription());
        result.setstyle(PyroStyle.fromDywaEntity(entity.getstyle(), objectCache));
        
        objectCache.putRestTo(entity, result);
                
        for(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser o : entity.getowners_PyroUser()){
            result.getowners().add(PyroUser.fromDywaEntity(o, objectCache));
        }

        for(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser m : entity.getmembers_PyroUser()){
            result.getmembers().add(PyroUser.fromDywaEntity(m, objectCache));
        }
        
        for(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject p : entity.getprojects_PyroProject()){
            result.getprojects().add(PyroProject.fromDywaEntity(p, objectCache));
        }

        return result;
    }
}