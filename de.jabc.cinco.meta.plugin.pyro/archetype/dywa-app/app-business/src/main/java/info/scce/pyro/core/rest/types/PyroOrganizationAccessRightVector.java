package info.scce.pyro.core.rest.types;

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
public class PyroOrganizationAccessRightVector extends info.scce.pyro.rest.RESTBaseImpl implements info.scce.pyro.rest.RESTBaseType {

	private java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRight> accessRights = new java.util.LinkedList<>();
    
    @com.fasterxml.jackson.annotation.JsonProperty("accessRights")
    public java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRight> getaccessRights() {
        return this.accessRights;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("accessRights")
    public void setaccessRights(final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRight> accessRights) {
        this.accessRights = accessRights;
    }
    
    
    
    private PyroUser user;

    @com.fasterxml.jackson.annotation.JsonProperty("user")
    public PyroUser getuser() {
        return this.user;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("user")
    public void setuser(final PyroUser user) {
        this.user = user;
    }
    
    
    
    private PyroOrganization organization;

    @com.fasterxml.jackson.annotation.JsonProperty("organization")
    public PyroOrganization getorganization() {
        return this.organization;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("organization")
    public void setorganization(final PyroOrganization organization) {
        this.organization = organization;
    }
    
    
    
    public static PyroOrganizationAccessRightVector fromDywaEntity(
    		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector entity, 
    		final info.scce.pyro.rest.ObjectCache objectCache) {

        if(objectCache.containsRestTo(entity)){
            return objectCache.getRestTo(entity);
        }
        
        final PyroOrganizationAccessRightVector result;
        result = new PyroOrganizationAccessRightVector();
        result.setDywaId(entity.getDywaId());
        result.setDywaName(entity.getDywaName());
        result.setDywaVersion(entity.getDywaVersion());

        result.setuser(PyroUser.fromDywaEntity(entity.getuser(), objectCache));
        result.setorganization(PyroOrganization.fromDywaEntity(entity.getorganization(), objectCache));

        objectCache.putRestTo(entity, result);

        for(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRight ar : entity.getaccessRights_PyroOrganizationAccessRight()){
            result.getaccessRights().add(ar);
        }

        return result;
    }
}