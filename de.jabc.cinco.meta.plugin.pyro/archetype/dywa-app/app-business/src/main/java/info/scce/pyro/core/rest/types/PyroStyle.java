package info.scce.pyro.core.rest.types;

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
public class PyroStyle extends info.scce.pyro.rest.RESTBaseImpl implements info.scce.pyro.rest.RESTBaseType {
   
	private java.lang.String navBgColor;

    @com.fasterxml.jackson.annotation.JsonProperty("navBgColor")
    public java.lang.String getnavBgColor() {
        return this.navBgColor;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("navBgColor")
    public void setnavBgColor(final java.lang.String navBgColor) {
        this.navBgColor = navBgColor;
    }
    
    
    
    private java.lang.String navTextColor;

    @com.fasterxml.jackson.annotation.JsonProperty("navTextColor")
    public java.lang.String getnavTextColor() {
        return this.navTextColor;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("navTextColor")
    public void setnavTextColor(final java.lang.String navTextColor) {
        this.navTextColor = navTextColor;
    }
    
    
    
    private java.lang.String bodyBgColor;

    @com.fasterxml.jackson.annotation.JsonProperty("bodyBgColor")
    public java.lang.String getbodyBgColor() {
        return this.bodyBgColor;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("bodyBgColor")
    public void setbodyBgColor(final java.lang.String bodyBgColor) {
        this.bodyBgColor = bodyBgColor;
    }
    
    
    
    private java.lang.String bodyTextColor;

    @com.fasterxml.jackson.annotation.JsonProperty("bodyTextColor")
    public java.lang.String getbodyTextColor() {
        return this.bodyTextColor;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("bodyTextColor")
    public void setbodyTextColor(final java.lang.String bodyTextColor) {
        this.bodyTextColor = bodyTextColor;
    }
    
    
    
    private java.lang.String primaryBgColor;

    @com.fasterxml.jackson.annotation.JsonProperty("primaryBgColor")
    public java.lang.String getprimaryBgColor() {
        return this.primaryBgColor;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("primaryBgColor")
    public void setprimaryBgColor(final java.lang.String primaryBgColor) {
        this.primaryBgColor = primaryBgColor;
    }
    
    
    
    private java.lang.String primaryTextColor;

    @com.fasterxml.jackson.annotation.JsonProperty("primaryTextColor")
    public java.lang.String getprimaryTextColor() {
        return this.primaryTextColor;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("primaryTextColor")
    public void setprimaryTextColor(final java.lang.String primaryTextColor) {
        this.primaryTextColor = primaryTextColor;
    }
    
    
    
    private FileReference logo;

    @com.fasterxml.jackson.annotation.JsonProperty("logo")
    public FileReference getlogo() {
        return this.logo;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("logo")
    public void setlogo(final FileReference logo) {
        this.logo = logo;
    }
    

    
    public static PyroStyle fromDywaEntity(
    		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroStyle entity, 
    		final info.scce.pyro.rest.ObjectCache objectCache) {

        if (objectCache.containsRestTo(entity)) {
            return objectCache.getRestTo(entity);
        }
        
        final PyroStyle result;
        result = new PyroStyle();
        result.setDywaId(entity.getDywaId());
        result.setDywaName(entity.getDywaName());
        result.setDywaVersion(entity.getDywaVersion());

        result.setnavBgColor(entity.getnavBgColor());
        result.setnavTextColor(entity.getnavTextColor());
        result.setbodyBgColor(entity.getbodyBgColor());
        result.setbodyTextColor(entity.getbodyTextColor());
        result.setprimaryBgColor(entity.getprimaryBgColor());
        result.setprimaryTextColor(entity.getprimaryTextColor());
        if (entity.getlogo() != null) {
        	result.setlogo(new FileReference(entity.getlogo()));
        }

        objectCache.putRestTo(entity, result);

        return result;
    }
}
