package info.scce.pyro.core.rest.types;

/**
 * Author zweihoff
 */

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
public class PyroUser extends info.scce.pyro.rest.RESTBaseImpl implements info.scce.pyro.rest.RESTBaseType
{   
    
    private java.util.List<PyroProject> ownedProjects = new java.util.LinkedList<>();

    @com.fasterxml.jackson.annotation.JsonProperty("ownedProjects")
    public java.util.List<PyroProject> getownedProjects() {
        return this.ownedProjects;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("ownedProjects")
    public void setownedProjects(final java.util.List<PyroProject> ownedProjects) {
        this.ownedProjects = ownedProjects;
    }

    
    
    private java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSystemRole> systemRoles = new java.util.LinkedList<>();
    
    @com.fasterxml.jackson.annotation.JsonProperty("systemRoles")
    public java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSystemRole> getsystemRoles() {
        return this.systemRoles;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("systemRoles")
    public void setsystemRoles(final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSystemRole> systemRoles) {
        this.systemRoles = systemRoles;
    }

    
    
    private java.lang.String username;

    @com.fasterxml.jackson.annotation.JsonProperty("username")
    public java.lang.String getusername() {
        return this.username;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("username")
    public void setusername(final java.lang.String username) {
        this.username = username;
    }

    
    
    private java.lang.String email;

    @com.fasterxml.jackson.annotation.JsonProperty("email")
    public java.lang.String getemail() {
        return this.email;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("email")
    public void setemail(final java.lang.String email) {
        this.email = email;
    }
    
    
    
    private java.lang.String emailHash;

    @com.fasterxml.jackson.annotation.JsonProperty("emailHash")
    public java.lang.String getemailHash() {
        return this.emailHash;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("emailHash")
    public void setemailHash(final java.lang.String emailHash) {
        this.emailHash = emailHash;
    }
    
    
    
    private FileReference profilePicture;

    @com.fasterxml.jackson.annotation.JsonProperty("profilePicture")
    public FileReference getprofilePicture() {
        return this.profilePicture;
    }

    @com.fasterxml.jackson.annotation.JsonProperty("profilePicture")
    public void setprofilePicture(final FileReference profilePicture) {
        this.profilePicture = profilePicture;
    }

    

    public static PyroUser fromDywaEntity(final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser entity, info.scce.pyro.rest.ObjectCache objectCache) {

        if(objectCache.containsRestTo(entity)){
            return objectCache.getRestTo(entity);
        }
        final PyroUser result;
        result = new PyroUser();
        result.setDywaId(entity.getDywaId());
        result.setDywaName(entity.getDywaName());
        result.setDywaVersion(entity.getDywaVersion());

        result.setemail(entity.getemail());
        result.setusername(entity.getusername());
        result.setemailHash(entity.getemailHash());
        
        if (entity.getprofilePicture() != null) {
        	result.setprofilePicture(new FileReference(entity.getprofilePicture()));
        }

        objectCache.putRestTo(entity, result);

        for(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject p:entity.getownedProjects_PyroProject()){
            result.getownedProjects().add(PyroProject.fromDywaEntity(p,objectCache));
        }

        for(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroSystemRole p:entity.getsystemRoles_PyroSystemRole()){
            result.getsystemRoles().add(p);
        }

        return result;
    }
}
