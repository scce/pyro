package info.scce.pyro.util;

import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroElement;
import org.eclipse.emf.ecore.EObject;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Author zweihoff
 */
public class PyroList<T extends EObject> extends LinkedList<T> {

    private final Consumer<PyroElement> addFun;
    private final Consumer<PyroElement> removefun;

    public PyroList(Consumer<PyroElement> remove, Consumer<PyroElement> add, List<T> initial) {
        super(initial);
        this.addFun = add;
        this.removefun = remove;
    }

    @Override
    public boolean remove(Object o) {
        removefun.accept(((EObject) o).getDelegate());
        return super.remove(o);
    }

    @Override
    public boolean add(T t) {
        addFun.accept(t.getDelegate());
        return super.add(t);
    }

}
