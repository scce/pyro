package org.eclipse.emf.ecore;

import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroElement;

public interface EObject
{

  EObject eContainer();
  String getId();
  PyroElement getDelegate();

}
