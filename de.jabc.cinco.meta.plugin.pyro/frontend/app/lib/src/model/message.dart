import 'core.dart';
import 'dart:convert';
import 'command.dart';
import '../deserializer/property_deserializer.dart';

const String BASIC_MESSAGE_TYPE = "basic";
const String UNDO_MESSAGE_TYPE = "undo";
const String REDO_MESSAGE_TYPE = "redo";

const String BASIC_ANSWER_TYPE = "basic_answer";
const String BASIC_VALID_ANSWER_TYPE = "basic_valid_answer";
const String BASIC_INVALID_ANSWER_TYPE = "basic_invalid_answer";
const String UNDO_VALID_ANSWER_TYPE = "undo_valid_answer";
const String UNDO_INVALID_ANSWER_TYPE = "undo_invalid_answer";
const String REDO_VALID_ANSWER_TYPE = "redo_valid_answer";
const String REDO_INVALID_ANSWER_TYPE = "redo_invalid_answer";

abstract class Message {
  String messageType;
  int senderDywaId;
  
  static Message fromJSOG(Map jsog)
  {
    if(jsog['messageType'] == 'project'){
      return ProjectMessage.fromJSOG(jsog);
    }
    if(jsog['messageType'] == 'property'){
      return PropertyMessage.fromJSOG(jsog, new Map());
    }
    if(jsog['messageType'] == 'command'){
      return CompoundCommandMessage.fromJSOG(jsog);
    }
    return null;
  }

  static Message fromJSON(String s)
  {
    return fromJSOG(jsonDecode(s));
  }

  String toJSON()
  {
    return jsonEncode(this.toJSOG());
  }
  Map toJSOG();
}

class ProjectMessage extends Message {
  String messageType = "project";
  PyroProject project;

  static ProjectMessage fromJSOG(Map jsog)
  {
    ProjectMessage pm = new ProjectMessage();
    pm.senderDywaId = jsog["senderDywaId"];
    pm.project = PyroProject.fromJSOG(cache:new Map(),jsog: jsog['project']);
    return pm;
  }

  Map toJSOG()
  {
    Map jsog = new Map();
    
    jsog["senderDywaId"] = senderDywaId;
    jsog['messageType'] = 'project';
    jsog['dywaRuntimeType'] = "info.scce.pyro.core.command.types.ProjectMessage";
    jsog['project'] = project.toJSOG(new Map());
    return jsog;
  }

}

abstract class GraphMessage extends Message {
  int graphModelId;
}

class PropertyMessage extends GraphMessage {
	
  String messageType = "property";
  String graphModelType;
  IdentifiableElement delegate;

  PropertyMessage(int graphModelId,this.graphModelType,this.delegate,int senderDywaId){
    super.senderDywaId = senderDywaId;
    super.graphModelId = graphModelId;
  }

  static PropertyMessage fromJSOG(Map jsog,Map cache)
  {
    PropertyMessage pm = new PropertyMessage(
        jsog['graphModelId'],
        jsog['graphModelType'],
        PropertyDeserializer.deserialize(jsog['delegate'],jsog['graphModelType'],cache),
        jsog["senderDywaId"]);

    return pm;
  }

  Map toJSOG()
  {
    Map jsog = new Map();
    jsog['dywaRuntimeType']="info.scce.pyro.core.command.types.PropertyMessage";
    jsog['messageType'] = 'property';
    jsog['graphModelId'] = graphModelId;
    jsog["senderDywaId"] = senderDywaId;
    jsog['graphModelType'] = graphModelType;
    jsog['delegate'] = delegate.toJSOG(new Map());
    return jsog;
  }
}

class DisplayMessages {
	List<DisplayMessage> messages = new List();
	static DisplayMessages fromJSOG(Map jsog) {
		var dm = new DisplayMessages();
		for(var m in jsog['messages']) {
	      dm.messages.add(DisplayMessage.fromJSOG(m));
	    }
	    return dm;
	}
}

class DisplayMessage {
	String type;
	String content;
	
	static DisplayMessage fromJSOG(Map jsog) {
		var d = new DisplayMessage();
		d.type = jsog["messageType"];
		d.content = jsog["content"];
		return d;
	}
	
}

class CompoundCommandMessage extends GraphMessage {
  String messageType = "command";
  CompoundCommand cmd;
  String type;
  List<HighlightCommand> highlightings = new List();
  OpenFileCommand openFile = null;
  List<RewriteRule> rewriteRules = new List();

  CompoundCommandMessage(int grapModelId, this.type,this.cmd,int senderDywaId,List<HighlightCommand> highlightings){
    super.senderDywaId = senderDywaId;
    super.graphModelId = graphModelId;
    this.highlightings = highlightings;
  }

  static CompoundCommandMessage fromJSOG(Map jsog)
  {
    List<HighlightCommand> hs = new List();
    for(var h in jsog['highlightings']) {
      hs.add(HighlightCommand.fromJSOG(h));
    }
    

    List<RewriteRule> rewriteRule = new List();
    for(var rr in jsog['rewriteRule']) {
      rewriteRule.add(new RewriteRule(rr));
    }

    CompoundCommandMessage ccm = new CompoundCommandMessage(
        jsog['graphModelId'],
        jsog['type'],
        CompoundCommand.fromJSOG(jsog['cmd']),
        jsog["senderDywaId"],
        hs
    );
    
    if(jsog.containsKey('openFile') && jsog['openFile'] != null) {
    	ccm.openFile = OpenFileCommand.fromJSOG(jsog['openFile']);
    }

    ccm.rewriteRules = rewriteRule;

    return ccm;
  }

  Map toJSOG()
  {
    Map jsog = new Map();
    jsog['dywaRuntimeType']="info.scce.pyro.core.command.types.CompoundCommandMessage";
    jsog['messageType'] = 'command';
    jsog['graphModelId'] = graphModelId;
    jsog["senderDywaId"] = senderDywaId;
    jsog['cmd'] = cmd.toJSOG();
    jsog['type'] = type;
    jsog['highlightings'] = highlightings.map((n)=>n.toJSOG()).toList();
    return jsog;
  }
  
  CompoundCommandMessage customCommands() {
    CompoundCommand cc = new CompoundCommand();
    if(this.cmd.queue.length>1){
      cc.queue.addAll(this.cmd.queue.sublist(1));
    }
    CompoundCommandMessage ccm = new CompoundCommandMessage(this.graphModelId,this.type,cc,this.senderDywaId,this.highlightings);
    ccm.openFile = this.openFile;
    return ccm;
  }
}

class RewriteRule {
  int oldId;
  int newId;

  RewriteRule(Map jsog) {
    oldId = int.parse(jsog['oldId'].toString());
    newId = int.parse(jsog['newId'].toString());
  }
}

class ValidCreatedMessage extends Message{
  int dywaId;
  String dywaName;
  String dywaVersion;

  ValidCreatedMessage(int senderDywaId,this.dywaId){
    super.senderDywaId = senderDywaId;
  }

  @override
  Map toJSOG() {
    Map jsog = new Map();
    jsog['messageType'] = 'node_created';
    jsog['dywaId'] = dywaId;
    jsog['dywaName'] = dywaName;
    jsog['dywaVersion'] = dywaVersion;
    jsog["senderDywaId"] = senderDywaId;
    return jsog;
  }
}

class ValidMessage extends Message {


  ValidMessage(int senderDywaId){
    super.senderDywaId = senderDywaId;
  }

  @override
  Map toJSOG() {
    Map jsog = new Map();
    jsog['messageType'] = 'valid_message';
    jsog["senderDywaId"] = senderDywaId;
    return jsog;
  }
}
