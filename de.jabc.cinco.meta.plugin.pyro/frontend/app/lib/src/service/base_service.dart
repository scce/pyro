import 'dart:html';
import '../pages/main/routes.dart';
import 'package:angular_router/angular_router.dart';

class BaseService {

	final Router _router;
	
	BaseService(this._router){}

	Map<String, String> requestHeaders = {'Content-Type':'application/json'};
	
	String getBaseUrl({String protocol:null}) {
		if(window.location.protocol.contains("https")) {
	    	return '${protocol==null?window.location.protocol:(protocol+"s:")}//${window.location.hostname}${window.location.port.isEmpty?'':':'+window.location.port}/app';
    	}
		return '${protocol==null?window.location.protocol:(protocol+":")}//${window.location.hostname}${window.location.port.isEmpty?'':':'+window.location.port}/app';
	}
	
	dynamic handleProgressEvent(dynamic e) {
    	if(e is ProgressEvent && e.currentTarget is HttpRequest) {
	        HttpRequest r = e.currentTarget as HttpRequest;
	  	  	if(r.status==401) {
	  	  		window.localStorage['pyro_redirect']=window.location.href;
	  	  		// FIXME: Routes.login.toUrl() does not return "/home/login" but "/login" which does not exist and nothing happens
				_router.navigate("/home/login");
	  	  	}
    	}
    	throw e;
    }
    
    static String getUrl({String protocol:null}) {
    	if(window.location.protocol.contains("https")) {
	    	return '${protocol==null?window.location.protocol:(protocol+"s:")}//${window.location.hostname}${window.location.port.isEmpty?'':':'+window.location.port}/app';
    	}
    	return '${protocol==null?window.location.protocol:(protocol+":")}//${window.location.hostname}${window.location.port.isEmpty?'':':'+window.location.port}/app';
	}
}