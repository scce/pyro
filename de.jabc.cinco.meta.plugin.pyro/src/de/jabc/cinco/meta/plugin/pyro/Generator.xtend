package de.jabc.cinco.meta.plugin.pyro

import de.jabc.cinco.meta.plugin.pyro.util.Escaper
import de.jabc.cinco.meta.plugin.pyro.util.FileHandler
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import de.jabc.cinco.meta.plugin.pyro.util.OAuthCompound
import de.jabc.cinco.meta.runtime.xapi.FileExtension
import java.io.File
import java.net.URL
import java.util.HashMap
import java.util.HashSet
import java.util.LinkedList
import java.util.List
import java.util.Map
import java.util.Set
import mgl.GraphModel
import mgl.Import
import org.apache.commons.io.FileUtils
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IFolder
import org.eclipse.core.resources.IProject
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.FileLocator
import org.eclipse.core.runtime.IPath
import org.eclipse.core.runtime.Path
import org.eclipse.core.runtime.Platform
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.osgi.framework.Bundle
import productDefinition.Annotation
import productDefinition.CincoProduct

class Generator {
	def generate(Set<GraphModel> graphModels,CincoProduct cpd, IPath base,IProject iProject)
	{
		val fileHelper = new FileExtension
		
		val rootPostCreate = new LinkedList<String>();
		val organizationPostCreate = new LinkedList<String>();
		val editorLayout = new LinkedList<String>();
		val projectPostCreate = new LinkedList<String>();
		val initialOrganizations = new LinkedList<String>();
		val projectServices = new LinkedList<Annotation>();
		val projectActions = new LinkedList<Annotation>();
		var organizationPerUser = false
		var List<String> projetcsPerUser = null
		var OAuthCompound oauth = null
		var Map<IProject,GraphModel> transientGraphModels = new HashMap
		val javaPath = base+"/dywa-app/app-business/src/main/java/"
		for(a : cpd.annotations){
			
			if(a.name == "pyroProjectService" && a.value.size >= 3){
				// FQN, ServiceName, Arguments...
				projectServices.add(a)
				FileHandler.copyAnnotatedClasses(a,iProject,javaPath)
			}
			
			if(a.name == "pyroProjectAction" && a.value.size == 2){
				// FQN, Action Name
				projectActions.add(a)
				FileHandler.copyAnnotatedClasses(a,iProject,javaPath)
			}
			
			if(a.name== "pyroInitialOrganizations" ){	
				 if(a.getValue().size()>0){
				 	
				 	initialOrganizations.addAll(a.value)
				} 	
			}
			
			if(a.name== "pyroTransientAPI" ){	
				 if(a.getValue().size()==1){
				 	val path = new Path(a.value.get(0));
    				val file = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
    				file.project
				 	val g = fileHelper.getContent(file, GraphModel)
				 	transientGraphModels.put(file.project,g)
				} 	
			}
			
			if(a.name== "pyroProjectPerUser" ){	
				projetcsPerUser = new LinkedList<String>()
				projetcsPerUser.addAll(a.value)
			}
			
			if(a.name== "pyroOrganizationPerUser" ){	
				 	organizationPerUser = true
			}
			
			if(a.name== "pyroProjectPostCreate" ){	
				 if(a.getValue().size()==1){
				 	FileHandler.copyAnnotatedClasses(a,iProject,javaPath)
				 	projectPostCreate.add(a.value.get(0))
				} 	
			}
			
			if(a.name== "pyroOrganizationPostCreate" ){	
				 if(a.getValue().size()==1){
				 	FileHandler.copyAnnotatedClasses(a,iProject,javaPath)
				 	organizationPostCreate.add(a.value.get(0))
				} 	
			}
			
			if(a.name== "pyroEditorLayout" ){	
				 if(a.getValue().size()==1){
				 	FileHandler.copyAnnotatedClasses(a,iProject,javaPath)
				 	editorLayout.add(a.value.get(0))
				} 	
			}
			
			if(a.name== "pyroRootPostCreate" ){	
				 if(a.getValue().size()==1){
				 	FileHandler.copyAnnotatedClasses(a,iProject,javaPath)
				 	rootPostCreate.add(a.value.get(0))
				} 	
			}
			
			if(a.name== "pyroOAuth" ){	
				 if(a.getValue().size()==1){
				 	val p = FileHandler.getPropertiesFile(a,iProject)
				 	if(p !== null) {
				 		oauth = new OAuthCompound(
				 			p.getProperty("name","OAuth"),
				 			p.getProperty("callbackURL",""),
				 			p.getProperty("clientID",""),
				 			p.getProperty("clientSecret",""),
				 			p.getProperty("scope","user"),
				 			p.getProperty("signinURL",""),
				 			p.getProperty("authURL",""),
				 			p.getProperty("userURL",""),
				 			p.getProperty("userAccountIdentifier",""),
				 			p.getProperty("userAccountName",""),
				 			p.getProperty("admins",""),
				 			new Escaper().randomString(10)
				 		); 
				 	}
				} 	
			}
				
		}
		
		val ecores = new HashSet<EPackage>();
		graphModels.forEach[ecoreImports(ecores)]
		val gc = new GeneratorCompound(cpd.name,
			graphModels,
			ecores,
			iProject,
			rootPostCreate,
			organizationPostCreate,
			projectPostCreate,
			oauth,
			editorLayout,
			initialOrganizations,
			projetcsPerUser,
			organizationPerUser,
			projectServices,
			projectActions,
			transientGraphModels,
			cpd
		); 
		
		
		//generate preview
		new de.jabc.cinco.meta.plugin.pyro.preview.Generator(base).generate(gc,iProject)
		
		//copy back end statics
		{
			copyResources("archetype/dywa-config",base)
			copyResources("archetype/dywa-app",base)
			//copyResources("archetype/dywa-nginx",base)
			if(!base.toFile.list.contains("run-config")){
				copyResources("archetype/run-config",base)
			}
			copyResources("archetype/docker-compose.override.yml",base)
			if(!base.toFile.list.contains("docker-compose.production.yml")){
				copyResources("archetype/docker-compose.production.yml",base)
			}
			copyResources("archetype/docker-compose.yml",base)
			copyResources("archetype/nginx.conf",base)
			copyResources("archetype/rsync-exclude.txt",base)
			if(!base.toFile.list.contains("oauth.properties")){
				copyResources("archetype/oauth.properties",base)
			}
			//generate cinco dependencies
			
			//MCAM
			//TODO enabel needed methods on C-API
//			val mcam = PluginRegistry.getInstance().getSuitableMetaPlugins("mcam",PluginRegistryEntryImpl.GRAPH_MODEL_ANNOTATION)
//			if(mcam.isEmpty) {
//				val v = mcam.map[metaPluginService].get(0)
//				graphModels.filter[gc.mglExtension.hasChecks(it)].forEach[g|{
//					v.execute(#{'graphModel'->g})
//				}]
//			}
			//generate backend sources
			val backEndGenerator = new de.jabc.cinco.meta.plugin.pyro.backend.Generator(base)
			backEndGenerator.generator(gc,iProject)				
		}
		
		
		//copy front end statics
		{
			//generate front end
			//val path = base.append("/dywa-app/app-presentation/target/generated-sources/app")
			val path = base.append("/webapp")
			copyResources("frontend/app/lib",path)
			copyResources("frontend/app/web",path)
			copyResources("frontend/app/build.yaml",path)
			copyResources("frontend/app/Dockerfile",path)
			copyResources("frontend/app/basic_auth",path)
			copyResources("frontend/app/default.conf",path)
			val frontEndGen = new de.jabc.cinco.meta.plugin.pyro.frontend.Generator(path)
			frontEndGen.generate(gc,iProject)
			//generate modeling canvas
			val modelingGen = new de.jabc.cinco.meta.plugin.pyro.canvas.Generator(path)
			modelingGen.generator(gc,iProject)
		}
		
		for(a : cpd.annotations){
			if(a.name== "pyroIncludeSources" ){	
				 if(a.getValue().size()!=0){
				 	val jPath = "dywa-app/app-business/src/main/java"
					val path = base.append(jPath)
				 	for (v : a.value){
				 		copySources(v, path)	
				 	}
									
				} 	
			}
		}
	}
	
	/**
	 * Copies all files recursively from a given bundle to a given target path.
	 * @param bundleId
	 * @param target
	 */
	static def copyResources(String source,IPath target) {
		val Bundle b = Platform.getBundle("de.jabc.cinco.meta.plugin.pyro");
		val URL directoryURL = b.getEntry(source);
		try {
			// solution based on http://stackoverflow.com/a/23953081
			val URL fileURL = FileLocator.toFileURL(directoryURL);
			val java.net.URI resolvedURI = new java.net.URI(fileURL.getProtocol(), fileURL.getPath(), null);
		    val File directory = new File(resolvedURI);
		    if(directory.isDirectory) {
				FileUtils.copyDirectoryToDirectory(directory, target.toFile);		    	
		    } else {
		    	FileUtils.copyFileToDirectory(directory, target.toFile);
		    }

		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	def ecoreImports(GraphModel g,Set<EPackage> ecores) {
		
		for(Import import1 : g.imports) {
		
			var r = findImportModels(import1,g);
			if(r !== null) {
		    for(EObject eObject:r.getContents() ){
		        if(eObject instanceof EPackage) {
		            val ePackage = eObject;
		                if(!ePackage.getName().equals(g.getName()) && !g.getNsURI().equals(ePackage.getNsURI())){
		                	if(!ecores.exists[name.equals(eObject.name)]) {
			                	ecores.add(eObject);                             
		           
		                	}
		                }
		            }                               
		        }
		    }
		}
		
	}
	
	private def Resource findImportModels(Import import1,GraphModel masterModel)
    {
        val path = import1.getImportURI();
        val uri = URI.createURI(path, true);
        try {
            var Resource res = null;
            if (uri.isPlatformResource()) {
                res = new ResourceSetImpl().getResource(uri, true);
            }
            else {
                val IProject p = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(masterModel.eResource().getURI().toPlatformString(true))).getProject();
                val IFile file = p.getFile(path);
                if (file.exists()) {
                    val URI fileURI = URI.createPlatformResourceURI(file.getFullPath().toOSString(), true);
                    res = new ResourceSetImpl().getResource(fileURI, true);
                }
                else {
                    return null;
                }
            }

            return res;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    static def copySources(String sourceName, IPath target){
    	var String[] splitedName = sourceName.split("/")
    	val bundleName = ResourcesPlugin.workspace.root.getProject(splitedName.get(0))
    	val String pathName = sourceName.substring(splitedName.get(0).length+1).replace('.','/')	    	   
           
			try {
				if(!pathName.isEmpty){
					if(pathName.equals("src")|| pathName.equals("src/")||pathName.equals("src-gen")|| pathName.equals("src-gen/")){
						val IFolder srcFolder = bundleName.getFolder(pathName)
           				val URL directoryURL = srcFolder.locationURI.toURL
           				val URL fileURL = FileLocator.toFileURL(directoryURL);
						val java.net.URI resolvedURI = new java.net.URI(fileURL.getProtocol(), fileURL.getPath(), null);
	    				val File directory = new File(resolvedURI);
						FileUtils.copyDirectory(directory, target.toFile);
					}else{
							val String[] newSplitedName = pathName.split("/")
						if((pathName.contains("src") && newSplitedName.get(0).equals("src"))||(pathName.contains("src-gen") && newSplitedName.get(0).equals("src-gen"))){
							val String newPathName = pathName.substring(newSplitedName.get(0).length+1)
							val IPath newTarget = target.append(newPathName);
							val IFolder srcFolder = bundleName.getFolder(pathName)
           					val URL directoryURL = srcFolder.locationURI.toURL
           					val URL fileURL = FileLocator.toFileURL(directoryURL);
							val java.net.URI resolvedURI = new java.net.URI(fileURL.getProtocol(), fileURL.getPath(), null);
	    					val File directory = new File(resolvedURI);
							FileUtils.copyDirectory(directory, newTarget.toFile);
						}
					}					
				}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
    }
}
