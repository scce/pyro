package de.jabc.cinco.meta.plugin.pyro.backend

import de.jabc.cinco.meta.core.utils.CincoUtil
import de.jabc.cinco.meta.plugin.pyro.backend.connector.DataConnector
import de.jabc.cinco.meta.plugin.pyro.backend.core.EditorLayoutService
import de.jabc.cinco.meta.plugin.pyro.backend.core.InitializeSettingsBean
import de.jabc.cinco.meta.plugin.pyro.backend.core.OrganizationController
import de.jabc.cinco.meta.plugin.pyro.backend.core.ProjectController
import de.jabc.cinco.meta.plugin.pyro.backend.core.ProjectService
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api.GraphModelElementInterface
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api.GraphModelFactoryInterface
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api.GraphModelSwitch
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.auth.OAuthController
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.command.GraphModelCommandExecuter
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.command.GraphModelControllerBundle
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.controller.EcoreController
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.core.PyroFileControllerGenerator
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.mcam.adapter.McamAdapter
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.mcam.adapter.McamId
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.mcam.cli.ContainmentCheck
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.mcam.cli.Execution
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.mcam.cli.IncomingCheck
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.mcam.cli.OutgoingCheck
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.mcam.modules.checks.Module
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.rest.EcoreRestTO
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.rest.GraphModelRestTO
import de.jabc.cinco.meta.plugin.pyro.util.EditorViewPluginRegistry
import de.jabc.cinco.meta.plugin.pyro.util.Escaper
import de.jabc.cinco.meta.plugin.pyro.util.FileGenerator
import de.jabc.cinco.meta.plugin.pyro.util.FileHandler
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import de.jabc.cinco.meta.plugin.pyro.util.MGLExtension
import java.util.HashMap
import mgl.ModelElement
import org.eclipse.core.resources.IProject
import org.eclipse.core.runtime.IPath
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.core.GraphmodelExporterGenerator
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.core.rest.RegistrationGenerator
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.controller.MainGraphModelController
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.controller.MainControllerBundle
import de.jabc.cinco.meta.plugin.pyro.backend.service.rest.ServiceRestTO
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.controller.GraphModelController
import de.jabc.cinco.meta.plugin.pyro.backend.service.ProjectServiceController
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api.EcoreInterface
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EEnum
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api.EcoreImplementation
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api.impl.GraphModelFactoryImplementation
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api.impl.GraphModelElementImplementation
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api.impl.GraphModelFactoryTransientImplementation
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api.impl.GraphModelElementTransientImplementation
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api.impl.GraphModelInterpreter

class Generator extends FileGenerator {
	
	protected extension MGLExtension mglExtension
	protected extension Escaper = new Escaper
	
	new(IPath base) {
		super(base)
	}
	
	def generator(GeneratorCompound gc,IProject iProject) {
		
		mglExtension = gc.mglExtension
		
		val businessPath = "dywa-app/app-business/"
		val businessBasePath = businessPath+"src/main/java/"
		val presentationPath = "dywa-app/app-presentation/"
		
		
		//create data connector
		{
			//dywa-app.app-connector.target.generated-sources.info.scce.pyro.data
			val path = "dywa-app/app-connector/target/generated-sources/info/scce/pyro/data"
			val gen = new DataConnector(gc)
			generateJavaFile(path,
				gen.fileNameDataConnector,
				gen.contentDataConnector
			)
		}
		
		// create ProjectService
		{
			val path = businessBasePath+"info/scce/pyro/core"
			val gen = new ProjectService(gc)
			generateJavaFile(path,
				gen.fileNameDispatcher,
				gen.contentDispatcher
			)
		}
		// create PyroFileController
		{
			val path = businessBasePath+"info/scce/pyro/core"
			val gen = new PyroFileControllerGenerator(gc)
			generateJavaFile(path,
				gen.filename,
				gen.content
			)
		}
		// create ProjectController
		{
			val path = businessBasePath+"info/scce/pyro/core"
			val gen = new ProjectController(gc)
			generateJavaFile(path,
				gen.fileName,
				gen.content
			)
		}
		// create OrganizationController
		{
			val path = businessBasePath+"info/scce/pyro/core"
			val gen = new OrganizationController(gc)
			generateJavaFile(path,
				gen.filename,
				gen.content
			)
		}
		// create InitializeSettingsBean
		{
			val path = businessBasePath+"info/scce/pyro/core"
			val gen = new InitializeSettingsBean(gc)
			generateJavaFile(path,
				gen.filename,
				gen.content
			)
		}
		
		// create Project Service Rest TOs
		{
			if(!gc.projectServices.empty) {
				
				val path = businessBasePath+"info/scce/pyro/service/rest"
				val gen = new ServiceRestTO(gc)
				gc.projectServices.forEach[
					generateJavaFile(path,
						gen.filename(it),
						gen.content(it)
					)
				]
				
				val servicePath = businessBasePath+"info/scce/pyro/service"
				val gen2 = new ProjectServiceController(gc)
				generateJavaFile(servicePath,
						gen2.filename(),
						gen2.content()
					)
			}
		}
		
		
		
		// create OAuth Authenticator
		{
			if(gc.authCompound !== null) {
				
				val path = businessBasePath+"info/scce/pyro/auth"
				val gen = new OAuthController(gc)
				generateJavaFile(path,
					gen.filename(),
					gen.content()
				)
			}
		}
		
		// create CINCO Exporter
		{
				
			val path = businessBasePath+"info/scce/pyro/core/export"
			val gen = new GraphmodelExporterGenerator(gc)
			gc.graphMopdels.forEach[g|{
				
				generateJavaFile(path,
					gen.filename(g),
					gen.content(g)
				)
			}]
		}
		
		// create CINCO Exporter
		{
				
			val path = businessBasePath+"info/scce/pyro/core/rest"
			val gen = new RegistrationGenerator(gc)
			gc.graphMopdels.forEach[g|{
				
				generateJavaFile(path,
					gen.filename(g),
					gen.content(g)
				)
			}]
		}
		
		// create EditorLayoutService
		{
			val path = businessBasePath+"info/scce/pyro/core"
			val gen = new EditorLayoutService(gc)
			generateJavaFile(path,
				gen.fileNameDispatcher,
				gen.contentDispatcher
			)
		}
		
		{
			val path = businessBasePath+"info/scce/pyro/interpreter/"
			val gen = new GraphModelInterpreter(gc)
			gc.graphMopdels.forEach[g|{
				val graphPath = path + g.name
				clearDirectory(graphPath)
				generateJavaFile(graphPath,
						gen.filename(g),
						gen.content(g)
					)
			}]
		}
		
		//create graphmodel rest TOs
		{
			//dywa-app.app-business.target.generated-sources.info.scce.pyro.data
			val path = businessBasePath+"info/scce/pyro/"
			val gen = new GraphModelRestTO(gc)
			gc.graphMopdels.forEach[g|{
				val styles = CincoUtil.getStyles(g, iProject)
				val graphPath = path + g.name+"/rest"
				clearDirectory(graphPath)
				generateJavaFile(graphPath,
						gen.filename(g),
						gen.content(g,g,styles)
					)
				g.elementsAndTypes.forEach[t|{
					generateJavaFile(graphPath,
						gen.filename(t),
						gen.content(t,g,styles)
					)
				}]
				
			}]
		}
		
		//create ecore rest TOs
		{
			//dywa-app.app-business.target.generated-sources.info.scce.pyro
			val path = businessBasePath+"info/scce/pyro/"
			val gen = new EcoreRestTO(gc)
			gc.ecores.forEach[g|{
				val ecorePath = path + g.name+"/rest"
				clearDirectory(ecorePath)
				generateJavaFile(ecorePath,
						gen.filenameEcore(g),
						gen.contentEcore(g)
					)
				generateJavaFile(ecorePath,
						gen.filenamePackage(g),
						gen.contentPackage(g)
					)
				g.elementsAndEnums.forEach[t|{
					generateJavaFile(ecorePath,
						gen.filenameStructural(t),
						gen.contentStructural(t,g)
					)
				}]
				
			}]
		}
		
		{
			val pluginPath = businessBasePath+"info/scce/pyro/plugin"
			clearDirectory(pluginPath)
			val plugins = new EditorViewPluginRegistry().getPlugins(gc)
			
			plugins.forEach[p|
				//generate rest controller java class
				generateJavaFile(pluginPath+"/controller",
					p.restController.filename,
					p.restController.content
				)
				
			]
		}
		
		//create mcam adapter and id
		{
			//dywa-app.app-business.target.generated-sources.info.scce.pyro.data
			val path = businessBasePath
			val adapterGen = new McamAdapter(gc)
			val execGen = new Execution(gc)
			val idGen = new McamId(gc)
			val moduleGen = new Module(gc)
			val containmentCheckGen = new ContainmentCheck(gc)
			val incomingCheckGen = new IncomingCheck(gc)
			val outgoingCheckGen = new OutgoingCheck(gc)
			gc.graphMopdels.filter[hasChecks].forEach[g|{
				val mcampath = path+g.packagePath.toString+"/mcam/adapter";
				clearDirectory(mcampath)
				generateJavaFile(mcampath,
					adapterGen.filename(g),
					adapterGen.content(g)
				)
				generateJavaFile(mcampath,
					idGen.filename(g),
					idGen.content(g)
				)
				val execpath = path+g.packagePath.toString+"/mcam/cli";
				clearDirectory(execpath)
				generateJavaFile(execpath,
					execGen.filename(g),
					execGen.content(g)
				)
				val modulepath = path+g.packagePath.toString+"/mcam/modules/checks";
				clearDirectory(modulepath)
				generateJavaFile(modulepath,
					moduleGen.filename(g),
					moduleGen.content(g)
				)
				generateJavaFile(modulepath,
					containmentCheckGen.filename(g),
					containmentCheckGen.content(g)
				)
				generateJavaFile(modulepath,
					incomingCheckGen.filename(g),
					incomingCheckGen.content(g)
				)
				generateJavaFile(modulepath,
					outgoingCheckGen.filename(g),
					outgoingCheckGen.content(g)
				)
				
			}]
		}
		
		//create graph model API Interfaces
		{
			//dywa-app.app-business.target.generated-sources.info.scce.pyro.data
			val path = businessBasePath
			val gen = new GraphModelElementInterface(gc)
			gc.graphMopdels.forEach[g|{
				val graphPath = path+g.apiPath.toString;
				clearDirectory(graphPath)
				(#[g]+(g.elementsAndTypes)).filter(ModelElement).forEach[t|{
					generateJavaFile(graphPath,
						gen.filename(t),
						gen.content(t,g,false)
					)
				}]
				//generate graph model enumerations
				g.enumerations.forEach[e|
					generateJavaFile(graphPath,
						gen.filename(e),
						gen.contentEnum(e,g)
					)
				]
				
			}]
		}
		
		//create graph model API Implementation
		{
			//dywa-app.app-business.target.generated-sources
			val path = businessBasePath
			val gen = new GraphModelElementImplementation(gc)
			gc.graphMopdels.forEach[g|{
				val styles = CincoUtil.getStyles(g, iProject)
				val graphPath = path+g.apiImplPath.toString;
				clearDirectory(graphPath)
				(#[g]+(g.elementsAndTypes)).filter(ModelElement).filter[!isIsAbstract].forEach[t|{
					generateJavaFile(graphPath,
						gen.filename(t),
						gen.content(t,g,styles)
					)
				}]
				
			}]
		}
		
		/**
		 * Transient API Generation
		 */
		 {
			//dywa-app.app-business.target.generated-sources.info.scce.pyro.data
			val path = businessBasePath
			val gen = new GraphModelElementInterface(gc)
			gc.transientGraphModels.values.forEach[g|{
				val graphPath = path+g.apiPath.toString;
				clearDirectory(graphPath)
				(#[g]+(g.elementsAndTypes)).filter(ModelElement).forEach[t|{
					generateJavaFile(graphPath,
						gen.filename(t),
						gen.content(t,g,true)
					)
				}]
				//generate graph model enumerations
				g.enumerations.forEach[e|
					generateJavaFile(graphPath,
						gen.filename(e),
						gen.contentEnum(e,g)
					)
				]
				
			}]
		}
		
		//create graph model API Implementation
		{
			//dywa-app.app-business.target.generated-sources
			val path = businessBasePath
			val gen = new GraphModelElementTransientImplementation(gc)
			gc.transientGraphModels.entrySet.forEach[e|{
				val styles = CincoUtil.getStyles(e.value, e.key)
				val graphPath = path+e.value.apiImplPath.toString;
				clearDirectory(graphPath)
				(#[e.value]+(e.value.elementsAndTypes)).filter(ModelElement).filter[!isIsAbstract].forEach[t|{
					generateJavaFile(graphPath,
						gen.filename(t),
						gen.content(t,e.value,styles)
					)
				}]
				
			}]
		}
		
		//create graph model Factory interface
		{
			//dywa-app.app-business.target.generated-sources
			val path = businessBasePath
			val gen = new GraphModelFactoryInterface(gc)
			gc.transientGraphModels.values.forEach[g|{
				val graphPath = path+g.factoryPath.toString;
				generateJavaFile(graphPath,
					gen.filename(g),
					gen.content(g,true)
				)
			}]
		}
		
		//create graph model Factory impl
		{
			//dywa-app.app-business.target.generated-sources
			val path = businessBasePath
			val gen = new GraphModelFactoryTransientImplementation(gc)
			gc.transientGraphModels.values.forEach[g|{
				val graphPath = path+g.apiImplPath.toString;
				generateJavaFile(graphPath,
					gen.filename(g),
					gen.content(g)
				)
			}]
		}
		
		
		
		//create ecore API Interface
		{
			//dywa-app.app-business.target.generated-sources
			val path = businessBasePath
			val gen = new EcoreInterface(gc)
			gc.ecores.forEach[g|{
				val graphPath = path+g.apiPath.toString;
				clearDirectory(graphPath)
				g.EClassifiers.filter(EClass).forEach[t|{
					generateJavaFile(graphPath,
						gen.filename(t),
						gen.content(t,g)
					)
				}]
				g.EClassifiers.filter(EEnum).forEach[t|{
					generateJavaFile(graphPath,
						gen.filename(t),
						gen.contentEnum(t,g)
					)
				}]
				
			}]
		}
		
		//create ecore API Implementation
		{
			//dywa-app.app-business.target.generated-sources
			val path = businessBasePath
			val gen = new EcoreImplementation(gc)
			gc.ecores.forEach[g|{
				val graphPath = path+g.apiImplPath.toString;
				clearDirectory(graphPath)
				g.EClassifiers.filter(EClass).forEach[t|{
					generateJavaFile(graphPath,
						gen.filename(t),
						gen.content(t,g)
					)
				}]
			}]
		}
		
		
		//create graph model switch
		{
			//dywa-app.app-business.target.generated-sources
			val path = businessBasePath
			val gen = new GraphModelSwitch(gc)
			gc.graphMopdels.forEach[g|{
				val graphPath = path+g.apiPath.toString+"/util";
				clearDirectory(graphPath)
				generateJavaFile(graphPath,
					gen.filename(g),
					gen.content(g)
				)
				
			}]
		}
		
		//create graph model Factory interface
		{
			//dywa-app.app-business.target.generated-sources
			val path = businessBasePath
			val gen = new GraphModelFactoryInterface(gc)
			gc.graphMopdels.forEach[g|{
				val graphPath = path+g.factoryPath.toString;
				generateJavaFile(graphPath,
					gen.filename(g),
					gen.content(g,false)
				)
			}]
		}
		
		//create graph model Factory impl
		{
			//dywa-app.app-business.target.generated-sources
			val path = businessBasePath
			val gen = new GraphModelFactoryImplementation(gc)
			gc.graphMopdels.forEach[g|{
				val graphPath = path+g.apiImplPath.toString;
				generateJavaFile(graphPath,
					gen.filename(g),
					gen.content(g)
				)
			}]
		}
		
		
		
		//create graphmodel rest controller
		{
			//dywa-app.app-business.target.generated-sources.info.scce.pyro.core
			val path = businessBasePath+"info/scce/pyro/core"
			gc.graphMopdels.forEach[g|{
				val staticResourceFiles = new HashMap
				if(g.hasIncludeResourcesAnnotation) {
					g.annotations.filter[name.equals("pyroGeneratorResource")&&!value.isEmpty].forEach[ann|{
						ann.value.forEach[v|{
							staticResourceFiles.put(v,FileHandler.getAllFiles(ann,v))
						}]
					}]
				}
				
				val styles = CincoUtil.getStyles(g, iProject)
				val gen = new GraphModelController(gc)
				generateJavaFile(path,
					gen.filename(g),
					gen.content(g,styles,staticResourceFiles)
				)
			}]
			 {
				val gen = new MainGraphModelController(gc)
				generateJavaFile(path,
					gen.filename(),
					gen.content()
				)
			 }
			 {
				val gen = new MainControllerBundle(gc)
				generateJavaFile(path,
					gen.filename(),
					gen.content()
				)
			 }
			
		}
		
		//create ecore rest controller
		{
			//dywa-app.app-business.target.generated-sources.info.scce.pyro.core
			val path = businessBasePath+"info/scce/pyro/core"
			gc.ecores.forEach[g|{
				val gen = new EcoreController(gc)
				generateJavaFile(path,
					gen.filename(g),
					gen.content(g)
				)
			}]
		}
		
		//create executer
		{
			//dywa-app.app-business.target.generated-sources.info.scce.pyro.core.command
			val path = businessBasePath+"info/scce/pyro/core/command"
			gc.graphMopdels.forEach[g|{
				val styles = CincoUtil.getStyles(g, iProject)
				val gen = new GraphModelCommandExecuter(gc)
				generateJavaFile(path,
					gen.filename(g),
					gen.content(g,styles)
				)
				val gen2 = new GraphModelControllerBundle(gc)
				generateJavaFile(path,
					gen2.filename(g),
					gen2.content(g)
				)
			}]
		}
		//copy mcam modules
		{
			//TODO support the needed API methods for containment and connection constraints #10
//			val path = basePath+"/dywa-app/app-business/src/main/java/"
//			gc.graphMopdels.filter[hasChecks].
//			forEach[g|
//				FileHandler.copyFileOrFolder(g,"src-gen/"+gc.mglExtension.packagePath(g)+"/mcam/modules/checks",path+gc.mglExtension.packagePath(g)+"/mcam/modules")
//			]
		}
		//copy annotated class
		{
			val path = basePath+"/dywa-app/app-business/src/main/java/"
			gc.graphMopdels.map[elementsAndGraphmodel].flatten.map[annotations].flatten.filter[hasClassAnnotation].
			forEach[ann|FileHandler.copyAnnotatedClasses(ann,iProject,path)]
			
			gc.graphMopdels.map[g|CincoUtil.getStyles(g, iProject).styles.filter[!appearanceProvider.nullOrEmpty]].flatten.
			forEach[app|FileHandler.copyAppearanceProviderClasses(app,iProject,path)]
		}
		//copy annotated included generator resources
		{
			val path = basePath+"/dywa-app/app-presentation/src/main/web-resources/static/"
			
			gc.graphMopdels.filter[hasIncludeResourcesAnnotation].
			forEach[g|{
				val graphPath = path + g.name.lowEscapeDart+"/"
				g.annotations.filter[name.equals("pyroGeneratorResource")&&!value.isEmpty].forEach[ann|{
					ann.value.forEach[v|{
						FileHandler.copyFileOrFolder(ann,v,graphPath)
					}]
					
				}]
			}]
		}
		//generate app-business pom
		{
			//dywa-app.app-business
			val path = businessPath
			val gen = new BusinessPomGenerator(gc)
			generateFile(path,
					gen.filename,
					gen.content
				)
		}
		//generate app-presentation pom
		{
			val path = presentationPath
			val gen = new PresentationPomGenerator(gc)
			generateFile(path, gen.filename, gen.content)
		}
		//copy annotated additional JARs
		{
			val path = basePath+"/dywa-app/app-business/repo/info/scce/pyro/external/"
			gc.graphMopdels.filter[hasIncludeJARAnnotation].
			forEach[g|{
				val graphPath = path + g.name.lowEscapeJava+"/"
				g.annotations.filter[name.equals("pyroAdditionalJAR")&&!value.isEmpty].forEach[ann|{
					ann.value.forEach[v|{
						FileHandler.copyFile(ann,v,graphPath+v.jarFilename+"/1.0/",true,v.jarFilename+"-1.0.jar")
					}]
					
				}]
			}]
		}
		

	}
	
	
	
	
	
	
}