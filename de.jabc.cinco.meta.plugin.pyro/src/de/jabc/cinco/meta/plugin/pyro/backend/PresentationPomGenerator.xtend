package de.jabc.cinco.meta.plugin.pyro.backend

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound

class PresentationPomGenerator extends Generatable {
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def filename()'''pom.xml'''
	
	
	def content()
	'''
	<?xml version="1.0" encoding="UTF-8"?>
	<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
		<modelVersion>4.0.0</modelVersion>
	
		<parent>
			<groupId>info.scce.dime</groupId>
			<artifactId>dywa-app</artifactId>
			<version>0.1</version>
			<relativePath>../pom.xml</relativePath>
		</parent>
	
		<artifactId>app-presentation</artifactId>
		<packaging>war</packaging>
	
		<name>${project.artifactId}</name>
	
		<properties>
			<shiro.version>1.2.3</shiro.version>
		</properties>
	
		<dependencies>
			<dependency>
				<groupId>info.scce.dime</groupId>
				<artifactId>app-addon</artifactId>
				<version>${project.version}</version>
			</dependency>
	
			<dependency>
				<groupId>info.scce.dime</groupId>
				<artifactId>app-business</artifactId>
				<version>${project.version}</version>
			</dependency>
	
			<dependency>
				<groupId>de.ls5.dywa</groupId>
				<artifactId>api</artifactId>
				<version>${de.ls5.dywa.version}</version>
				<scope>provided</scope>
			</dependency>
	
			<dependency>
				<groupId>org.tuckey</groupId>
				<artifactId>urlrewritefilter</artifactId>
				<version>4.0.3</version>
			</dependency>
	
			<!-- explicitly declare dependency provided so it's not packaged -->
			<dependency>
				<groupId>de.ls5.dywa</groupId>
				<artifactId>entities</artifactId>
				<version>${de.ls5.dywa.version}</version>
				<scope>provided</scope>
			</dependency>
	
			<dependency>
				<groupId>org.jboss.spec</groupId>
				<artifactId>jboss-javaee-7.0</artifactId>
				<type>pom</type>
				<version>1.0.0.Final</version>
				<scope>provided</scope>
			</dependency>
	
			<dependency>
				<groupId>org.jboss</groupId>
				<artifactId>jboss-vfs</artifactId>
				<version>3.0.1.GA</version>
				<scope>provided</scope>
			</dependency>
		</dependencies>
	
		<profiles>
			<profile>
				<id>autodeploy</id>
				<activation>
					<property>
						<name>wildfly.path</name>
					</property>
				</activation>
				<build>
					<plugins>
						<plugin>
							<groupId>org.apache.maven.plugins</groupId>
							<artifactId>maven-dependency-plugin</artifactId>
							<version>2.4</version>
							<executions>
								<execution>
									<id>copy</id>
									<phase>install</phase>
									<goals>
										<goal>copy</goal>
									</goals>
									<inherited>false</inherited>
									<configuration>
										<artifactItems>
											<artifactItem>
												<groupId>${project.groupId}</groupId>
												<artifactId>${project.artifactId}</artifactId>
												<version>${project.version}</version>
												<type>war</type>
												<overWrite>true</overWrite>
												<outputDirectory>${wildfly.path}/standalone/deployments</outputDirectory>
												<destFileName>app${de.ls5.dywa.instanceId}.war</destFileName>
											</artifactItem>
										</artifactItems>
									</configuration>
								</execution>
							</executions>
						</plugin>
					</plugins>
				</build>
			</profile>
		</profiles>
	
		<build>
			<resources>
				<resource>
					<directory>src/main/resources</directory>
				</resource>
			</resources>
	
			<plugins>
				<!-- This plugin only creates a dummy 'target/generated-resources' directory, so the war-plugin does not complain -->
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-war-plugin</artifactId>
					<version>2.4</version>
					<configuration>
						<webResources>
						«IF !gc.graphMopdels.filter[it.hasIncludeResourcesAnnotation].isEmpty»
							<resource>
								<directory>src/main/web-resources/static</directory>
								<targetPath>asset/static</targetPath>
								<filtering>false</filtering>
							</resource>
						«ENDIF»
							<resource>
								<directory>src/main/webapp/WEB-INF</directory>
								<targetPath>WEB-INF</targetPath>
								<filtering>true</filtering>
							</resource>
						</webResources>
					</configuration>
				</plugin>
	
			</plugins>
			<finalName>${de.ls5.dywa.finalName}${de.ls5.dywa.instanceId}</finalName>
		</build>
	</project>

	'''
}
