package de.jabc.cinco.meta.plugin.pyro.backend.core

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import de.jabc.cinco.meta.plugin.pyro.util.EditorViewPluginRegistry
import de.jabc.cinco.meta.plugin.pyro.util.EditorViewPlugin
import java.util.List

class ProjectController extends Generatable {
	List<EditorViewPlugin> eps

	new(GeneratorCompound gc) {
		super(gc)
		
		eps = new EditorViewPluginRegistry().getPlugins(gc);
	}

	def fileName() '''ProjectController.java'''

	def content() '''	
		package info.scce.pyro.core;
		
		import de.ls5.dywa.generated.controller.info.scce.pyro.core.*;
		import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRight;
		import info.scce.pyro.core.rest.types.PyroProject;
		import info.scce.pyro.core.rest.types.PyroProjectStructure;
		import info.scce.pyro.IPyroController;
		
		import javax.ws.rs.core.Response;
		
		@javax.transaction.Transactional
		@javax.ws.rs.Path("/project")
		public class ProjectController {
		
			@javax.inject.Inject
			private PyroUserController subjectController;
		
		    @javax.inject.Inject
		    private GraphModelController graphModelController;
		
			@javax.inject.Inject
			private PyroProjectController projectController;
			
			@javax.inject.Inject
			private PyroOrganizationController organizationController;
			
			@javax.inject.Inject
		    private PyroOrganizationAccessRightVectorController orgArvController;
			
			@javax.inject.Inject
		    private PyroGraphModelPermissionVectorController permissionController;
		    
		    «FOR g:gc.graphMopdels»
		    @javax.inject.Inject
		    private info.scce.pyro.core.«g.name.fuEscapeJava»Controller «g.name.lowEscapeJava»Controller;
		    «ENDFOR»
		    
		    «FOR g:gc.ecores»
		    @javax.inject.Inject
		    private info.scce.pyro.core.«g.name.fuEscapeJava»Controller «g.name.lowEscapeJava»Controller;
		    «ENDFOR»
		
		    @javax.inject.Inject
			private ProjectService projectService;
		    
			@javax.inject.Inject
			private info.scce.pyro.rest.ObjectCache objectCache;
				
			@javax.ws.rs.POST
			@javax.ws.rs.Path("create/private")
			@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
			@org.jboss.resteasy.annotations.GZIP
			public javax.ws.rs.core.Response create(PyroProject newProject) {
		
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
				
				if (newProject.getorganization() == null) {
					return javax.ws.rs.core.Response.status(Response.Status.BAD_REQUEST).build(); 
				}
				
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org = organizationController.read(newProject.getorganization().getDywaId());
				if (org == null) {
					return javax.ws.rs.core.Response.status(Response.Status.NOT_FOUND).build(); 
				}
			
				if (canCreateProject(subject, org)) {
					final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject pp = createProject(
						newProject.getname(),
						newProject.getdescription(),
						subject,
						org
					);
			        
			        return javax.ws.rs.core.Response.ok(PyroProject.fromDywaEntity(pp,objectCache)).build();
				}
				
				return javax.ws.rs.core.Response.status(Response.Status.FORBIDDEN).build(); 
			}
			
			public de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject createProject(
				String name,
				String description,
				de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject,
				de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org
				)
			{
							
				final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject pp = projectController.create("Project_"+name);
				pp.setowner(subject);
				pp.setname(name);
				pp.setdescription(description);
				pp.setorganization(org);
				subject.getownedProjects_PyroProject().add(pp);
				org.getprojects_PyroProject().add(pp);
						        
				projectService.createDefaultGraphModelPermissionVectors(pp);
				projectService.createDefaultEditorGrid(pp);
						        
				java.util.Set<IPyroController> bundles = new java.util.HashSet<>();
				«FOR g:gc.graphMopdels»
					bundles.add(«g.name.lowEscapeJava»Controller);
				«ENDFOR»
				«FOR g:gc.ecores»
					bundles.add(«g.name.lowEscapeJava»Controller);
				«ENDFOR»
						        
				«FOR a:gc.projectPostCreate.indexed»
					«a.value» hook«a.key» = new «a.value»();
					hook«a.key».init(bundles); 
					hook«a.key».execute(pp);
				«ENDFOR»
				return pp;
			}
						
		    @javax.ws.rs.POST
		    @javax.ws.rs.Path("update/private")
		    @javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		    @javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		    @org.jboss.resteasy.annotations.GZIP
		    public javax.ws.rs.core.Response updateProject(PyroProject ownedProject) {
		
		        final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
		
		        final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject pp = projectController.read(ownedProject.getDywaId());
		        graphModelController.checkPermission(pp);
		                
		        if(canEditProject(subject, pp)){
		            pp.setdescription(ownedProject.getdescription());
		            pp.setname(ownedProject.getname());
		            
		            // set new owner
		            if (pp.getorganization().getowners_PyroUser().contains(subject) 
		            		|| subject.getsystemRoles_PyroSystemRole().size() > 0
		            		|| pp.getowner().equals(subject)) {
		                final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser newOwner = subjectController.read(ownedProject.geowner().getDywaId());
		                if (newOwner == null) return javax.ws.rs.core.Response.status(Response.Status.NOT_FOUND).build();
		                if (!isInOrganization(newOwner, pp.getorganization())) return javax.ws.rs.core.Response.status(Response.Status.BAD_REQUEST).build();               
		                pp.getowner().getownedProjects_PyroProject().remove(pp);
		                pp.setowner(newOwner);
		                newOwner.getownedProjects_PyroProject().add(pp);
		            }
		            
		            return javax.ws.rs.core.Response.ok(PyroProject.fromDywaEntity(pp,objectCache)).build();
		        } 
		            
		        return javax.ws.rs.core.Response.status(Response.Status.FORBIDDEN).build(); 
		    }
		    
		    @javax.ws.rs.GET
		    @javax.ws.rs.Path("/{projectId}")
		    @javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		    @org.jboss.resteasy.annotations.GZIP
		    public javax.ws.rs.core.Response getProject(@javax.ws.rs.PathParam("projectId") final long projectId) {
		        final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
		        final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project = projectController.read(projectId);
		
		        graphModelController.checkPermission(project);
		       
		        if(isInOrganization(subject, project.getorganization())){
		            return javax.ws.rs.core.Response.ok(PyroProject.fromDywaEntity(project, objectCache)).build();
		        }
		        
		        return javax.ws.rs.core.Response.status(Response.Status.FORBIDDEN).build();
		    }
		
		    @javax.ws.rs.GET
		    @javax.ws.rs.Path("structure/{id}/private")
		    @javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		    @org.jboss.resteasy.annotations.GZIP
		    public javax.ws.rs.core.Response loadProjectStructure(@javax.ws.rs.PathParam("id") final long id) {
		
		        final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
		    
		        final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject pp = projectController.read(id);
		
		        graphModelController.checkPermission(pp);
		       
		        if(isInOrganization(subject, pp.getorganization())){
		            return javax.ws.rs.core.Response.ok(PyroProjectStructure.fromDywaEntity(pp,objectCache)).build();
		        }
		        
		        return javax.ws.rs.core.Response.status(Response.Status.FORBIDDEN).build();
		    }
		
		    @javax.ws.rs.GET
		    @javax.ws.rs.Path("remove/{id}/private")
		    @org.jboss.resteasy.annotations.GZIP
		    public javax.ws.rs.core.Response removeProject(@javax.ws.rs.PathParam("id") final long id) {
		        final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
		        
		        final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project = projectController.read(id);
		        if (canDeleteProject(subject, project)) {		        	
		        	projectService.deleteById(subject, id);
		            return javax.ws.rs.core.Response.ok("Removed").build();
		        }
		        
		        return javax.ws.rs.core.Response.status(Response.Status.FORBIDDEN).build();
		        
		    }
		    
		    private boolean isInOrganization(
		    		de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
		    		de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org) {
		    	return org.getmembers_PyroUser().contains(user) || org.getowners_PyroUser().contains(user);
		    }
		
		    private boolean canCreateProject(
		    		de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
		    		de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org) {
		    	de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector arv = getAccessRightVector(user, org);
		    	return arv != null && arv.getaccessRights_PyroOrganizationAccessRight().contains(PyroOrganizationAccessRight.CREATE_PROJECTS);
		    };
		    
		    private boolean canEditProject(
		    		de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
		    		de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project) {
		    	de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector arv = getAccessRightVector(user, project);
		    	return arv != null && arv.getaccessRights_PyroOrganizationAccessRight().contains(PyroOrganizationAccessRight.EDIT_PROJECTS);
		    };
		    
		    private boolean canDeleteProject(
		    		de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
		    		de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project) {
		    	de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector arv = getAccessRightVector(user, project);
		    	return arv != null && arv.getaccessRights_PyroOrganizationAccessRight().contains(PyroOrganizationAccessRight.DELETE_PROJECTS);
		    };
		    
		    private de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector getAccessRightVector(
		    		de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
		    		de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project
		    		) {
				return getAccessRightVector(user, project.getorganization());
		    }
		    
		    private de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector getAccessRightVector(
		    		de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user,
		    		de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization org
		    		) {
		    	final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector searchObject = orgArvController.createSearchObject(null);
				searchObject.setuser(user);
				searchObject.setorganization(org);
				
				final java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganizationAccessRightVector> result = orgArvController.findByProperties(searchObject);
				return result.size() == 1 ? result.get(0) : null;
		    }
		}
		
	'''
}
