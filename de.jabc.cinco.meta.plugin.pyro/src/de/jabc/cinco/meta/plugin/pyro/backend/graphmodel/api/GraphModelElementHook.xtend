package de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api

import mgl.GraphModel
import mgl.ModelElement
import de.jabc.cinco.meta.plugin.pyro.util.MGLExtension
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound

class GraphModelElementHook {
	
	protected extension MGLExtension mglExtension
	
	def postCreate(ModelElement em,GraphModel g,String elementName,GeneratorCompound gc,boolean isTransient) {
		mglExtension = gc.mglExtension
		'''
		«IF em.hasPostCreateHook»
		//post create
		«em.postCreateHook» hook = new «em.postCreateHook»();
			«IF !isTransient»
			hook.init(cmdExecuter);
			«ENDIF»
		hook.postCreate(«elementName»);
		«ENDIF»
		'''
	}
}