  package de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api.impl

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import de.jabc.cinco.meta.plugin.pyro.util.MGLExtension
import mgl.Attribute
import mgl.ContainingElement
import mgl.Edge
import mgl.GraphModel
import mgl.GraphicalModelElement
import mgl.ModelElement
import mgl.Node
import mgl.NodeContainer
import mgl.UserDefinedType
import style.NodeStyle
import style.Styles
import mgl.ComplexAttribute
import de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.api.GraphModelElementHook

class GraphModelElementTransientImplementation extends Generatable {
	
	protected extension GraphModelElementHook = new GraphModelElementHook
	
	new(GeneratorCompound gc) {
		super(gc)
	}
		
	def filename(ModelElement me)'''«me.name.fuEscapeJava»Impl.java'''
	
	def content(ModelElement me,GraphModel g,Styles styles)
	{
	'''
	package «g.apiImplFQN»;
	
	public class «me.name.fuEscapeJava»Impl implements «g.apiFQN».«me.name.fuEscapeJava» {
		
		final String id;
		«IF me instanceof GraphModel»
		final java.util.List<graphmodel.Edge> edges = new java.util.LinkedList<>();
		«ENDIF»
		«IF me instanceof ContainingElement»
		final java.util.List<graphmodel.ModelElement> modelElements = new java.util.LinkedList<>();
		«ENDIF»
		«IF me instanceof GraphicalModelElement»
		graphmodel.ModelElementContainer container;
		«ENDIF»
		«IF me instanceof Edge»
		graphmodel.Node source;
		graphmodel.Node target;
		final java.util.List<info.scce.pyro.trans.BendingPoint> bendingPoints = new java.util.LinkedList<>();
		«ENDIF»
		«IF me instanceof Node»
		int x = 0;
		int y = 0;
		int width = 0;
		int height = 0;
		final java.util.List<graphmodel.Edge> incoming = new java.util.LinkedList<>();
		final java.util.List<graphmodel.Edge> outgoing = new java.util.LinkedList<>();
			«IF me.isPrime»
				«me.primeReference.type.graphModel.apiFQN».«me.primeReference.type.name.fuEscapeJava» «me.primeReference.name.escapeJava»;
			«ENDIF»
		«ENDIF»
		
		«FOR attr:me.attributesExtended»
			«IF attr.isList»java.util.List<«ENDIF»«IF !attr.isPrimitive(g)»«g.apiFQN».«ENDIF»«attr.javaType(g)»«IF attr.isList»>«ENDIF» «attr.name.escapeJava»«IF attr.isList» =  new LinkedList<>()«ENDIF»;
		«ENDFOR»
		
		
	    public «me.name.fuEscapeJava»Impl(«IF me instanceof GraphicalModelElement»graphmodel.ModelElementContainer container«ENDIF») {
	    	«IF me instanceof GraphicalModelElement»
	    	this.container = container;
	    	«ENDIF»
	    	this.id = org.apache.commons.lang3.RandomStringUtils.random(10, true, false);
	    	«FOR attr:me.attributesExtended.filter[isPrimitive(g)]»
					«IF attr.type.getEnum(g)!==null»
					this.«attr.name.escapeJava» = «attr.type.getEnum(g).literals.get(0).toUnderScoreCase»;
					«ELSE»
					this.«attr.name.escapeJava» = «attr.type.getPrimitiveDefault(attr)»;
					«ENDIF»
			«ENDFOR»
	    }
	    
		@Override
		public String getId() {
			return id;
		}
		
		@Override
		public de.ls5.dywa.generated.entity.info.scce.pyro.core.«me.baseTypeName» getDelegate() {
			return null;
		}
		«IF me instanceof GraphModel»
		«me.embeddedEdges»
		@Override
		public void save() {}
				
		@Override
		public void deleteModelElement(graphmodel.ModelElement cme) {
			«FOR e:g.elements»
			if(cme instanceof «g.apiFQN».«e.name.fuEscapeJava») {
				((«g.apiFQN».«e.name.fuEscapeJava»)cme).delete();
			}
			«ENDFOR»
		}
		«ENDIF»
		«IF me instanceof GraphicalModelElement»
		
	    @Override
	    public «g.apiFQN».«g.name.fuEscapeJava» getRootElement() {
	    	if(this.getContainer() instanceof «g.apiFQN».«g.name.fuEscapeJava»){
	    		return («g.apiFQN».«g.name.fuEscapeJava») this.getContainer();
	    	}
	    	return («g.apiFQN».«g.name.fuEscapeJava») ((graphmodel.Container)this.getContainer()).getRootElement();
	    }
	    
	    @Override
	    public «me.getBestContainerSuperTypeNameAPI(g.apiFQN).escapeJava» getContainer() {
	        return («me.getBestContainerSuperTypeNameAPI(g.apiFQN).escapeJava»)this.container;
	    }
		«ENDIF»
		«IF me instanceof Edge»
		@Override
		public void delete() {
			«'''
				this.source.getOutgoing().remove(this);
				this.target.getIncoming().remove(this);
				this.getRootElement().getModelElements().remove(this);
			'''.deleteHooks(me)»
		}
		
		@Override
		public graphmodel.Node getSourceElement() {
			return source;
		}
		
		@Override
		public graphmodel.Node getTargetElement() {
			return target;
		}
		
		@Override
		public void reconnectSource(graphmodel.Node node) {
			this.source.getOutgoing().remove(this);
			this.source = node;
			this.source.getOutgoing().add(this);
		}
		
		@Override
		public void reconnectTarget(graphmodel.Node node) {
			this.target.getIncoming().remove(this);
			this.target = node;
			this.target.getIncoming().add(this);
		}
		
		@Override
		public void addBendingPoint(int x, int y) {
			bendingPoints.add(new info.scce.pyro.trans.BendingPoint(x,y));
		}
		«ENDIF»
		«IF me instanceof Node»
			@Override
			public void delete() {
				«'''
				«IF me instanceof NodeContainer»
				getModelElements().stream().filter(n->n instanceof graphmodel.Node).forEach(n->((graphmodel.Node)n).delete());
				«ENDIF»
				java.util.Set<graphmodel.Edge> edges = new java.util.HashSet<>();
				edges.addAll(getIncoming());
				edges.addAll(getOutgoing());
				edges.forEach(graphmodel.Edge::delete);
				this.container.getModelElements().remove(this);
				'''.deleteHooks(me)»
			}
			
			@Override
		    public int getX() {
		        return x;
		    }
		
		    @Override
		    public int getY() {
		        return y;
		    }
		
		    @Override
		    public int getWidth() {
		        return width;
		    }
		
		    @Override
		    public int getHeight() {
		        return height;
		    }
		
		    @Override
		    public java.util.List<graphmodel.Edge> getIncoming() {
		    	return incoming;
		    }
		
		    @Override
		    public <T extends graphmodel.Edge> java.util.List<T> getIncoming(Class<T> clazz) {
		        return getIncoming().stream().filter(n->clazz.isInstance(n)).map(n->clazz.cast(n)).collect(java.util.stream.Collectors.toList());
		    }
		
		    @Override
		    public java.util.List<graphmodel.Node> getPredecessors() {
		        return getIncoming().stream().map(n->n.getSourceElement()).collect(java.util.stream.Collectors.toList());
		    }
		
		    @Override
		    public <T extends graphmodel.Node> java.util.List<T> getPredecessors(Class<T> clazz) {
		       return getPredecessors().stream().filter(n->clazz.isInstance(n)).map(n->clazz.cast(n)).collect(java.util.stream.Collectors.toList());
		    }
		
		    @Override
		    public java.util.List<graphmodel.Edge> getOutgoing() {
		    	return outgoing;
		    }
		
		    @Override
		    public <T extends graphmodel.Edge> java.util.List<T> getOutgoing(Class<T> clazz) {
		       return getOutgoing().stream().filter(n->clazz.isInstance(n)).map(n->clazz.cast(n)).collect(java.util.stream.Collectors.toList());
		    }
		
		    @Override
		    public java.util.List<graphmodel.Node> getSuccessors() {
		        return getOutgoing().stream().map(n->n.getTargetElement()).collect(java.util.stream.Collectors.toList());
		    }
		
		    @Override
		    public <T extends graphmodel.Node> java.util.List<T> getSuccessors(Class<T> clazz) {
		        return getSuccessors().stream().filter(n->clazz.isInstance(n)).map(n->clazz.cast(n)).collect(java.util.stream.Collectors.toList());
		    }
		
		    @Override
		    public void move(int x, int y) {
				this.moveTo(this.getContainer(),x,y);
		    }
		
		    @Override
		    public void moveTo(graphmodel.ModelElementContainer container,int x, int y) {
		    	«IF me.hasPostMove»
		    	graphmodel.ModelElementContainer preContainer = this.getContainer();
		    	int preX = this.getX();
		    	int preY = this.getY();
		    	«ENDIF»
		    	this.container.getModelElements().remove(this);
		    	container.getModelElements().add(this);
		    	this.container = container;
		    	this.x = x;
		    	this.y = y;
		    	«IF me.hasPostMove»
		    	//post move
		    	«me.postMoveHook» hook = new «me.postMoveHook»();
		    	hook.init(cmdExecuter);
		    	hook.postMove(this,preContainer,container,x,y,x-preX,y-preY);
		    	«ENDIF»
		    }
		
		    @Override
		    public void resize(int width, int height) {
				this.width = width;
				this.height = height;
				«IF me.hasPostResize»
				//post resize
				«me.postResizeHook» hook = new «me.postResizeHook»();
				hook.init(cmdExecuter);
				hook.postResize(this,width,height);
				«ENDIF»
		    }
		
			«IF me.isPrime»
			@Override
			public «me.primeReference.type.graphModel.apiFQN».«me.primeReference.type.name.fuEscapeJava» get«me.primeReference.name.fuEscapeJava»()
			{
				return «me.primeReference.name.escapeJava»;
			}
			«ENDIF»
		«connectedNodeMethods(me,g)»
		«ENDIF»
		«IF me instanceof ContainingElement»
		
		@Override
		public java.util.List<graphmodel.ModelElement> getModelElements() {
			return modelElements;
		}
			
		@Override
		public <T extends graphmodel.ModelElement> java.util.List<T> getModelElements(Class<T> clazz) {
			return this.getModelElements().stream().filter(n->clazz.isInstance(n)).map(n->clazz.cast(n)).collect(java.util.stream.Collectors.toList());
		}
				
		private java.util.List<graphmodel.ModelElement> getAllModelElements(graphmodel.ModelElementContainer cmc) {
			java.util.List<graphmodel.ModelElement> cm = new java.util.LinkedList<>(cmc.getModelElements());
			cm.addAll(cmc.getModelElements().stream().filter(n->n instanceof graphmodel.ModelElementContainer).flatMap(n->getAllModelElements((graphmodel.ModelElementContainer)n).stream()).collect(java.util.stream.Collectors.toList()));
			return cm;
		}
		
		@Override
		public <T extends graphmodel.Edge> java.util.List<T> getEdges(Class<T> clazz) {
			return getModelElements(clazz);
		}
	
		@Override
		public <T extends graphmodel.Node> java.util.List<T> getNodes(Class<T> clazz) {
			return getModelElements(clazz);
		}
	
		@Override
		public java.util.List<graphmodel.Node> getNodes() {
			return getModelElements(graphmodel.Node.class);
		}
			
		@Override
		public java.util.List<graphmodel.Node> getAllNodes() {
			return getAllModelElements(this).stream()
			.filter(n->n instanceof graphmodel.Node)
			.map(n->(graphmodel.Node)n)
			.collect(java.util.stream.Collectors.toList());
		}
			
		@Override
		public java.util.List<graphmodel.Edge> getAllEdges() {
			return getAllModelElements(this).stream()
				.filter(n->n instanceof graphmodel.Edge)
				.map(n->(graphmodel.Edge)n)
				.collect(java.util.stream.Collectors.toList());
		}
		
		@Override
		public java.util.List<graphmodel.Container> getAllContainers() {
			return getAllModelElements(this).stream()
				.filter(n->n instanceof graphmodel.Container)
				.map(n->(graphmodel.Container)n)
				.collect(java.util.stream.Collectors.toList());
		}
		«embeddedNodeMethods(me,g,styles)»
		«ENDIF»
		«FOR attr:me.attributesExtended»
		@Override
		public «IF attr.isList»java.util.List<«ENDIF»«IF !attr.isPrimitive(g)»«g.apiFQN».«ENDIF»«attr.javaType(g)»«IF attr.isList»>«ENDIF» «IF attr.type.equals("EBoolean")»is«ELSE»get«ENDIF»«attr.name.fuEscapeJava»() {
			return «attr.name.escapeJava»;
		}
		
		@Override
		public void set«attr.name.fuEscapeJava»(«IF attr.isList»java.util.List<«ENDIF»«IF !attr.isPrimitive(g)»«g.apiFQN».«ENDIF»«attr.javaType(g)»«IF attr.isList»>«ENDIF» attr) {
			this.«attr.name.escapeJava» = attr;
			«IF me.hasPostAttributeValueChange»
				//property change hook
				org.eclipse.emf.ecore.EStructuralFeature esf = new org.eclipse.emf.ecore.EStructuralFeature();
				esf.setName("«attr.name»");
				«me.postAttributeValueChange» hook = new «me.postAttributeValueChange»();
				hook.init(cmdExecuter);
				if(hook.canHandleChange(this,esf)) {
					hook.handleChange(this,esf);
				}
			«ENDIF»
		}
		«ENDFOR»
	}
	'''
	}
	
	
	
	
	def primitiveGETConverter(Attribute attribute, String string) {
		return switch(attribute.type) {
			case "EInt":'''«IF attribute.list»«string».stream().map(n->Math.toIntExact(n)).collect(java.util.stream.Collectors.toList())«ELSE»Math.toIntExact(«string»)«ENDIF»'''
			case "EBigInteger":'''«IF attribute.list»«string».stream().map(n->Math.toIntExact(n)).collect(java.util.stream.Collectors.toList())«ELSE»Math.toIntExact(«string»)«ENDIF»'''
			case "ELong":'''«IF attribute.list»«string».stream().map(n->Math.toIntExact(n)).collect(java.util.stream.Collectors.toList())«ELSE»Math.toIntExact(«string»)«ENDIF»'''
			case "EByte":'''«IF attribute.list»«string».stream().map(n->Math.toIntExact(n)).collect(java.util.stream.Collectors.toList())«ELSE»Math.toIntExact(«string»)«ENDIF»'''
			case "EShort":'''«IF attribute.list»«string».stream().map(n->Math.toIntExact(n)).collect(java.util.stream.Collectors.toList())«ELSE»Math.toIntExact(«string»)«ENDIF»'''
			default:string
		}
	}
	
	def primitiveSETConverter(Attribute attribute, String string) {
		return switch(attribute.type) {
			case "EInt":'''«IF attribute.list»«string».stream().map(n->Long.valueOf(n)).collect(java.util.stream.Collectors.toList())«ELSE»Long.valueOf(«string»)«ENDIF»'''
			case "EBigInteger":'''«IF attribute.list»«string».stream().map(n->Long.valueOf(n)).collect(java.util.stream.Collectors.toList())«ELSE»Long.valueOf(«string»)«ENDIF»'''
			case "ELong":'''«IF attribute.list»«string».stream().map(n->Long.valueOf(n)).collect(java.util.stream.Collectors.toList())«ELSE»Long.valueOf(«string»)«ENDIF»'''
			case "EByte":'''«IF attribute.list»«string».stream().map(n->Long.valueOf(n)).collect(java.util.stream.Collectors.toList())«ELSE»Long.valueOf(«string»)«ENDIF»'''
			case "EShort":'''«IF attribute.list»«string».stream().map(n->Long.valueOf(n)).collect(java.util.stream.Collectors.toList())«ELSE»Long.valueOf(«string»)«ENDIF»'''
			default:string
		}
	}
	

	
	def embeddedEdges(GraphModel g)
	'''
	«FOR edge:g.edgesTopologically»
	@Override
	public java.util.List<«g.apiFQN».«edge.name.fuEscapeJava»> get«edge.name.fuEscapeJava»s() {
		return this.getModelElements(«g.apiFQN».«edge.name.fuEscapeJava».class);
	}
	«ENDFOR»
	'''
	
	def connectedNodeMethods(Node node,GraphModel g)
	'''
	«FOR incoming:node.possibleIncoming(g)»
	@Override
	public java.util.List<«g.apiFQN».«incoming.name.fuEscapeJava»> getIncoming«incoming.name.fuEscapeJava»s() {
		return getIncoming(«g.apiFQN».«incoming.name.fuEscapeJava».class);
	}
	«ENDFOR»
	«FOR source:node.possibleIncoming(g).map[possibleSources(g)].flatten.toSet»
	@Override
	public java.util.List<«g.apiFQN».«source.name.fuEscapeJava»> get«source.name.fuEscapeJava»Predecessors() {
		return getPredecessors(«g.apiFQN».«source.name.fuEscapeJava».class);
	}
	«ENDFOR»
	«FOR outgoing:node.possibleOutgoing(g)»
	@Override
	public java.util.List<«g.apiFQN».«outgoing.name.fuEscapeJava»> getOutgoing«outgoing.name.fuEscapeJava»s() {
		return getOutgoing(«g.apiFQN».«outgoing.name.fuEscapeJava».class);
	}
		«IF !outgoing.isIsAbstract»
			«FOR target:outgoing.possibleTargets(g)»
				@Override
				public «g.apiFQN».«outgoing.name.fuEscapeJava» new«outgoing.name.fuEscapeJava»(«g.apiFQN».«target.name» target) {
					«outgoing.name.fuEscapeJava»Impl cn = new «outgoing.name.fuEscapeJava»Impl(getRootElement());
					cn.source = this;
					this.outgoing.add(cn);
					cn.target = target;
					target.getIncoming().add(cn);
					this.getRootElement().getModelElements().add(cn);
					«outgoing.postCreate(g,"cn",gc,true)»
					return cn;
				}
			«ENDFOR»
		«ENDIF»
	«ENDFOR»
	«FOR target:node.possibleOutgoing(g).map[possibleTargets(g)].flatten.toSet»
		@Override
		public java.util.List<«g.apiFQN».«target.name.fuEscapeJava»> get«target.name.fuEscapeJava»Successors() {
			return getSuccessors(«g.apiFQN».«target.name.fuEscapeJava».class);
		}
	«ENDFOR»
	'''
	
	
	def embeddedNodeMethods(ContainingElement ce,GraphModel g,Styles styles)
	'''
	«FOR em:ce.possibleEmbeddingTypes(g)»
	«IF !em.isIsAbstract»
		«IF em.isPrime»
		@Override
		public «g.apiFQN».«em.name.fuEscapeJava» new«em.name.fuEscapeJava»(
			«(em as Node).primeReference.type.graphModel.apiFQN».«(em as Node).primeReference.type.name.fuEscapeJava» object,
			int x,
			int y
		) {
			return new«em.name.fuEscapeJava»(object,x,y,«{
							val nodeStyle = styling(em as Node,styles) as NodeStyle
							val size = nodeStyle.mainShape.size
							 '''
								  	 «IF size!==null»
								  	 «size.width»,
								  	 «size.height»
								  	 «ELSE»
								  	 «MGLExtension.DEFAULT_WIDTH»,
								  	 «MGLExtension.DEFAULT_HEIGHT»
								  	 «ENDIF»
								  	 '''
						}»);
		}
		
		@Override
		public «g.apiFQN».«em.name.fuEscapeJava» new«em.name.fuEscapeJava»(
			«(em as Node).primeReference.type.graphModel.apiFQN».«(em as Node).primeReference.type.name.fuEscapeJava» object,
			int x,
			int y,
			int width,
			int height
		) {
			«em.name.fuEscapeJava»Impl cn = new «em.name.fuEscapeJava»Impl(this);
			modelElements.add(cn);
			cn.x = x;
			cn.y = y;
			cn.width = width;
			cn.height = height;
			cn.«(em as Node).primeReference.name.escapeJava» = object;
			«em.postCreate(g,"cn",gc,true)»
			return cn;
		}
		«ELSE»
		@Override
		public «g.apiFQN».«em.name.fuEscapeJava» new«em.name.fuEscapeJava»(int x, int y, int width, int height) {
			«em.name.fuEscapeJava»Impl cn = new «em.name.fuEscapeJava»Impl(this);
			modelElements.add(cn);
			cn.x = x;
			cn.y = y;
			cn.width = width;
			cn.height = height;
			«em.postCreate(g,"cn",gc,true)»
			return cn;
		}
		@Override
		public «g.apiFQN».«em.name.fuEscapeJava» new«em.name.fuEscapeJava»(int x, int y) {
				return this.new«em.name.fuEscapeJava»(x,y,«{
					val nodeStyle = styling(em as Node,styles) as NodeStyle
					val size = nodeStyle.mainShape.size
					 '''
						  	 «IF size!==null»
						  	 «size.width»,
						  	 «size.height»
						  	 «ELSE»
						  	 «MGLExtension.DEFAULT_WIDTH»,
						  	 «MGLExtension.DEFAULT_HEIGHT»
						  	 «ENDIF»
						  	 '''
				}»);
		}
		«ENDIF»
	«ENDIF»
	@Override
	public java.util.List<«g.apiFQN».«em.name.fuEscapeJava»> get«em.name.fuEscapeJava»s() {
		return getModelElements(«g.apiFQN».«em.name.fuEscapeJava».class);
	}
	
	«ENDFOR»
	
	
	'''
	
	def deleteHooks(CharSequence inner, ModelElement me)'''
	«IF me.hasPreDeleteHook»
	//pre delete
	«me.preDeleteHook» prehook = new «me.preDeleteHook»();
	prehook.init(cmdExecuter);
	prehook.preDelete(this);
	«ENDIF»
	«IF me.hasPostDeleteHook»
	«me.postDeleteHook» posthook = new «me.postDeleteHook»();
	Runnable runnable = posthook.getPostDeleteFunction(this);
	«ENDIF»
	«inner»
	«IF me.hasPostDeleteHook»
	runnable.run();
	«ENDIF»
	'''
	
	
}