package de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.command

import de.jabc.cinco.meta.plugin.pyro.canvas.PyroAppearance
import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import java.util.LinkedHashMap
import java.util.Map
import mgl.Edge
import mgl.GraphModel
import mgl.GraphicalModelElement
import mgl.ModelElement
import mgl.Node
import mgl.NodeContainer
import mgl.UserDefinedType
import style.AbstractShape
import style.BooleanEnum
import style.ConnectionDecorator
import style.ContainerShape
import style.EdgeStyle
import style.NodeStyle
import style.Styles
import mgl.Attribute
import de.jabc.cinco.meta.plugin.pyro.util.MGLExtension

class GraphModelCommandExecuter extends Generatable {
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def filename(GraphModel g)'''«g.name.fuEscapeJava»CommandExecuter.java'''
	
	def content(GraphModel g,Styles styles)
	'''
	package info.scce.pyro.core.command;
	
	import info.scce.pyro.core.graphmodel.BendingPoint;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.ModelElementContainer;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.Node;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject;
	import info.scce.pyro.sync.GraphModelWebSocket;
	
	/**
	 * Author zweihoff
	 */
	public class «g.name.fuEscapeJava»CommandExecuter extends CommandExecuter {
	
	    «g.name.fuEscapeJava»ControllerBundle bundle;
	    
	    public «g.name.fuEscapeJava»ControllerBundle getBundle() {
	    	return bundle;
	    }
	    
	    private info.scce.pyro.rest.ObjectCache objectCache;
	
	    public «g.name.fuEscapeJava»CommandExecuter(
		    	«g.name.fuEscapeJava»ControllerBundle bundle,
		    	PyroUser user,
		    	info.scce.pyro.rest.ObjectCache objectCache,
		    	GraphModelWebSocket graphModelWebSocket,
		    	de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«g.name.fuEscapeJava» graph,
		    	java.util.List<info.scce.pyro.core.command.types.HighlightCommand> highlightings,
		    	PyroProject pyroProject,
		    	info.scce.pyro.core.MainControllerBundle mainControllerBundle
		   	) {
		    super(
		        bundle,
		        new BatchExecution(user,graph),
		        graphModelWebSocket,
		        highlightings,
		        pyroProject,
		        mainControllerBundle
	        );
	        this.bundle = bundle;
	        this.objectCache = objectCache;
	    }
	    
	    public void remove«g.name.escapeJava»(de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«g.name.escapeJava» entity){
			//for complex props
			«FOR attr:g.attributesExtended.filter[!isPrimitive(g)]»
			if(entity.get«attr.name.escapeJava»«IF attr.list»_«attr.type.escapeJava»«ENDIF»()!=null) {
				«IF attr.list»
				entity.get«attr.name.escapeJava»_«attr.type.escapeJava»().forEach(n->remove«attr.type.escapeJava»(n));
				«ELSE»
				remove«attr.type.escapeJava»(entity.get«attr.name.escapeJava»());
				«ENDIF»
			}
			«ENDFOR»
			entity.getmodelElements_ModelElement().clear();
			bundle.«g.name.escapeJava»Controller.deleteWithIncomingReferences(entity);
		}
	    
		«FOR e:g.nodesTopologically.filter[!isIsAbstract]»
			public de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» create«e.name.escapeJava»(long x, long y, long width, long height, ModelElementContainer mec,info.scce.pyro.«g.name.lowEscapeJava».rest.«e.name.fuEscapeJava» prev«IF e.prime»,long primeId«ENDIF»){
			    de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» node = bundle.«e.name.escapeJava»Controller.create("«e.name.escapeJava»");
			    «'''node'''.setDefault(e,g,false)»
			    «IF e.prime»
			    «{
			    	val refType = e.primeReference.type
			    	val refGraph = refType.graphModel
			    	'''
			    		«refGraph.dywaFQN».«refType.name.escapeJava» prime = bundle.«IF g.nodesTopologically.importedPrimeNodes(g).exists[n|n.equals(e.primeReference)]»prime«refGraph.name.escapeJava»«ENDIF»«refType.name.escapeJava»Controller.read(primeId);
			    		node.set«e.primeReference.name.escapeJava»(prime);
			    	'''
			    }»
			    «ENDIF»
			    
			    if(prev != null) {
			    	//create from copy
			    	this.update«e.name.escapeJava»(node,prev,false);
			    }
			    
			    super.createNode("«g.name.lowEscapeDart».«e.name.fuEscapeDart»",node,mec,x,y,width,height
			    		  «IF e.prime»
	    		  			«{
	    		  				val refType = e.primeReference.type
	    		  				val refGraph = refType.graphModel
	    		  		    	'''
	    		  		    		,info.scce.pyro.«refGraph.name.lowEscapeJava».rest.«refType.name.escapeJava».fromDywaEntityProperties(prime,objectCache)
	    		  		    	'''
	    		  			}»
	    		  			«ENDIF»
			    		  ,info.scce.pyro.«g.name.lowEscapeJava».rest.«e.name.fuEscapeJava».fromDywaEntityProperties(node,null));
			   return node;
			}
			
			public de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» create«e.name.escapeJava»(long x, long y, ModelElementContainer mec,info.scce.pyro.«g.name.lowEscapeJava».rest.«e.name.fuEscapeJava» prev«IF e.prime»,long primeId«ENDIF») {
				«{
					val nodeStyle = styling(e,styles) as NodeStyle
					val size = nodeStyle.mainShape.size
					'''return create«e.name.escapeJava»(x,y, «IF size!=null»
								                						  	 «size.width»,
								                						  	 «size.height»
								                						  	 «ELSE»
								                						  	 «MGLExtension.DEFAULT_WIDTH»,
								                						  	 «MGLExtension.DEFAULT_HEIGHT»
								                						  	 «ENDIF»,mec,prev«IF e.prime»,primeId«ENDIF»);'''
				}»
			}
			public void move«e.name.escapeJava»(de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» node, long x, long y, ModelElementContainer mec){
				super.moveNode("«g.name.lowEscapeDart».«e.name.fuEscapeDart»",node,mec,x,y);
			}
			public void resize«e.name.escapeJava»(de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» node, long width, long height){
				super.resizeNode("«g.name.lowEscapeDart».«e.name.fuEscapeDart»",node,width,height);
			}
			public void rotate«e.name.escapeJava»(de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» node, long angle){
				super.rotateNode("«g.name.lowEscapeDart».«e.name.fuEscapeDart»",node,angle);
			}
			public void remove«e.name.escapeJava»(
				de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» entity
				«IF e.prime»
				«{
			    	val refType = e.primeReference.type
			    	val refGraph = refType.graphModel
			    	'''
		    		,«refGraph.dywaFQN».«refType.name.escapeJava» prime
			    	'''
				}»
				«ENDIF»
			){
				//for complex props
				«FOR attr:e.attributesExtended.filter[!isPrimitive(g)]»
					«IF attr.list»
				if(entity.get«attr.name.escapeJava»_«attr.type.escapeJava»()!=null) {
					entity.get«attr.name.escapeJava»_«attr.type.escapeJava»().forEach(n->{
						«FOR sub:attr.type.subTypesAndType(g).filter(ModelElement).filter[!isAbstract] SEPARATOR " else "»
						if(n instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«sub.name.fuEscapeJava») {
							remove«sub.name.escapeJava»((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«sub.name.fuEscapeJava»)n);
						}
						«ENDFOR»
					});
					«ELSE»
				if(entity.get«attr.name.escapeJava»()!=null) {
						«FOR sub:attr.type.subTypesAndType(g).filter(ModelElement).filter[!isAbstract] SEPARATOR " else "»
						if(entity.get«attr.name.escapeJava»() instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«sub.name.fuEscapeJava») {
							remove«sub.name.escapeJava»((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«sub.name.fuEscapeJava»)entity.get«attr.name.escapeJava»());
						}
						«ENDFOR»
					«ENDIF»
				}
				«ENDFOR»
				super.removeNode("«g.name.lowEscapeDart».«e.name.fuEscapeDart»",entity,«IF e.prime»
				«{
					val refType = e.primeReference.type
					val refGraph = refType.graphModel
			    	'''
			    		prime==null?null:(info.scce.pyro.«refGraph.name.lowEscapeJava».rest.«refType.name.escapeJava».fromDywaEntityProperties(prime,objectCache))
			    	'''
				}»
				«ELSE»null«ENDIF»,info.scce.pyro.«g.name.lowEscapeJava».rest.«e.name.fuEscapeJava».fromDywaEntityProperties(entity,null));
			}
	    «ENDFOR»
	    
	    «FOR e:g.edgesTopologically.filter[!isIsAbstract]»
	    	    public de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» create«e.name.escapeJava»(Node source, Node target,java.util.List<BendingPoint> positions, info.scce.pyro.«g.name.lowEscapeJava».rest.«e.name.fuEscapeJava» prev){
	    	        de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» edge = bundle.«e.name.escapeJava»Controller.create("«e.name.escapeJava»");
	    	        «'''edge'''.setDefault(e,g,false)»
	    	        java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.core.BendingPoint> bendingPoints = new java.util.LinkedList<>();
	        		positions.forEach(p->{
	        			de.ls5.dywa.generated.entity.info.scce.pyro.core.BendingPoint bp = bundle.bendingPointController.create("BendingPoint");
	        			bp.setx(p.getx());
	        			bp.sety(p.gety());
	        			bendingPoints.add(bp);
	        		});
	        		if(prev != null) {
	        			//create from copy
	        			this.update«e.name.fuEscapeJava»(edge,prev,false);
	        		}
	    	        super.createEdge("«g.name.lowEscapeDart».«e.name.fuEscapeDart»",edge,source,target,bendingPoints,info.scce.pyro.«g.name.lowEscapeJava».rest.«e.name.fuEscapeJava».fromDywaEntityProperties(edge,null));
	    	    	return edge;
	    	    }
	    	    public void reconnect«e.name.escapeJava»(de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» edge, Node source, Node target){
	    	        super.reconnectEdge("«g.name.lowEscapeDart».«e.name.fuEscapeDart»",edge,source,target);
	    	    }
	    	    public void addBendpoint«e.name.escapeJava»(de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» edge, long x,long y){
	    	    	BendingPoint bp = new BendingPoint();
	    	    	bp.setx(x);
	    	    	bp.sety(y);
	    	    	java.util.List<BendingPoint> bps = edge.getbendingPoints_BendingPoint().stream().map(n->BendingPoint.fromDywaEntity(n)).collect(java.util.stream.Collectors.toList());
	    	    	bps.add(bp);
	    	    	super.updateBendingPoints("«g.name.lowEscapeJava».«e.name.escapeJava»",edge,bps);
	    	    }
	    	    public void update«e.name.escapeJava»(de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» edge, java.util.List<BendingPoint> points){
	    	        super.updateBendingPoints("«g.name.lowEscapeDart».«e.name.fuEscapeDart»",edge,points);
	    	    }
	    	    public void remove«e.name.escapeJava»(de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» entity){
	    	    	//for complex props
	    	    	«FOR attr:e.attributesExtended.filter[!isPrimitive(g)]»
						«IF attr.list»
						if(entity.get«attr.name.escapeJava»_«attr.type.escapeJava»()!=null) {
							entity.get«attr.name.escapeJava»_«attr.type.escapeJava»().forEach(n->{
								«FOR sub:attr.type.subTypesAndType(g).filter(ModelElement).filter[!isAbstract] SEPARATOR " else "»
								if(n instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«sub.name.fuEscapeJava») {
									remove«sub.name.escapeJava»((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«sub.name.fuEscapeJava»)n);
								}
								«ENDFOR»
							});
						«ELSE»
						if(entity.get«attr.name.escapeJava»()!=null) {
								«FOR sub:attr.type.subTypesAndType(g).filter(ModelElement).filter[!isAbstract] SEPARATOR " else "»
								if(entity.get«attr.name.escapeJava»() instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«sub.name.fuEscapeJava») {
									remove«sub.name.escapeJava»((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«sub.name.fuEscapeJava»)entity.get«attr.name.escapeJava»());
								}
								«ENDFOR»
						}
						«ENDIF»
					«ENDFOR»
	    	    	super.removeEdge("«g.name.lowEscapeDart».«e.name.fuEscapeDart»",entity,info.scce.pyro.«g.name.lowEscapeJava».rest.«e.name.fuEscapeJava».fromDywaEntityProperties(entity,null));
	    	    }
	    «ENDFOR»
	    public void updateIdentifiableElement(de.ls5.dywa.generated.entity.info.scce.pyro.core.IdentifiableElement entity,info.scce.pyro.core.graphmodel.IdentifiableElement prev) {
	    	«FOR e:g.elements.filter[!isIsAbstract]+#[g]»
	    	if(entity instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava») {
	    		update«e.name.escapeJava»Properties((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava») entity,(info.scce.pyro.«g.name.lowEscapeJava».rest.«e.name.fuEscapeJava»)prev);
	    		return;
	    	}
	    	«ENDFOR»
	    }
		«FOR e:g.elements.filter[!isIsAbstract]+#[g]»
		public void update«e.name.escapeJava»Properties(de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» entity, info.scce.pyro.«g.name.lowEscapeJava».rest.«e.name.fuEscapeJava» prev) {
			super.updatePropertiesReNew("«g.name.lowEscapeDart».«e.name.fuEscapeDart»",info.scce.pyro.«g.name.lowEscapeJava».rest.«e.name.fuEscapeJava».fromDywaEntityProperties(entity,null),prev);
		}
		«ENDFOR»
	    //FOR NODE EDGE GRAPHMODEL TYPE
		«FOR e:g.elementsAndTypes.filter[!isIsAbstract]+#[g]»
		public de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» update«e.name.escapeJava»(«IF !e.isType»de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» entity, «ENDIF»info.scce.pyro.«g.name.lowEscapeJava».rest.«e.name.escapeJava» update){
	    	return update«e.name.escapeJava»(«IF !e.isType»entity,«ENDIF»update,true);
	    }
		
		
	    public de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» update«e.name.escapeJava»(«IF !e.isType»de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» entity, «ENDIF»info.scce.pyro.«g.name.lowEscapeJava».rest.«e.name.escapeJava» update, boolean propagate){
	        
	        «IF e.isType»
	        de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» entity = null;
	        if(update.getDywaId() > 0L) {
	        	entity = bundle.«e.name.escapeJava»Controller.read(update.getDywaId());
	        }
	        if(entity == null) {
	        	entity = bundle.«e.name.escapeJava»Controller.create("«e.name.escapeJava»");
	        }
	        «'''entity'''.setDefault(e,g,false)»
	        «ELSE»
	        info.scce.pyro.«g.name.lowEscapeJava».rest.«e.name.fuEscapeJava» prev = info.scce.pyro.«g.name.lowEscapeJava».rest.«e.name.fuEscapeJava».fromDywaEntityProperties(entity,null);
	        «ENDIF»
	        
	        //for primitive prop
	        «FOR attr:e.attributesExtended.filter[isPrimitive(g)]»
		        «IF attr.type.getEnum(g)!==null»
		        //for enums
		        «IF attr.list»
		        	if(update.get«attr.name.escapeJava»()!=null) {
		        		entity.set«attr.name.escapeJava»_«attr.type.fuEscapeJava»(update.get«attr.name.escapeJava»().stream().map((n)->bundle.«attr.type.escapeJava»Controller.fetchByName(n.getdywaName()).iterator().next()).collect(java.util.stream.Collectors.toList()));
		        	}
		        «ELSE»
			        if(update.get«attr.name.escapeJava»()!=null) {
			        	entity.set«attr.name.escapeJava»(bundle.«attr.type.escapeJava»Controller.fetchByName(update.get«attr.name.escapeJava»().getdywaName()).iterator().next());
			        }
		        «ENDIF»
		        «ELSE»
		        entity.set«attr.name.escapeJava»(update.get«attr.name.escapeJava»());
		        «ENDIF»
	        «ENDFOR»
	        //for complex prop
	        «FOR attr:e.attributesExtended.filter[!isPrimitive(g)]»
	        	«IF attr.list»
	        	java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«attr.type.escapeJava»> toRemove«attr.name.escapeJava» = entity.get«attr.name.escapeJava»_«attr.type.escapeJava»().stream().filter(a->update.get«attr.name.escapeJava»().stream().noneMatch(b->b.getDywaId()==a.getDywaId())).collect(java.util.stream.Collectors.toList());
	        	toRemove«attr.name.escapeJava».forEach(n->{
	        		«FOR sub:attr.type.subTypesAndType(g).filter(ModelElement).filter[!isAbstract] SEPARATOR " else "»
	        		if(n instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«sub.name.fuEscapeJava») {
	        			entity.getentries_Entry().remove(n);
	        			remove«sub.name.escapeJava»((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«sub.name.fuEscapeJava»)n);
	        		}
	        		«ENDFOR»
	        		
	        	});
	        	this.getControllerBundle().getEntityManager().flush();
	        	for(int i=0;i<update.get«attr.name.escapeJava»().size();i++) {
					info.scce.pyro.core.graphmodel.PyroElement n = update.get«attr.name.escapeJava»().get(i);
					if(n.getDywaId()==-1L) {
						«FOR sub:attr.type.subTypesAndType(g).filter(ModelElement).filter[!isAbstract] SEPARATOR " else "»
		        		if(n instanceof info.scce.pyro.«g.name.lowEscapeJava».rest.«sub.name.fuEscapeJava») {
		        			entity.get«attr.name.escapeJava»_«attr.type.escapeJava»().add(update«sub.name.escapeJava»((info.scce.pyro.«g.name.lowEscapeJava».rest.«sub.name.fuEscapeJava»)n));
		        		}
		        		«ENDFOR»
					} else if(entity.get«attr.name.escapeJava»_«attr.type.escapeJava»().stream().anyMatch(e->e.getDywaId()==n.getDywaId())) {
						«FOR sub:attr.type.subTypesAndType(g).filter(ModelElement).filter[!isAbstract] SEPARATOR " else "»
		        		if(n instanceof info.scce.pyro.«g.name.lowEscapeJava».rest.«sub.name.fuEscapeJava») {
		        			entity.get«attr.name.escapeJava»_«attr.type.escapeJava»().set(i,update«sub.name.escapeJava»((info.scce.pyro.«g.name.lowEscapeJava».rest.«sub.name.fuEscapeJava»)n));
		        		}
		        		«ENDFOR»
					}
				}
				this.getControllerBundle().getEntityManager().flush();
	        	
	        	«ELSE»
	        	de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«attr.type.escapeJava» old«attr.name.escapeJava» = entity.get«attr.name.escapeJava»();
	        	if(old«attr.name.escapeJava»!=null) {
	        		entity.set«attr.name.escapeJava»(null);
	        		«IF !attr.isModelElement(g)»
	        			«FOR sub:attr.type.subTypesAndType(g).filter(ModelElement).filter[!isAbstract] SEPARATOR " else "»
	        			if(old«attr.name.escapeJava» instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«sub.name.fuEscapeJava») {
	        				remove«sub.name.escapeJava»((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«sub.name.fuEscapeJava»)old«attr.name.escapeJava»);
	        			}
	        			«ENDFOR»
	        		«ENDIF»
	        	}
	        	if(update.get«attr.name.escapeJava»()!=null) {
	        		«IF attr.isModelElement(g)»
	        		//fetch entity and reset
	        		de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«attr.type.escapeJava» new«attr.name.escapeJava» = bundle.«attr.type.escapeJava»Controller.read(update.get«attr.name.escapeJava»().getDywaId());
	        		«ELSE»
	        			«FOR sub:attr.type.subTypesAndType(g).filter(ModelElement).filter[!isAbstract] SEPARATOR " else "»
			        		if(update.get«attr.name.escapeJava»() instanceof info.scce.pyro.«g.name.lowEscapeJava».rest.«sub.name.fuEscapeJava») {
				        		//update user defined type
				        		de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«attr.type.escapeJava» new«attr.name.escapeJava» = update«sub.name.escapeJava»((info.scce.pyro.«g.name.lowEscapeJava».rest.«sub.name.fuEscapeJava»)update.get«attr.name.escapeJava»());
				        		entity.set«attr.name.escapeJava»(new«attr.name.escapeJava»);
			        		}
	        			«ENDFOR»
	        		«ENDIF»
	        	}
	        	«ENDIF»
	        «ENDFOR»
	        «IF !e.isType»
	        if(propagate) {
		        super.updateProperties("«g.name.lowEscapeDart».«e.name.fuEscapeDart»",info.scce.pyro.«g.name.lowEscapeJava».rest.«e.name.fuEscapeJava».fromDywaEntityProperties(entity,null),prev);
	        }
	        «ENDIF»
	        return entity;
	    }
	    
	    «IF e instanceof GraphicalModelElement && (e as GraphicalModelElement).hasAppearanceProvider(styles)»
	    private void updateAppearanceProvider«e.name.escapeJava»(de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» entity)
	    {
	    	
	    	«{
	    		val style = (e as GraphicalModelElement).styleFor(styles)
	    		val cl = style.appearanceProvider.substring(1,style.appearanceProvider.length-1)
		    	'''
		    	«cl» app = new «cl»();
		    	«g.apiFQN».«e.name.fuEscapeJava» modelElement = new «g.apiImplFQN».«e.name.fuEscapeJava»Impl(entity,this);
		    	String elementName = "«g.name.lowEscapeDart».«e.name.fuEscapeDart»";
		    	«IF style instanceof EdgeStyle»
			    	style.Appearance root = style.StyleFactory.eINSTANCE.createAppearance();
			    		root.setId("root");
			    		root.setName("«style.name»");
			    		«style.appearance("null","root")»
			    		style.Appearance rootResult = app.getAppearance(modelElement,root.getName());
			    		if(rootResult != null) {
			    			style.Appearance rootMerged = super.mergeAppearance(root,rootResult);
			    			super.updateAppearance(elementName,modelElement,rootMerged);
			    		}
			    	«style.collectMarkupCSSTags().values.join»
		    	«ENDIF»
		    	«IF style instanceof NodeStyle»
			    	«style.mainShape.collectMarkupCSSTags("x",0,"null").values.join»
		    	«ENDIF»
		    	'''
	    	}»
	    }
	    «ENDIF»
		«ENDFOR»
		
		@Override
		public void updateAppearance() {
			super.getAllModelElements().forEach((element)->{
				«FOR e:g.elements.filter[!isIsAbstract].filter[hasAppearanceProvider(styles)] SEPARATOR "else "
				»if(element instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava») {
					updateAppearanceProvider«e.name.escapeJava»((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava»)element);
				}
				«ENDFOR»
			});
		}
	   
	   «FOR e:g.types.filter(UserDefinedType).filter[!isIsAbstract]»
	   public void remove«e.name.escapeJava»(de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«e.name.escapeJava» entity){
	   			//for enums
	   			«FOR attr:e.attributes.filter[isPrimitive(g)].filter[type.getEnum(g)!=null]»
		   			«IF attr.list»
	   			if(entity.get«attr.name.escapeJava»_«attr.type.escapeJava»()!=null){
		   			entity.get«attr.name.escapeJava»_«attr.type.escapeJava»().clear();
		   			«ELSE»
		   		if(entity.get«attr.name.escapeJava»()!=null){
		   			entity.set«attr.name.escapeJava»(null);
		   			«ENDIF»
		   		}
	   			«ENDFOR»
	   	    	//remove all complex fieds
	   	    	«FOR attr:e.attributes.filter[!isPrimitive(g)]»
	   	    		«IF attr.list»
	   	    	if(entity.get«attr.name.escapeJava»_«attr.type.escapeJava»()!=null){
	   	    		java.util.List<de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«attr.type.escapeJava»> cp«attr.name.escapeJava» = new java.util.LinkedList(entity.get«attr.name.escapeJava»_«attr.type.escapeJava»());
	   	    		entity.get«attr.name.escapeJava»_«attr.type.escapeJava»().clear();
	   	    		cp«attr.name.escapeJava».forEach(this::remove«attr.type.escapeJava»);
	   	    		«ELSE»
	   	    	if(entity.get«attr.name.escapeJava»()!=null){
	   	    		de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«attr.type.escapeJava» cp«attr.name.escapeJava» = entity.get«attr.name.escapeJava»();
	   	    		entity.set«attr.name.escapeJava»(null);
	   	    		remove«attr.type.escapeJava»(cp«attr.name.escapeJava»);
	   	    		«ENDIF»
	   	    	}
	   	    	«ENDFOR»
	   	    	bundle.«e.name.escapeJava»Controller.deleteWithIncomingReferences(entity);
	   }
	   «ENDFOR»
	

	}
	
	'''
	
	def dispatch getMainShape(EdgeStyle style) {
		style.decorator
	}
	
	def dispatch getMainShape(NodeStyle style) {
		style.mainShape
	}
	
	
	
	def setDefault(CharSequence s,ModelElement t,GraphModel g,boolean useExecuter)
	'''
	«IF t instanceof GraphModel»
		«s».setscale(1.0);
		«s».setconnector("normal");
		«s».setheight(600L);
		«s».setwidth(2000L);
		«s».setrouter(null);
		«s».setisPublic(false);
	«ENDIF»
	«IF t instanceof Node»
	«s».setwidth(0L);
	«s».setheight(0L);
	«s».setx(0L);
	«s».setangle(0L);
	«s».sety(0L);
		«IF t instanceof NodeContainer»
		«ENDIF»
	«ENDIF»
	«IF t instanceof Edge»
	«ENDIF»
«««	«FOR attr:t.attributes.filter[!isPrimitive(g)].filter[list]»
«««	«s».set«attr.name.escapeJava»(new java.util.LinkedList<>());
«««	«ENDFOR»
	//primitive init
	«FOR attr:t.attributesExtended.filter[isPrimitive(g)]»
		«IF attr.list»
«««		«s».set«attr.name.escapeJava»(new java.util.LinkedList<>());
		«ELSE»
			«IF attr.type.getEnum(g)!=null»
			«s».set«attr.name.escapeJava»(«attr.type.getEnumDefault(g,useExecuter)»);
			«ELSE»
			«s».set«attr.name.escapeJava»(«attr.type.getPrimitiveDefault(attr)»);
			«ENDIF»
		«ENDIF»
	«ENDFOR»
	'''
	
	
	
	
	
	def getEnumDefault(String string,GraphModel g,boolean useExecuter)
	'''«IF useExecuter»executer.getBundle()«ELSE»bundle«ENDIF».«string»Controller.fetchByName("«string.getEnum(g).literals.get(0).toUnderScoreCase»").iterator().next()'''
	
	def Map<AbstractShape,CharSequence> collectMarkupCSSTags(AbstractShape shape,String prefix,int i,String ref){
		val l = new LinkedHashMap
		l.put(shape,shape.markupCSS(prefix,i,ref))
		if(shape instanceof ContainerShape) {
			shape.children.forEach[n,idx|l.putAll(n.collectMarkupCSSTags(i+"x",idx,'''«prefix.tagClass(i)»'''))]			
		}
		return l
	}
	
	def Map<ConnectionDecorator,CharSequence> collectMarkupCSSTags(EdgeStyle style){
		val l = new LinkedHashMap
		style.decorator.forEach[n,idx|l.put(n,n.markupCSS("x",idx,'''null'''))]			
		return l
	}
	
	def tagClass(String s,int i)'''pyro«s»«i»tag'''
	
	def markupCSS(AbstractShape shape,String s,int i,String ref)
	'''
	style.Appearance «s.tagClass(i)» = style.StyleFactory.eINSTANCE.createAppearance();
	«s.tagClass(i)».setId("«s.tagClass(i)»");
	«IF shape.name.nullOrEmpty»
		«s.tagClass(i)».setName("«s.tagClass(i)»");
	«ELSE»
		«s.tagClass(i)».setName("«shape.name.escapeJava»");
	«ENDIF»
	«shape.appearance(ref,s.tagClass(i).toString)»
	style.Appearance «s.tagClass(i)»Result = app.getAppearance(modelElement,«s.tagClass(i)».getName());
	if(«s.tagClass(i)»Result != null) {
		style.Appearance «s.tagClass(i)»Merged = super.mergeAppearance(«s.tagClass(i)»,«s.tagClass(i)»Result);
		super.updateAppearance(elementName,modelElement,«s.tagClass(i)»Merged);
	}
	'''
	
	def markupCSS(ConnectionDecorator shape,String s,int i,String ref)
	'''
	style.Appearance «s.tagClass(i)» = style.StyleFactory.eINSTANCE.createAppearance();
	«s.tagClass(i)».setId("«s.tagClass(i)»");
	«IF shape.name.nullOrEmpty»
		«s.tagClass(i)».setName("«s.tagClass(i)»");
	«ELSE»
		«s.tagClass(i)».setName("«shape.name.escapeJava»");
	«ENDIF»
	«shape.appearance(ref,s.tagClass(i).toString)»
	style.Appearance «s.tagClass(i)»Result = app.getAppearance(modelElement,«s.tagClass(i)».getName());
	if(«s.tagClass(i)»Result != null) {
		style.Appearance «s.tagClass(i)»Merged = super.mergeAppearance(«s.tagClass(i)»,«s.tagClass(i)»Result);
		super.updateAppearance(elementName,modelElement,«s.tagClass(i)»Merged);
	}
	'''
	
	def appearance(AbstractShape shape,String parent,String s){
		if(shape.referencedAppearance!=null){
			return new PyroAppearance(shape.referencedAppearance).appearancePreparing(parent,s)
		}
		if(shape.inlineAppearance!=null){
			return new PyroAppearance(shape.inlineAppearance).appearancePreparing(parent,s)
		}
		return new PyroAppearance().appearancePreparing(parent,s)
	}
	
	def appearance(EdgeStyle shape,String parent,String s){
		if(shape.referencedAppearance!=null){
			return new PyroAppearance(shape.referencedAppearance).appearancePreparing(parent,s)
		}
		if(shape.inlineAppearance!=null){
			return new PyroAppearance(shape.inlineAppearance).appearancePreparing(parent,s)
		}
		return new PyroAppearance().appearancePreparing(parent,s)
	}
	
	def appearance(ConnectionDecorator shape,String parent,String s){
		if(shape.predefinedDecorator!=null) {
			if(shape.predefinedDecorator.referencedAppearance!=null){
				return new PyroAppearance(shape.predefinedDecorator.referencedAppearance).appearancePreparing(parent,s)
			}
			if(shape.predefinedDecorator.inlineAppearance!=null){
				return new PyroAppearance(shape.predefinedDecorator.inlineAppearance).appearancePreparing(parent,s)
			}
		}
		return new PyroAppearance().appearancePreparing(parent,s)
	}
	
	def appearancePreparing(PyroAppearance app,String parent,String name)
	{
		'''
		«name».setAngle(«app.angle»F);
		«name».setFilled(style.BooleanEnum.«IF app.filled==BooleanEnum.FALSE»FALSE«ELSEIF app.filled==BooleanEnum.TRUE»TRUE«ELSE»UNDEF«ENDIF»);
		style.Color foreground«name» = style.StyleFactory.eINSTANCE.createColor();
		«IF !(app.foreground==null)»
			foreground«name».setR(«app.foreground.r»);
			foreground«name».setG(«app.foreground.g»);
			foreground«name».setB(«app.foreground.b»);
		«ENDIF»
		«name».setForeground(foreground«name»);
		style.Color background«name» = style.StyleFactory.eINSTANCE.createColor();
		«IF !(app.background==null)»
			background«name».setR(«app.background.r»);
			background«name».setG(«app.background.g»);
			background«name».setB(«app.background.b»);
		«ENDIF»
		«name».setBackground(background«name»);
		«name».setLineInVisible(«IF app.lineInVisible»true«ELSE»false«ENDIF»);
		style.Font font«name» = style.StyleFactory.eINSTANCE.createFont();
		«IF app.font!=null»
			font«name».setFontName("«app.font.fontName»");
			font«name».setSize(«app.font.size»);
			font«name».setIsBold(«IF app.font.isIsBold»true«ELSE»false«ENDIF»);
			font«name».setIsItalic(«IF app.font.isIsItalic»true«ELSE»false«ENDIF»);
		«ENDIF»
		«name».setFont(font«name»);
		«name».setLineWidth(«app.lineWidth»);
		«name».setLineStyle(style.LineStyle.«app.lineStyle.toString.toUpperCase»);
		«name».setTransparency(«app.transparency»);
		«name».setParent(«parent»);
		«name».setImagePath(«IF app.imagePath.nullOrEmpty»null«ELSE»«app.imagePath»«ENDIF»);
		'''
		
	}
	
}