package de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.command

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import mgl.GraphModel
import mgl.Enumeration
import mgl.UserDefinedType

class GraphModelControllerBundle extends Generatable {
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def filename(GraphModel g)'''«g.name.fuEscapeJava»ControllerBundle.java'''
	
	def content(GraphModel g)
	'''
	package info.scce.pyro.core.command;
	
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.BendingPointController;
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.EdgeController;
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.NodeController;
	import de.ls5.dywa.generated.util.DomainFileController;
	
	import javax.persistence.EntityManager;
	
	/**
	 * Author zweihoff
	 */
	public class «g.name.fuEscapeJava»ControllerBundle extends ControllerBundle {
	
	de.ls5.dywa.generated.controller.info.scce.pyro.«g.name.escapeJava».«g.name.escapeJava»Controller «g.name.escapeJava»Controller;
	«FOR e:g.elementsAndTypesAndEnums»
	    «IF e instanceof Enumeration || e instanceof UserDefinedType »public «ENDIF»de.ls5.dywa.generated.controller.info.scce.pyro.«g.name.escapeJava».«e.name.fuEscapeJava»Controller «e.name.escapeJava»Controller;
	    
	    public final de.ls5.dywa.generated.controller.info.scce.pyro.«g.name.escapeJava».«e.name.fuEscapeJava»Controller get«e.name.fuEscapeJava»Controller() { return «e.name.escapeJava»Controller; }
	«ENDFOR»
	«FOR pr:g.importetPrimeTypes»
    	«pr.type.graphModel.dywaControllerFQN».«pr.type.name.fuEscapeJava»Controller prime«pr.type.graphModel.name.escapeJava»«pr.type.name.escapeJava»Controller;
    	
    	public final «pr.type.graphModel.dywaControllerFQN».«pr.type.name.fuEscapeJava»Controller getPrime«pr.type.graphModel.name.escapeJava»«pr.type.name.escapeJava»Controller()
    	{ return prime«pr.type.graphModel.name.escapeJava»«pr.type.name.escapeJava»Controller; }
	«ENDFOR»
	«FOR gpr:g.importetPrimeTypes.map[n|n.type.graphModel].filter(GraphModel).filter[gr|!gr.equals(g)].toSet»
		@javax.inject.Inject
		public info.scce.pyro.core.«gpr.name.fuEscapeJava»Controller primeGraph«gpr.name.fuEscapeJava»Controller;
		
		public final info.scce.pyro.core.«gpr.name.fuEscapeJava»Controller getPrimeGraph«gpr.name.fuEscapeJava»Controller() { return primeGraph«gpr.name.fuEscapeJava»Controller; }
	«ENDFOR»
	
	    public «g.name.fuEscapeJava»ControllerBundle(
	            NodeController nodeController,
	            EdgeController edgeController,
	            BendingPointController bendingPointController,
	            DomainFileController domainFileController,
	            EntityManager entityManager,
	            de.ls5.dywa.generated.controller.info.scce.pyro.«g.name.escapeJava».«g.name.escapeJava»Controller «g.name.escapeJava»Controller
        	«FOR e:g.elementsAndTypesAndEnums BEFORE "," SEPARATOR ","»
        	«g.dywaControllerFQN».«e.name.fuEscapeJava»Controller «e.name.escapeJava»Controller
        	«ENDFOR»
        	«FOR pr:g.importetPrimeTypes.toSet BEFORE "," SEPARATOR ","»
        	«pr.type.graphModel.dywaControllerFQN».«pr.type.name.fuEscapeJava»Controller prime«pr.type.graphModel.name.escapeJava»«pr.type.name.escapeJava»Controller
        	«ENDFOR»
        	«FOR gpr:g.importetPrimeTypes.map[n|n.type.graphModel].filter(GraphModel).filter[gr|!gr.equals(g)].toSet BEFORE "," SEPARATOR ","»
    		info.scce.pyro.core.«gpr.name.fuEscapeJava»Controller primeGraph«gpr.name.fuEscapeJava»Controller
    		«ENDFOR»
        ) {
	        super(nodeController, edgeController, bendingPointController, domainFileController, entityManager);
	        this.«g.name.escapeJava»Controller = «g.name.escapeJava»Controller;
	        «FOR e:g.elementsAndTypesAndEnums»
	        	    this.«e.name.escapeJava»Controller = «e.name.escapeJava»Controller;
	        «ENDFOR»
	        «FOR pr:g.importetPrimeTypes»
	                this.prime«pr.type.graphModel.name.escapeJava»«pr.type.name.fuEscapeJava»Controller = prime«pr.type.graphModel.name.escapeJava»«pr.type.name.escapeJava»Controller;
        	«ENDFOR»
        	«FOR gpr:g.importetPrimeTypes.map[n|n.type.graphModel].filter(GraphModel).filter[gr|!gr.equals(g)].toSet»
    			this.primeGraph«gpr.name.fuEscapeJava»Controller = primeGraph«gpr.name.fuEscapeJava»Controller;
    		«ENDFOR»
	    }
	
	
	}
	
	'''
	
}