package de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.controller

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound

class MainGraphModelController extends Generatable {
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def filename()'''MainGraphModelController.java'''
	
	def content()
	'''
	package info.scce.pyro.core;
	
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroFolderController;
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroUserController;
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroBinaryFileController;
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroURLFileController;
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroElementController;
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.ModelElementContainerController;
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroGraphModelPermissionVectorController;
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.NodeController;
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.EdgeController;
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.BendingPointController;
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.IdentifiableElementController;
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroFileController;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.*;
	import de.ls5.dywa.generated.util.DomainFileController;
	import info.scce.pyro.sync.GraphModelWebSocket;
	import info.scce.pyro.sync.ProjectWebSocket;
	import info.scce.pyro.sync.WebSocketMessage;
	import javax.persistence.EntityManager;
	
	
	public class MainGraphModelController implements info.scce.pyro.IPyroController {
		@javax.inject.Inject
		protected ProjectWebSocket projectWebSocket;
		
		public ProjectWebSocket getProjectWebSocket() { return projectWebSocket; }
		
		@javax.inject.Inject
		protected EntityManager entityManager;
		
		public EntityManager getEntityManager() { return entityManager; }
		
		@javax.inject.Inject
		protected GraphModelWebSocket graphModelWebSocket;
		
		public GraphModelWebSocket getGraphModelWebSocket() { return graphModelWebSocket; }
		
		@javax.inject.Inject
		protected GraphModelController graphModelController;
		
		public GraphModelController getGraphModelController() { return graphModelController; }
	
	    @javax.inject.Inject
	    protected PyroFolderController folderController;
	    
	    public PyroFolderController getPyroFolderController() { return folderController; }
	    
	    @javax.inject.Inject
	    protected PyroFileController fileController;
	    
	    public PyroFileController getPyroFileController() { return fileController; }
	    
	    @javax.inject.Inject
	    protected PyroBinaryFileController binaryFileController;
	    
	    public PyroBinaryFileController getPyroBinaryFileController() { return binaryFileController; }
	    
	    @javax.inject.Inject
	    protected PyroURLFileController urlFileController;
	    
	    public PyroURLFileController getPyroURLFileController() { return urlFileController; }
	    
	    @javax.inject.Inject
	    protected DomainFileController domainFileController;
	    
	    public DomainFileController getDomainFileController() { return domainFileController; }
	
		@javax.inject.Inject
		protected PyroUserController subjectController;
		
		public PyroUserController getPyroUserController() { return subjectController; }
		
		@javax.inject.Inject
		protected IdentifiableElementController identifiableElementController;
		
		public IdentifiableElementController getIdentifiableElementController() { return identifiableElementController; }
		
		@javax.inject.Inject
		protected BendingPointController bendingPointController;
		
		public BendingPointController getBendingPointController() { return bendingPointController; }
		
		@javax.inject.Inject
		protected EdgeController edgeController;
		
		public EdgeController getEdgeController() { return edgeController; }
	
		@javax.inject.Inject
		protected NodeController nodeController;
		
		public NodeController getNodeController() { return nodeController; }
		
		@javax.inject.Inject
		protected ModelElementContainerController modelElementContainerController;
		
		public ModelElementContainerController getModelElementContainerController() { return modelElementContainerController; }
	
		@javax.inject.Inject
		protected PyroElementController pyroElementController;
		
		public PyroElementController getPyroElementController() { return pyroElementController; }
		
		@javax.inject.Inject
		protected PyroGraphModelPermissionVectorController permissionController;
		
		public PyroGraphModelPermissionVectorController getPyroGraphModelPermissionVectorController() { return permissionController; }
		
		@javax.inject.Inject
		protected info.scce.pyro.rest.ObjectCache objectCache;
		
		public info.scce.pyro.rest.ObjectCache getObjectCache() { return objectCache; }
				
		«FOR g:gc.graphMopdels»
		@javax.inject.Inject
		«g.dywaControllerFQN».«g.name.escapeJava»Controller «g.name.escapeJava»Controller;
		
		public «g.dywaControllerFQN».«g.name.escapeJava»Controller get«g.name.fuEscapeJava»Controller() { return «g.name.escapeJava»Controller; }
		«ENDFOR»
	}
	'''
	
}