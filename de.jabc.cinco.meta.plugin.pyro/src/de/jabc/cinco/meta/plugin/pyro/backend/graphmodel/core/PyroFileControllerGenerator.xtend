package de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.core

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound

class PyroFileControllerGenerator extends Generatable {
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def filename()'''PyroFileController.java'''
	
	def content()
	'''
	package info.scce.pyro.core;
	
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.*;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroBinaryFile;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroFile;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroFolder;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroTextualFile;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroURLFile;
	import info.scce.pyro.core.rest.types.*;
	import info.scce.pyro.sync.GraphModelWebSocket;
	import info.scce.pyro.sync.ProjectWebSocket;
	import info.scce.pyro.sync.WebSocketMessage;
	import org.apache.tika.config.TikaConfig;
	import org.apache.tika.metadata.Metadata;
	import org.apache.tika.mime.MediaType;
	
	import javax.ws.rs.WebApplicationException;
	import javax.ws.rs.core.Context;
	import javax.ws.rs.core.Response;
	import javax.ws.rs.core.UriInfo;
	import javax.ws.rs.core.CacheControl;
	import java.io.BufferedInputStream;
	import java.io.ByteArrayInputStream;
	import java.io.IOException;
	import java.io.InputStream;
	import java.net.URL;
	import java.nio.charset.StandardCharsets;
	import java.util.LinkedList;
	import java.util.Optional;
	
	@javax.transaction.Transactional
	@javax.ws.rs.Path("/pyrofile")
	public class PyroFileController {
	
	    @javax.inject.Inject
	    private IdentifiableElementController identifiableElementController;
	
	    @javax.inject.Inject
	    private BendingPointController bendingPointController;
	
	    @javax.inject.Inject
	    private de.ls5.dywa.generated.controller.info.scce.pyro.core.GraphModelController graphModelController;
	
	    @javax.inject.Inject
	    private de.ls5.dywa.generated.util.DomainFileController DomainFileController;
	
		@javax.inject.Inject
		private PyroUserController subjectController;
	
	    @javax.inject.Inject
	    private PyroFolderController folderController;
	    
	    @javax.inject.Inject
	    private PyroBinaryFileController binaryFileController;
	    
	    @javax.inject.Inject
	    private PyroTextualFileController textualFileController;
	    
	    @javax.inject.Inject
	    private PyroURLFileController urlFileController;
	    
	    @javax.inject.Inject
	    private de.ls5.dywa.generated.controller.info.scce.pyro.core.PyroFileController pyroFileController;
	
		@javax.inject.Inject
		private PyroProjectController projectController;
	
		@javax.inject.Inject
		private info.scce.pyro.rest.ObjectCache objectCache;
	
	    @javax.inject.Inject
	    private ProjectWebSocket projectWebSocket;
	
	    @javax.inject.Inject
	    private GraphModelWebSocket graphModelWebSocket;
	    
	    «FOR g:gc.graphMopdels»
	    @javax.inject.Inject
	    private de.ls5.dywa.generated.controller.info.scce.pyro.«g.name.escapeJava».«g.name.fuEscapeJava»Controller «g.name.escapeJava»Controller;
	    «ENDFOR»
	
		@javax.ws.rs.POST
		@javax.ws.rs.Path("create/binary/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response createBinary(CreatePyroBinaryFile newFile) {
	
	        //find parent
	        final PyroFolder pf = folderController.read(newFile.getparentId());
	        checkPermission(pf);
	        if(pf==null){
	            return Response.status(Response.Status.NOT_FOUND).build();
	        }
	        final PyroBinaryFile newPBF = binaryFileController.create("PyroBinaryFile_"+newFile.getfile().getFileName());
	        final String filename = newFile.getfile().getFileName();
	        final int dot = filename.lastIndexOf(".");
	
	        newPBF.setfilename(filename.substring(0,dot));
	        newPBF.setextension(filename.substring(dot+1));
	        newPBF.setfile(this.DomainFileController.getFileReference(newFile.getfile().getDywaId()));
	        pf.getfiles_PyroFile().add(newPBF);
	        sendProjectUpdate(pf);
	
			return Response.ok(info.scce.pyro.core.rest.types.PyroBinaryFile.fromDywaEntity(newPBF,objectCache)).build();
		}
		
		@javax.ws.rs.POST
	    @javax.ws.rs.Path("create/blob/private")
	    @javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	    @javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	    @org.jboss.resteasy.annotations.GZIP
	    public Response createBlob(CreatePyroBlobFile newFile) {
	
	        //find parent
	        final PyroFolder pf = folderController.read(newFile.getparentId());
	        checkPermission(pf);
	        if(pf==null){
	            return Response.status(Response.Status.NOT_FOUND).build();
	        }
	        final PyroBinaryFile newPBF = binaryFileController.create("PyroBinaryFile_"+newFile.getname());
	        final String filename = newFile.getname();
	        final int dot = filename.lastIndexOf(".");
	
	        final de.ls5.dywa.generated.util.FileReference reference = this.DomainFileController
	                .storeFile(newFile.getname(),
	                        new ByteArrayInputStream(newFile.getfile().getBytes(StandardCharsets.UTF_8)));
	
	        newPBF.setfilename(filename.substring(0,dot));
	        newPBF.setextension(filename.substring(dot+1));
	        newPBF.setfile(reference);
	        pf.getfiles_PyroFile().add(newPBF);
	        sendProjectUpdate(pf);
	
	        return Response.ok(info.scce.pyro.core.rest.types.PyroBinaryFile.fromDywaEntity(newPBF,objectCache)).build();
	    }
		
		«FOR g:gc.graphMopdels»
		@javax.ws.rs.GET
		@javax.ws.rs.Path("export/«g.fileExtension.lowEscapeJava»/{id}/{path:.+}")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response export«g.name.fuEscapeJava»(@javax.ws.rs.PathParam("id") final long id,@javax.ws.rs.PathParam("path") final String path) {
			
			final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«g.name.fuEscapeJava» graph = «g.name.escapeJava»Controller.read(id);
			if (graph == null) {
			    return Response.status(Response.Status.NOT_FOUND).build();
			}
			PyroFolder pf = getProject(graph);
			checkPermission(pf);
			
			info.scce.pyro.core.export.«g.name.fuEscapeJava»Exporter exporter = new info.scce.pyro.core.export.«g.name.fuEscapeJava»Exporter();
			
			InputStream input = new ByteArrayInputStream(exporter.getContent(graph).getBytes(StandardCharsets.UTF_8));
			final byte[] result;
			
			try {
			    result = org.apache.commons.io.IOUtils.toByteArray(input);
			} catch (IOException e) {
			    throw new WebApplicationException(e);
			}
			CacheControl cc = new CacheControl();
			        cc.setMustRevalidate(true);
			        cc.setNoStore(true);
			        cc.setNoCache(true);
			return Response
			        .ok(result,"text/plain")
			        .cacheControl(cc)
			        .header("Content-Disposition", "attachment; filename=" + path)
			        .build();
		}
		«ENDFOR»
			
		
		@javax.ws.rs.POST
		@javax.ws.rs.Path("create/textual/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response createTextual(CreatePyroTextualFile newFile) {
	
	        //find parent
	        final PyroFolder pf = folderController.read(newFile.getparentId());
	        checkPermission(pf);
	        if(pf==null){
	            return Response.status(Response.Status.NOT_FOUND).build();
	        }
	        final PyroTextualFile newPTF = textualFileController.create("CreatePyroTextualFile_"+newFile.getfilename());
	        newPTF.setfilename(newFile.getfilename());
	        newPTF.setextension(newFile.getextension());
	        newPTF.setcontent("");
	        pf.getfiles_PyroFile().add(newPTF);
	        sendProjectUpdate(pf);
	
	        return Response.ok(info.scce.pyro.core.rest.types.PyroTextualFile.fromDywaEntity(newPTF,objectCache)).build();
	
	
		}
		
		@javax.ws.rs.POST
		@javax.ws.rs.Path("create/url/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response createTextual(CreatePyroURLFile newFile) {
	
	        //find parent
	        final PyroFolder pf = folderController.read(newFile.getparentId());
	        checkPermission(pf);
	        if(pf==null){
	            return Response.status(Response.Status.NOT_FOUND).build();
	        }
	        final PyroURLFile newPTF = urlFileController.create("CreatePyroURLFile_"+newFile.getfilename());
	        newPTF.setfilename(newFile.getfilename());
	        newPTF.setextension(newFile.getextension());
	        newPTF.seturl(newFile.geturl());
	        pf.getfiles_PyroFile().add(newPTF);
	        sendProjectUpdate(pf);
	
	        return Response.ok(info.scce.pyro.core.rest.types.PyroURLFile.fromDywaEntity(newPTF,objectCache)).build();
		}
		
		private void sendProjectUpdate(PyroFolder folder){
	        final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser subject = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
	
	        final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject parent = getProject(folder);
	        projectWebSocket.send(parent.getDywaId(), WebSocketMessage.fromDywaEntity(subject.getDywaId(),PyroProjectStructure.fromDywaEntity(parent,objectCache)));
	
	    }
	
	    void checkPermission(PyroFolder folder) {
	        final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroUser user = subjectController.read((Long)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
	
	        de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project = getProject(folder);
	        if(project.getowner().equals(user)){
	            return;
	        }
	        throw new WebApplicationException(Response.Status.FORBIDDEN);
	    }
	
	    de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject getProject(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroFile graph){
	        PyroFolder so = folderController.createSearchObject("search_folder");
	        so.setfiles_PyroFile(new LinkedList<>());
	        so.getfiles_PyroFile().add(graph);
	        java.util.List<PyroFolder> parents = folderController.findByProperties(so);
	        if(parents.isEmpty()){
	            throw new IllegalStateException("Graph without parent detected");
	        }
	        return getProject(parents.get(0));
	    }
	
	    de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject getProject(PyroFolder folder){
		    if(folder instanceof de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject){
		        return (de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject) folder;
	        }
	        PyroFolder so = folderController.createSearchObject("search_project");
	        so.setinnerFolders_PyroFolder(new LinkedList<>());
	        so.getinnerFolders_PyroFolder().add(folder);
	        java.util.List<PyroFolder> parents = folderController.findByProperties(so);
	        if(parents.isEmpty()){
	            throw new IllegalStateException("Folder without parent detected");
	        }
	        return getProject(parents.get(0));
	    }
	    
	    PyroFolder getParent(PyroFile file) {
	    	PyroFolder so = folderController.createSearchObject("search_project");
	        so.setfiles_PyroFile(new LinkedList<>());
	        so.getfiles_PyroFile().add(file);
	        
	        java.util.List<PyroFolder> parents = folderController.findByProperties(so);
	        if(parents.isEmpty()){
	            throw new IllegalStateException("File without parent detected");
	        }
	        
	        return parents.get(0);
	    }
	
	    @javax.ws.rs.POST
	    @javax.ws.rs.Path("update/file/private")
	    @javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	    @javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	    @org.jboss.resteasy.annotations.GZIP
	    public Response updateFile(UpdatePyroFile file) {
	
	
	        final PyroFile pfile = pyroFileController.read(file.getdywaId());
	        if (pfile == null) {
	            return Response.status(Response.Status.NOT_FOUND).build();
	        }
	        PyroFolder pf = getProject(pfile);
	        checkPermission(pf);
	        pfile.setfilename(file.getfilename());
	        sendProjectUpdate(pf);
	        return Response.ok(file).build();
	
	    }
	
	    @javax.ws.rs.GET
	        @javax.ws.rs.Path("read/projectresource/{id}/{path:.+}")
	        @org.jboss.resteasy.annotations.GZIP
	        public Response readFile(@Context UriInfo ui, @javax.ws.rs.PathParam("id") final long id, @javax.ws.rs.PathParam("path") final String path) {
	    
	            //find parent
	            final PyroFolder pf = projectController.read(id);
	    
	            if(pf==null) {
	                return Response.status(Response.Status.NOT_FOUND).build();
	            }
	    
	    		CacheControl cc = new CacheControl();
		        cc.setMustRevalidate(true);
		        cc.setNoStore(true);
		        cc.setNoCache(true);
	            //checkPermission(pf);
	    
	            //find folder
	            String[] folders = path.split("/");
	            PyroFolder current = pf;
	            for(int i=0;i<folders.length;i++) {
	                //is last part, e.g. the file
	                final int fin_i = i;
	                if(i>=(folders.length-1)) {
	                    Optional<PyroFile> optFile = current.getfiles_PyroFile().stream().filter(n->(n.getfilename()+"."+n.getextension()).equals(folders[fin_i])).findAny();
	                    if(optFile.isPresent()) {
	                        //return file
	                        if(optFile.get() instanceof PyroBinaryFile) {
	                            PyroBinaryFile bFile = ((PyroBinaryFile) optFile.get());
	                            final de.ls5.dywa.generated.util.FileReference reference = bFile.getfile();
	                            final java.io.InputStream stream = this.DomainFileController
	                                    .loadFile(reference);
	                            TikaConfig config = TikaConfig.getDefaultConfig();
	                            Metadata md = new Metadata();
	                            md.set(Metadata.RESOURCE_NAME_KEY, bFile.getfilename()+"."+bFile.getextension());
	                            final byte[] result;
	                            String mime = "text/plain";
	                            try {
	                                MediaType mediaType = config.getMimeRepository().detect(stream,md);
	                                mime = mediaType.toString();
	                                result = org.apache.commons.io.IOUtils.toByteArray(stream);
	                            } catch (java.io.IOException e) {
	                                throw new javax.ws.rs.WebApplicationException(e);
	                            }
	    
	                            return javax.ws.rs.core.Response
	                                    .ok(result, mime)
	                                    .cacheControl(cc)
	                                    .build();
	                        }
	                        if(optFile.get() instanceof PyroURLFile) {
	                            PyroURLFile urlFile = (PyroURLFile)optFile.get();
	                            String url = urlFile.geturl();
	    
	                            if(!url.startsWith("http")){
	                                url = ui.getBaseUri().toString().substring(0,ui.getBaseUri().toString().lastIndexOf("/rest"))+"/"+url;
	                            }
	                            try {
	                                InputStream input = new URL(url).openStream();
	    
	                                TikaConfig config = TikaConfig.getDefaultConfig();
	                                Metadata md = new Metadata();
	                                md.set(Metadata.RESOURCE_NAME_KEY, urlFile.getfilename()+"."+urlFile.getextension());
	                                MediaType mediaType = config.getMimeRepository().detect(new BufferedInputStream(new URL(url).openStream()),md);
	    
	                                final byte[] result;
	                                try {
	                                    result = org.apache.commons.io.IOUtils.toByteArray(input);
	                                } catch (java.io.IOException e) {
	                                    throw new javax.ws.rs.WebApplicationException(e);
	                                }
	                                return javax.ws.rs.core.Response
	                                        .ok(result,mediaType.toString())
	                                        .cacheControl(cc)
	                                        .build();
	                            } catch (IOException e) {
	                                e.printStackTrace();
	                            }
	                        }
	                        if(optFile.get() instanceof PyroTextualFile) {
	                            PyroTextualFile tFile = (PyroTextualFile)optFile.get();
	                            InputStream input = new ByteArrayInputStream(tFile.getcontent().getBytes(StandardCharsets.UTF_8));
	                            InputStream input2 = new ByteArrayInputStream(tFile.getcontent().getBytes(StandardCharsets.UTF_8));
	                            final byte[] result;
	                            String mime = "text/plain";
	    
	                            try {
	                                result = org.apache.commons.io.IOUtils.toByteArray(input);
	    
	                                TikaConfig config = TikaConfig.getDefaultConfig();
	                                Metadata md = new Metadata();
	                                md.set(Metadata.RESOURCE_NAME_KEY, tFile.getfilename()+"."+tFile.getextension());
	                                MediaType mediaType = config.getMimeRepository().detect(input2,md);
	                                mime = mediaType.toString();
	    
	    
	    
	                            } catch (IOException e) {
	                                throw new WebApplicationException(e);
	                            }
	                            return Response
	                                    .ok(result,mime)
	                                    .cacheControl(cc)
	                                    .build();
	                        }
	                        return Response.status(Response.Status.NOT_FOUND).build();
	    
	                    } else {
	                        return Response.status(Response.Status.NOT_FOUND).build();
	                    }
	                } else {
	                    Optional<PyroFolder> optFolder = current.getinnerFolders_PyroFolder().stream().filter(n->n.getname().equals(folders[fin_i])).findAny();
	                    if(optFolder.isPresent()) {
	                        current = optFolder.get();
	                    } else {
	                        return Response.status(Response.Status.NOT_FOUND).build();
	                    }
	                }
	            }
	    
	            return Response.status(Response.Status.BAD_REQUEST).build();
	    
	        }
	
	    @javax.ws.rs.POST
	    @javax.ws.rs.Path("move/{id}/{targetId}/private")
	    @javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	    @org.jboss.resteasy.annotations.GZIP
	    public Response moveFile(@javax.ws.rs.PathParam("id") final long id, 
	    		                 @javax.ws.rs.PathParam("targetId") final long targetId) {
	    	
	    	final PyroFile file = pyroFileController.read(id);
	    	if (file == null) return Response.status(Response.Status.NOT_FOUND).build();
	    	
	    	PyroFolder targetFolder = folderController.read(targetId);
	    	if (targetFolder == null) {
	    		targetFolder = projectController.read(targetId);
	    		if (targetFolder == null) return Response.status(Response.Status.NOT_FOUND).build();
	    	}
	    	
	    	checkPermission(targetFolder);
	    	
	    	for (final PyroFile f: targetFolder.getfiles_PyroFile()) {
	    		if (file.getfilename().equals(f.getfilename())) 
	    			return Response.status(Response.Status.BAD_REQUEST).entity("Name already exists").build();
	    	}
	    		
	    	final PyroFolder parentFolder = getParent(file);
	    	parentFolder.getfiles_PyroFile().remove(file);
	    	targetFolder.getfiles_PyroFile().add(file);
	    	    	
	    	sendProjectUpdate(parentFolder);
	    	sendProjectUpdate(targetFolder);
	    	
	    	return Response.ok().build();
	    }
	    
	       
	    @javax.ws.rs.GET
	    @javax.ws.rs.Path("remove/{id}/{parentId}/private")
	    @javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	    @org.jboss.resteasy.annotations.GZIP
	    public Response removeFile(@javax.ws.rs.PathParam("id") final long id,@javax.ws.rs.PathParam("parentId") final long parentId) {
	
	        //find parent
	        final PyroFolder pf = folderController.read(parentId);
	
	        checkPermission(pf);
	
	        final PyroFile file = pyroFileController.read(id);
	        if(file==null||pf==null){
	            return Response.status(Response.Status.NOT_FOUND).build();
	        }
	        
	        //remove file
	        if(file instanceof PyroBinaryFile) {
	            de.ls5.dywa.generated.util.FileReference fileReference = ((PyroBinaryFile) file).getfile();
	            ((PyroBinaryFile) file).setfile(null);
	            DomainFileController.deleteFile(fileReference);
	        }
	        pf.getfiles_PyroFile().remove(file);
	        pyroFileController.deleteWithIncomingReferences(file);
	        sendProjectUpdate(pf);
	        
	        return Response.ok().build();
	    }
	
	
	}
	
	'''
}