package de.jabc.cinco.meta.plugin.pyro.backend.graphmodel.rest

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import mgl.Attribute
import mgl.ContainingElement
import mgl.Edge
import mgl.Enumeration
import mgl.GraphModel
import mgl.GraphicalModelElement
import mgl.ModelElement
import mgl.Node
import mgl.NodeContainer
import mgl.ReferencedModelElement
import mgl.Type
import mgl.UserDefinedType
import style.Styles

class GraphModelRestTO extends Generatable{
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def filename(Type t)'''«t.name.fuEscapeJava».java'''
	
	def content(ModelElement t,GraphModel g,Styles styles)
	'''
	package info.scce.pyro.«g.name.lowEscapeJava».rest;
	
	/**
	 * Author zweihoff
	 */
	
	@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
	@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
	@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CLASS, property = info.scce.pyro.util.Constants.DYWA_RUNTIME_TYPE)
	public «IF t.isIsAbstract»abstract «ENDIF»class «t.name.fuEscapeJava» «IF t.isExtending»extends«ELSE»implements«ENDIF» «t.restExtending»«IF t.hasToExtendContainer» implements info.scce.pyro.core.graphmodel.Container«ENDIF»
	{
		«t.attributes.map[attributeDeclaration(g)].join("\n")»
		
		private String __type;
		
	    @com.fasterxml.jackson.annotation.JsonProperty("__type")
	    public String get__type() {
	        return this.__type;
	    }
	
	    @com.fasterxml.jackson.annotation.JsonProperty("__type")
	    public void set__type(final String __type) {
	        this.__type = __type;
	    }
	    
	    @com.fasterxml.jackson.annotation.JsonProperty("dywaId")
	    private long dywaId;
	    
	    @Override
    	public long getDywaId() {
    		return dywaId;
    	}
    
    	@Override
    	public void setDywaId(long id) {
    		dywaId = id;
    	}
    	
    	@com.fasterxml.jackson.annotation.JsonProperty("dywaVersion")
    	private long dywaVersion;
	    
    	@Override
    	public long getDywaVersion() {
    		return dywaVersion;
    	}
	    
    	@Override
    	public void setDywaVersion(long version) {
    		dywaVersion = version;
    	}
    	
    	@com.fasterxml.jackson.annotation.JsonProperty("dywaName")
    	private String dywaName;
    		    
	    
    	@Override
    	public String getDywaName() {
    		return dywaName;
    	}
	    
    	@Override
    	public void setDywaName(String name) {
    		dywaName = name;
    	}
		
		«IF t instanceof GraphicalModelElement»
		private info.scce.pyro.core.graphmodel.IdentifiableElement container;
		
	    @com.fasterxml.jackson.annotation.JsonProperty("a_container")
	    public info.scce.pyro.core.graphmodel.IdentifiableElement getcontainer() {
	        return this.container;
	    }
	
	    @com.fasterxml.jackson.annotation.JsonProperty("container")
	    public void setcontainer(final info.scce.pyro.core.graphmodel.IdentifiableElement container) {
	        this.container = container;
	    }
		«ENDIF»
		
		«IF t instanceof Node»
			«IF t.prime»
			«{
				val refElem = t.primeReference.type
				val refGraph = refElem.graphModel
				'''
				protected info.scce.pyro.«refGraph.name.lowEscapeJava».rest.«refElem.name.fuEscapeJava» «t.primeReference.name.escapeJava»;
				
				@com.fasterxml.jackson.annotation.JsonProperty("b_«t.primeReference.name.escapeJava»")
				public info.scce.pyro.«refGraph.name.lowEscapeJava».rest.«refElem.name.fuEscapeJava» get«t.primeReference.name.escapeJava»() {
				    return this.«t.primeReference.name.escapeJava»;
				}
				
				@com.fasterxml.jackson.annotation.JsonProperty("«t.primeReference.name.escapeJava»")
				public void set«t.primeReference.name.escapeJava»(final info.scce.pyro.«refGraph.name.lowEscapeJava».rest.«refElem.name.fuEscapeJava» «t.primeReference.name.escapeJava») {
				    this.«t.primeReference.name.escapeJava» = «t.primeReference.name.escapeJava»;
				}
				'''
			}»
			«ENDIF»
			
			private long x;
			
			    @com.fasterxml.jackson.annotation.JsonProperty("a_x")
			    public long getx() {
			        return this.x;
			    }
			
			    @com.fasterxml.jackson.annotation.JsonProperty("x")
			    public void setx(final long x) {
			        this.x = x;
			    }
			
			    private long y;
			
			    @com.fasterxml.jackson.annotation.JsonProperty("a_y")
			    public long gety() {
			        return this.y;
			    }
			
			    @com.fasterxml.jackson.annotation.JsonProperty("y")
			    public void sety(final long y) {
			        this.y = y;
			    }
			
			    private long angle;
			
			    @com.fasterxml.jackson.annotation.JsonProperty("a_angle")
			    public long getangle() {
			        return this.angle;
			    }
			
			    @com.fasterxml.jackson.annotation.JsonProperty("angle")
			    public void setangle(final long angle) {
			        this.angle = angle;
			    }
			
			    private long width;
			
			    @com.fasterxml.jackson.annotation.JsonProperty("a_width")
			    public long getwidth() {
			        return this.width;
			    }
			
			    @com.fasterxml.jackson.annotation.JsonProperty("width")
			    public void setwidth(final long width) {
			        this.width = width;
			    }
			
			    private long height;
			
			    @com.fasterxml.jackson.annotation.JsonProperty("a_height")
			    public long getheight() {
			        return this.height;
			    }
			
			    @com.fasterxml.jackson.annotation.JsonProperty("height")
			    public void setheight(final long height) {
			        this.height = height;
			    }
			    
			
			    private java.util.List<info.scce.pyro.core.graphmodel.Edge> incoming;
			
			    @com.fasterxml.jackson.annotation.JsonProperty("a_incoming")
			    public java.util.List<info.scce.pyro.core.graphmodel.Edge> getincoming() {
			        return this.incoming;
			    }
			
			    @com.fasterxml.jackson.annotation.JsonProperty("incoming")
			    public void setincoming(final java.util.List<info.scce.pyro.core.graphmodel.Edge> incoming) {
			        this.incoming = incoming;
			    }
			
			    private java.util.List<info.scce.pyro.core.graphmodel.Edge> outgoing;
			
			    @com.fasterxml.jackson.annotation.JsonProperty("a_outgoing")
			    public java.util.List<info.scce.pyro.core.graphmodel.Edge> getoutgoing() {
			        return this.outgoing;
			    }
			
			    @com.fasterxml.jackson.annotation.JsonProperty("outgoing")
			    public void setoutgoing(final java.util.List<info.scce.pyro.core.graphmodel.Edge> outgoing) {
			        this.outgoing = outgoing;
			    }
		«ENDIF»
		«IF t instanceof Edge»
		private info.scce.pyro.core.graphmodel.Node sourceElement;
		
		    @com.fasterxml.jackson.annotation.JsonProperty("a_sourceElement")
		    public info.scce.pyro.core.graphmodel.Node getsourceElement() {
		        return this.sourceElement;
		    }
		
		    @com.fasterxml.jackson.annotation.JsonProperty("sourceElement")
		    public void setsourceElement(final info.scce.pyro.core.graphmodel.Node sourceElement) {
		        this.sourceElement = sourceElement;
		    }
		
		    private info.scce.pyro.core.graphmodel.Node targetElement;
		
		    @com.fasterxml.jackson.annotation.JsonProperty("a_targetElement")
		    public info.scce.pyro.core.graphmodel.Node gettargetElement() {
		        return this.targetElement;
		    }
		
		    @com.fasterxml.jackson.annotation.JsonProperty("targetElement")
		    public void settargetElement(final info.scce.pyro.core.graphmodel.Node targetElement) {
		        this.targetElement = targetElement;
		    }
		
		    private java.util.List<info.scce.pyro.core.graphmodel.BendingPoint> bendingPoints;
		
		    @com.fasterxml.jackson.annotation.JsonProperty("a_bendingPoints")
		    public java.util.List<info.scce.pyro.core.graphmodel.BendingPoint> getbendingPoints() {
		        return this.bendingPoints;
		    }
		
		    @com.fasterxml.jackson.annotation.JsonProperty("bendingPoints")
		    public void setbendingPoints(final java.util.List<info.scce.pyro.core.graphmodel.BendingPoint> bendingPoints) {
		        this.bendingPoints = bendingPoints;
		    }
		
		«ENDIF»
		«IF t instanceof ContainingElement»
		private java.util.List<info.scce.pyro.core.graphmodel.ModelElement> modelElements;
		
		@com.fasterxml.jackson.annotation.JsonProperty("a_modelElements")
		public java.util.List<info.scce.pyro.core.graphmodel.ModelElement> getmodelElements() {
		   return this.modelElements;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("modelElements")
		public void setmodelElements(final java.util.List<info.scce.pyro.core.graphmodel.ModelElement> modelElements) {
		   this.modelElements = modelElements;
		}
		«ENDIF»
		«IF t instanceof GraphModel»
		private Double scale;
		
		    @com.fasterxml.jackson.annotation.JsonProperty("a_scale")
		    public Double getscale() {
		        return this.scale;
		    }
		
		    @com.fasterxml.jackson.annotation.JsonProperty("scale")
		    public void setscale(final Double scale) {
		        this.scale = scale;
		    }
		    
		    private Long width;
		
		    @com.fasterxml.jackson.annotation.JsonProperty("a_width")
		    public Long getwidth() {
		        return this.width;
		    }
		
		    @com.fasterxml.jackson.annotation.JsonProperty("width")
		    public void setwidth(final Long width) {
		        this.width = width;
		    }
		
		    private Long height;
		
		    @com.fasterxml.jackson.annotation.JsonProperty("a_height")
		    public Long getheight() {
		        return this.height;
		    }
		
		    @com.fasterxml.jackson.annotation.JsonProperty("height")
		    public void setheight(final Long height) {
		        this.height = height;
		    }
		    
		    private String filename;
		
		    @com.fasterxml.jackson.annotation.JsonProperty("filename")
		    public String getfilename() {
		        return this.filename;
		    }
		
		    @com.fasterxml.jackson.annotation.JsonProperty("filename")
		    public void setfilename(final String filename) {
		        this.filename = filename;
		    }
		    
		    private String extension;
		
		    @com.fasterxml.jackson.annotation.JsonProperty("extension")
		    public String getextension() {
		        return this.extension;
		    }
		
		    @com.fasterxml.jackson.annotation.JsonProperty("extension")
		    public void setextension(final String extension) {
		        this.extension = extension;
		    }
		
		
		    private String router;
		
		    @com.fasterxml.jackson.annotation.JsonProperty("a_router")
		    public String getrouter() {
		        return this.router;
		    }
		
		    @com.fasterxml.jackson.annotation.JsonProperty("router")
		    public void setrouter(final String router) {
		        this.router = router;
		    }
		
		    private String connector;
		
		    @com.fasterxml.jackson.annotation.JsonProperty("a_connector")
		    public String getconnector() {
		        return this.connector;
		    }
		
		    @com.fasterxml.jackson.annotation.JsonProperty("connector")
		    public void setconnector(final String connector) {
		        this.connector = connector;
		    }
		    
		    private boolean isPublic;
		    
		    @com.fasterxml.jackson.annotation.JsonProperty("a_isPublic")
		    public boolean getisPublic() {
		        return this.isPublic;
		    }
		
		    @com.fasterxml.jackson.annotation.JsonProperty("isPublic")
		    public void setisPublic(final boolean isPublic) {
		        this.isPublic = isPublic;
		    }
		
		«ENDIF»
		«IF !t.isIsAbstract»
	    public static «t.name.fuEscapeJava» fromDywaEntity(final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«t.name.fuEscapeJava» entity, info.scce.pyro.rest.ObjectCache objectCache) {
			«t.parse(g,false,styles)»
	    }
	    public static «t.name.fuEscapeJava» fromDywaEntityProperties(final de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«t.name.fuEscapeJava» entity, info.scce.pyro.rest.ObjectCache objectCache) {
			«t.parse(g,true,styles)»
	    }
	    «ENDIF»
	}
	'''
	
	def parse(ModelElement t,GraphModel g,boolean onlyProperties,Styles styles)
	'''
	if(objectCache!=null&&objectCache.containsRestTo(entity)){
		return objectCache.getRestTo(entity);
	}
	final «t.name.fuEscapeJava» result;
	result = new «t.name.fuEscapeJava»();
	if(objectCache!=null) {
		objectCache.putRestTo(entity, result);
	}
	result.setDywaId(entity.getDywaId());
	result.setDywaName(entity.getDywaName());
	result.setDywaVersion(entity.getDywaVersion());
	result.set__type("«g.name.lowEscapeDart».«t.name.fuEscapeDart»");
    «IF !onlyProperties»
		«IF t instanceof ContainingElement»
			«IF g.elements.filter(GraphicalModelElement).filter[!isIsAbstract].empty»
				result.setmodelElements(new java.util.LinkedList<>());
			«ELSE»
			    result.setmodelElements(entity.getmodelElements_ModelElement().stream().map((n)->{
				    	«FOR containable:g.nodesTopologically.filter[!isIsAbstract] SEPARATOR "else "»
				    	if(n instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«containable.name.fuEscapeJava»){
							return «containable.name.fuEscapeJava».fromDywaEntity((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«containable.name.fuEscapeJava»)n,objectCache);
						}
				        «ENDFOR»
				        «FOR containable:g.edgesTopologically.filter[!isIsAbstract] SEPARATOR "else "»
				    	if(n instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«containable.name.fuEscapeJava»){
							return «containable.name.fuEscapeJava».fromDywaEntity((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«containable.name.fuEscapeJava»)n,objectCache);
						}
				        «ENDFOR»
					throw new IllegalStateException("Unknown Modelelement "+n.getDywaId());
					}).collect(java.util.stream.Collectors.toList()));
			«ENDIF»
		«ENDIF»
		«IF t instanceof GraphicalModelElement»
		«t.serializeContainer("container",g)»
		«ENDIF»
		«IF t instanceof Node»
			result.setwidth(entity.getwidth());
			result.setheight(entity.getheight());
			result.setangle(entity.getangle());
			result.setx(entity.getx());
			result.sety(entity.gety());
			«IF t.prime»
			«t.primeAttributes(g,false)»
			«ENDIF»
			«t.serializeEdges("incoming",g)»
			«t.serializeEdges("outgoing",g)»
		«ENDIF»
		«IF t instanceof Edge»
			result.setbendingPoints(entity.getbendingPoints_BendingPoint().stream().map(n->info.scce.pyro.core.graphmodel.BendingPoint.fromDywaEntity((de.ls5.dywa.generated.entity.info.scce.pyro.core.BendingPoint)n)).collect(java.util.stream.Collectors.toList()));
			«t.serializeConnection("targetElement",g)»
			«t.serializeConnection("sourceElement",g)»
		«ENDIF»
	«ELSE»
		«IF t instanceof Node && (t as Node).prime»
			«(t as Node).primeAttributes(g,true)»
		«ENDIF»
	«ENDIF»
	«IF t instanceof GraphModel»
	result.setscale(entity.getscale());
	result.setwidth(entity.getwidth());
	result.setheight(entity.getheight());
	result.setrouter(entity.getrouter());
	result.setconnector(entity.getconnector());
	result.setfilename(entity.getfilename());
	result.setextension(entity.getextension());
	result.setisPublic(entity.getisPublic());
	«ENDIF»
	//additional attributes
	«t.attributesExtended.map[attributeSerialization(g,onlyProperties)].join("\n")»
    return result;
	'''
	
	def attributeSerialization(Attribute attr,GraphModel g,boolean onlyProperties)
	'''
	«IF attr.isList»
		«IF attr.isPrimitive(g)»
			«IF attr.type.getEnum(g)!=null»
			if(entity.get«attr.name.escapeJava»_«attr.type.fuEscapeJava»()!=null){
				result.set«attr.name.escapeJava»(entity.get«attr.name.escapeJava»_«attr.type.fuEscapeJava»().stream().map(n->info.scce.pyro.core.graphmodel.PyroEnum.fromDywaEntity(n,"«g.name.lowEscapeDart».«attr.type.fuEscapeDart»")).collect(java.util.stream.Collectors.toList()));
			«ELSE»
			if(entity.get«attr.name.escapeJava»()!=null){
				result.set«attr.name.escapeJava»(«IF attr.isList»new java.util.ArrayList<>(«ENDIF»entity.get«attr.name.escapeJava»()«IF attr.isList»)«ENDIF»);
			«ENDIF»
		«ELSE»
		if(entity.get«attr.name.escapeJava»_«attr.type.escapeJava»()!=null){
			result.set«attr.name.escapeJava»(entity.get«attr.name.escapeJava»_«attr.type.escapeJava»().stream().map(n->{
				«FOR sub:attr.type.subTypesAndType(g).filter(ModelElement).filter[!isAbstract] SEPARATOR " else "»
					if(n instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«sub.name.fuEscapeJava») {
						return «sub.name.fuEscapeJava».fromDywaEntity«IF onlyProperties»Properties«ENDIF»((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«sub.name.fuEscapeJava»)n,objectCache);
					}
				«ENDFOR»
				return null;
			}).collect(java.util.stream.Collectors.toList()));
		«ENDIF»
	«ELSE»
		if(entity.get«attr.name.escapeJava»()!=null){
		«IF attr.isPrimitive(g)»
			«IF attr.type.getEnum(g)!==null»
			result.set«attr.name.escapeJava»(info.scce.pyro.core.graphmodel.PyroEnum.fromDywaEntity(entity.get«attr.name.escapeJava»(),"«g.name.lowEscapeDart».«attr.type.fuEscapeDart»"));
			«ELSE»
			result.set«attr.name.escapeJava»(entity.get«attr.name.escapeJava»());
			«ENDIF»
		«ELSE»
		«FOR sub:attr.type.subTypesAndType(g).filter(ModelElement).filter[!isAbstract] SEPARATOR " else "»
			if(entity.get«attr.name.escapeJava»() instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«sub.name.fuEscapeJava») {
				result.set«attr.name.escapeJava»(«sub.name.fuEscapeJava».fromDywaEntity«IF onlyProperties»Properties«ENDIF»((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.escapeJava».«sub.name.fuEscapeJava»)entity.get«attr.name.escapeJava»(),objectCache));
			}
		«ENDFOR»
		«ENDIF»
	«ENDIF»
	}
	'''
	
	def primeAttributes(Node t,GraphModel g, boolean onlyProperties)
	{
		val refElem = t.primeReference.type
		val refGraph = refElem.graphModel
		val sameGraphModelType = refGraph.name.equals(g.name)
		'''
		if(entity.get«t.primeReference.name.escapeJava»()!=null) {
			
			«IF sameGraphModelType»
			if(info.scce.pyro.core.graphmodel.Node.getRootGraphModel(entity.get«t.primeReference.name.escapeJava»()).equals(info.scce.pyro.core.graphmodel.Node.getRootGraphModel(entity))){
				result.set«t.primeReference.name.escapeJava»(
					info.scce.pyro.«refGraph.name.lowEscapeJava».rest.«refElem.name.fuEscapeJava».fromDywaEntity«IF onlyProperties»Properties«ENDIF»(
						entity.get«t.primeReference.name.escapeJava»(),objectCache
					)
				);
			} else {
			«ENDIF»
			result.set«t.primeReference.name.escapeJava»(
					info.scce.pyro.«refGraph.name.lowEscapeJava».rest.«refElem.name.fuEscapeJava».fromDywaEntityProperties(
						entity.get«t.primeReference.name.escapeJava»(),objectCache
					)
				);
			«IF sameGraphModelType»
			}
			«ENDIF»
		}
		'''		
	}
	
	
	def attributeDeclaration(Attribute attr,GraphModel g)
	'''
	private «attr.javaType(g)» «attr.name.escapeJava»;
	
	@com.fasterxml.jackson.annotation.JsonProperty("c_«attr.name.escapeJava»")
	public «attr.javaType(g)» get«attr.name.escapeJava»() {
	    return this.«attr.name.escapeJava»;
	}
	
	@com.fasterxml.jackson.annotation.JsonProperty("«attr.name.escapeJava»")
	public void set«attr.name.escapeJava»(final «attr.javaType(g)» «attr.name.escapeJava») {
	    this.«attr.name.escapeJava» = «attr.name.escapeJava»;
	}
	'''
	
	def javaType(Attribute attribute, GraphModel g){
		var res = ""
		if(attribute.isList){
			res += "java.util.List<"
		}
		if(attribute.isPrimitive(g)){
			if(attribute.isList){
				if(attribute.type.getEnum(g)!=null) {
					res += attribute.getToJavaType(g)
				} else {
					res += attribute.getToJavaType(g).toFirstUpper
				}
			} else {
				res += attribute.getToJavaType(g)
			}
		}
		else{
			res += attribute.type.fuEscapeJava
		}
		if(attribute.isList){
			res += ">"
		}
		res
	}
	
	def getToJavaType(Attribute attr,GraphModel g) {
		if(attr.type.getEnum(g)!=null){
			return "info.scce.pyro.core.graphmodel.PyroEnum"
		}
		switch(attr.type){
			case "EBoolean": {
				if(attr.list){
					return '''Boolean'''
				}
				return '''boolean'''
			}
			case "EInt":{
				if(attr.list){
					return '''Long'''
				}
				return '''Long'''
			} 
			case "EDouble":{
				if(attr.list) {
					return '''Double'''
				}
				return '''Double'''
			}
			case "ELong": {
				if(attr.list){
					return '''Long'''
				}
				return '''Long'''
			} 
			case "EBigInteger":{
				if(attr.list){
					return '''Long'''
				}
				return '''Long'''
			} 
			case "EByte": {
				if(attr.list){
					return '''Long'''
				}
				return '''Long'''
			} 
			case "EShort": {
				if(attr.list){
					return '''Long'''
				}
				return '''Long'''
			} 
			case "EFloat":{
				if(attr.list) {
					return '''Double'''
				}
				return '''Double'''
			}
			case "EBigDecimal": {
				if(attr.list) {
					return '''Double'''
				}
				return '''Double'''
			}
			default: return '''String'''
		
		}
		
	}
	
	def serializeEdges(Node n,String attr,GraphModel g)
	'''
	«IF g.elements.filter(Edge).filter[!isIsAbstract].empty»
		result.set«attr»(new java.util.LinkedList<>());
	«ELSE»
		result.set«attr»(entity.get«attr»_Edge().stream().map((n)->{
		«FOR edge:g.edgesTopologically.filter[!isIsAbstract] »
			            if(n instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«edge.name.fuEscapeJava»){
			                return «edge.name.fuEscapeJava».fromDywaEntity((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«edge.name.fuEscapeJava»)n,objectCache);
			            }
	    «ENDFOR»
			            throw new IllegalStateException("Unknown Modelelement "+n.getDywaId());
			        }).collect(java.util.stream.Collectors.toList()));
    «ENDIF»
	'''
	
	def serializeContainer(GraphicalModelElement n,String attr,GraphModel g)
	'''
	«FOR container:g.nodesTopologically.filter(NodeContainer).filter[!isIsAbstract]+#[g] SEPARATOR "else "»
	if(entity.getcontainer() instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«container.name.fuEscapeJava»){
		 result.set«attr»(«container.name.fuEscapeJava».fromDywaEntity((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«container.name.fuEscapeJava»)entity.getcontainer() ,objectCache));
	}
    «ENDFOR»

	'''
	
	def serializeConnection(Edge e,String attr,GraphModel g)
	'''
	«FOR node:g.nodesTopologically.filter(Node).filter[!isIsAbstract] SEPARATOR "else "»
	if(entity.get«attr»() instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«node.name.fuEscapeJava»){
		 result.set«attr»(«node.name.fuEscapeJava».fromDywaEntity((de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«node.name.fuEscapeJava»)entity.get«attr»(),objectCache));
	}
    «ENDFOR»

	'''
	
	def restExtending(ModelElement element){
		switch element {
			GraphModel: return "info.scce.pyro.core.graphmodel.GraphModel, info.scce.pyro.core.rest.types.IPyroFile"
			NodeContainer: {
				if (element.extends == null) return "info.scce.pyro.core.graphmodel.Container"
				return element.extends.name
			}
			Node: {
				if (element.extends == null) return "info.scce.pyro.core.graphmodel.Node"
				return element.extends.name
			}
			Edge: {
				if (element.extends == null) return "info.scce.pyro.core.graphmodel.Edge"
				return element.extends.name
			}
			UserDefinedType: {
				if (element.extends == null) return "info.scce.pyro.core.graphmodel.PyroElement"
				return element.extends.name
			}
			Enumeration: {
				return ""
			}
		}
		return ""
	}
	
	
}