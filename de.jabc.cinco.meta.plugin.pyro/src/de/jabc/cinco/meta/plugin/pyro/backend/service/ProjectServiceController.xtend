package de.jabc.cinco.meta.plugin.pyro.backend.service

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound

class ProjectServiceController extends Generatable {
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def filename()'''ProjectServiceController.java'''
	
	
	
	def content() {
	'''
	package info.scce.pyro.service;
	
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.*;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.*;
	import info.scce.pyro.IPyroController;
	import java.util.Map;
	import java.util.HashMap;
	
	import javax.ws.rs.core.Response;
	
	@javax.transaction.Transactional
	@javax.ws.rs.Path("/service")
	public class ProjectServiceController {
		
		@javax.inject.Inject
		private PyroUserController subjectController;
	
	    @javax.inject.Inject
	    private GraphModelController graphModelController;
	
		@javax.inject.Inject
		private PyroProjectController projectController;
		
		«FOR s:gc.projectServices»
		@javax.inject.Inject
		private PyroProjectService«s.value.get(1).escapeJava»Controller «s.value.get(1).escapeJava»ServiceController;
		«ENDFOR»
		
		«FOR g:gc.graphMopdels»
	    @javax.inject.Inject
	    private info.scce.pyro.core.«g.name.fuEscapeJava»Controller «g.name.lowEscapeJava»Controller;
	    «ENDFOR»
	    
	    «FOR g:gc.ecores»
	    @javax.inject.Inject
	    private info.scce.pyro.core.«g.name.fuEscapeJava»Controller «g.name.lowEscapeJava»Controller;
	    «ENDFOR»
	    
	    @javax.ws.rs.GET
		@javax.ws.rs.Path("list/{id}/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response list(@javax.ws.rs.PathParam("id") final long id) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project = projectController.read(id);
			
			if(project==null){
			    return Response.status(Response.Status.BAD_REQUEST).build();
			}
			
			java.util.Set<IPyroController> bundles = new java.util.HashSet<>();
			«FOR g:gc.graphMopdels»
				bundles.add(«g.name.lowEscapeJava»Controller);
			«ENDFOR»
			«FOR g:gc.ecores»
				bundles.add(«g.name.lowEscapeJava»Controller);
			«ENDFOR»
			info.scce.pyro.core.rest.types.ProjectServiceList list = new info.scce.pyro.core.rest.types.ProjectServiceList();
			«FOR s:gc.projectServices»
			{
				«s.value.get(0)» service = new «s.value.get(0)»();
				service.init(bundles);
				boolean canExecute = service.canExecute(project.getservices_PyroProjectService());
				if(canExecute) {
					list.getActive().add("«s.value.get(1).escapeJava»");
				}
				boolean isDisabled = service.isDisabled(project.getservices_PyroProjectService());
				if(isDisabled) {
					list.getDisabled().add("«s.value.get(1).escapeJava»");
				}
			}
			«ENDFOR»
			«FOR s:gc.projectActions»
			{
				«s.value.get(0)» service = new «s.value.get(0)»();
				service.init(bundles);
				boolean canExecute = service.canExecute(project);
				if(canExecute) {
					list.getActive().add("«s.value.get(1).escapeJava»");
				}
			}
			«ENDFOR»
			return Response.ok(list).build();
	    }
		
		«FOR s:gc.projectServices»
		@javax.ws.rs.POST
		@javax.ws.rs.Path("trigger/«s.value.get(1).escapeJava»/{id}/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response trigger«s.value.get(1).fuEscapeJava»(@javax.ws.rs.PathParam("id") final long id,info.scce.pyro.service.rest.«s.value.get(1).fuEscapeJava» req) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project = projectController.read(id);
			
			if(project==null){
			    return Response.status(Response.Status.BAD_REQUEST).build();
			}
			
			java.util.Set<IPyroController> bundles = new java.util.HashSet<>();
			«FOR g:gc.graphMopdels»
				bundles.add(«g.name.lowEscapeJava»Controller);
			«ENDFOR»
			«FOR g:gc.ecores»
				bundles.add(«g.name.lowEscapeJava»Controller);
			«ENDFOR»
			
			«s.value.get(0)» service = new «s.value.get(0)»();
			service.init(bundles); 
			Map<String,String> inputs = new HashMap<>();
			«FOR attr:s.value.subList(2,s.value.size)»
			inputs.put("«attr»",req.get«attr.fuEscapeJava»());
			«ENDFOR»
			try {
				boolean isValid = service.isValid(inputs,project.getservices_PyroProjectService());
				if(!isValid) {
					return Response.status(Response.Status.BAD_REQUEST).build();
				}
				PyroProjectService«s.value.get(1).escapeJava» s = «s.value.get(1).escapeJava»ServiceController.create(project.getname()+"«s.value.get(1)»");
				«FOR attr:s.value.subList(2,s.value.size)»
				s.set«attr.fuEscapeJava»(req.get«attr.fuEscapeJava»());
				«ENDFOR»
				service.execute(s);
				project.getservices_PyroProjectService().add(s);
			} catch(Exception e) {
				e.printStackTrace();
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
			return Response.ok().build();
		}
		«ENDFOR»
		
		«FOR s:gc.projectActions»
		@javax.ws.rs.GET
		@javax.ws.rs.Path("triggeraction/«s.value.get(1).escapeJava»/{id}/private")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@org.jboss.resteasy.annotations.GZIP
		public Response triggerAction«s.value.get(1).fuEscapeJava»(@javax.ws.rs.PathParam("id") final long id) {
			final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject project = projectController.read(id);
			
			if(project==null){
			    return Response.status(Response.Status.BAD_REQUEST).build();
			}
			try {
				java.util.Set<IPyroController> bundles = new java.util.HashSet<>();
				«FOR g:gc.graphMopdels»
					bundles.add(«g.name.lowEscapeJava»Controller);
				«ENDFOR»
				«FOR g:gc.ecores»
					bundles.add(«g.name.lowEscapeJava»Controller);
				«ENDFOR»
				«s.value.get(0)» action = new «s.value.get(0)»();
				action.init(bundles);
				if(action.canExecute(project)) {
					action.execute(project);
				}
			} catch(Exception e) {
				e.printStackTrace();
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
			return Response.ok().build();
		}
		«ENDFOR»
	}
	'''
	}
	
}