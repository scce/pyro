package de.jabc.cinco.meta.plugin.pyro.canvas

import de.jabc.cinco.meta.core.utils.CincoUtil
import de.jabc.cinco.meta.plugin.pyro.util.FileGenerator
import de.jabc.cinco.meta.plugin.pyro.util.FileHandler
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import de.jabc.cinco.meta.plugin.pyro.util.MGLExtension
import org.eclipse.core.resources.IProject
import org.eclipse.core.runtime.IPath
import de.jabc.cinco.meta.plugin.pyro.util.Escaper

class Generator extends FileGenerator {
	
	extension MGLExtension mglExtension
	extension Escaper = new Escaper
	
	new(IPath base) {
		super(base)
		
	}
	
	def generator(GeneratorCompound gc,IProject iProject) {
		
		mglExtension = gc.mglExtension
		
		//create shapes
		{
			gc.graphMopdels.forEach[g|{
				//web.asset.js.graphmodel
				val path = "web/js/"+g.name.lowEscapeDart
				val gen = new Shapes(gc,g)
				val styles = CincoUtil.getStyles(g, iProject)
				generateFile(path,
					gen.fileNameShapes(g),
					gen.contentShapes(styles)
				)
			}]
		}
		
		//copy images
		gc.graphMopdels.forEach[g|{
				val styles = CincoUtil.getStyles(g, iProject)
				g.elements
				.filter[!isIsAbstract]
				.map[styling(styles)]
				.map[getImages]
				.flatten
				.forEach[e|FileHandler.copyFile(e,e.path,basePath+"/web/"+e.iconPath(g.name.lowEscapeDart,false).toString.toLowerCase,true)]			
			}
		]
		
		//create controller
		{
			gc.graphMopdels.forEach[g|{
				//web.asset.js.graphmodel.controller
				val path = "web/js/"+g.name.lowEscapeDart
				val gen = new Controller(gc)
				val styles = CincoUtil.getStyles(g, iProject)
				generateFile(path,
					gen.fileNameController,
					gen.contentController(g,styles)
				)
			}]
		}
	}
}