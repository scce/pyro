package de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.canvas.graphs.graphmodel

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import mgl.ContainingElement
import mgl.Edge
import mgl.EdgeElementConnection
import mgl.GraphModel
import mgl.ModelElement
import mgl.Node
import mgl.NodeContainer
import style.Styles
import mgl.GraphicalElementContainment

class GraphmodelComponent extends Generatable {

	new(GeneratorCompound gc) {
		super(gc)
	}

	def fileNameGraphModelCommandGraph(String graphModel) '''«graphModel.lowEscapeDart»_command_graph.dart'''

	def contentGraphModelCommandGraph(GraphModel g,Styles styles) '''
		import 'package:«gc.projectName.escapeDart»/src/model/core.dart' as core;
		import 'package:«gc.projectName.escapeDart»/src/model/«g.name.lowEscapeDart».dart';
		import 'package:«gc.projectName.escapeDart»/src/model/command_graph.dart';
		import 'package:«gc.projectName.escapeDart»/src/model/command.dart';
		
		«FOR pr:g.primeReferencedGraphModels.filter[!equals(g)]»
		//prime referenced graphmodel «pr.name»
		import 'package:«gc.projectName.escapeDart»/src/model/«pr.name.lowEscapeDart».dart' as «pr.name.lowEscapeDart»;
		«ENDFOR»
		«FOR pr:gc.ecores»
		//prime referenced ecore «pr.name»
		import 'package:«gc.projectName.escapeDart»/src/model/«pr.name.lowEscapeDart».dart' as «pr.name.lowEscapeDart»;
		«ENDFOR»
		
		import 'dart:js' as js;
		
		class «g.name.fuEscapeDart»CommandGraph extends CommandGraph{
		
		  «g.name.fuEscapeDart»CommandGraph(core.GraphModel currentGraphModel,List<HighlightCommand> highlightings,{Map jsog}) : super(currentGraphModel,highlightings,jsog:jsog);
		
		
		  @override
		  core.Node execCreateNodeType(String type,core.PyroElement primeElement)
		  {
		    // for each node type
		    «FOR elem : g.nodesTopologically.filter[!isIsAbstract]»
		    	if(type == '«g.name.lowEscapeDart».«elem.name.fuEscapeDart»'){
		    	  var newNode = new «elem.name.fuEscapeDart»();
		    	  «IF elem.prime»
		    	  newNode.«elem.primeReference.name.escapeDart» = primeElement as «IF !elem.primeReference.type.graphModel.equals(g)»«elem.primeReference.type.graphModel.name.lowEscapeDart».«ENDIF»«elem.primeReference.type.name.fuEscapeDart»;
		    	  «ENDIF»
		    	  return newNode;
		    	}
		    «ENDFOR»
		    throw new Exception("Unkown node type ${type}");
		  }
		
		  @override
		  core.Edge execCreateEdgeType(String type)
		  {
		    core.Edge edge;
		     «FOR elem : g.edgesTopologically.filter[!isIsAbstract]»
		     	if(type == '«g.name.lowEscapeDart».«elem.name.fuEscapeDart»')
		     	{
		     	 edge = new «elem.name.fuEscapeDart»();
		     	}
		    «ENDFOR»
		    return edge;
		
		  }
		  
		  @override
		  void execCreateEdgeCommandCanvas(CreateEdgeCommand cmd) {
		      core.ModelElement e = findElement(cmd.delegateId);
		      if(e == null) {
		      	return;
		      }
		      «FOR edge : g.edgesTopologically.filter[!isIsAbstract]»
		      	if(cmd.type=='«g.name.lowEscapeDart».«edge.name.escapeDart»'){
		      	  js.context.callMethod('create_edge_«edge.name.lowEscapeDart»_«g.name.lowEscapeDart»',[
		      	    cmd.sourceId,cmd.targetId,cmd.delegateId,cmd.dywaName,cmd.dywaVersion,js.JsObject.jsify(cmd.positions),js.JsObject.jsify(e.styleArgs()),e.$information(),e.$label()
		      	  ]);
		      	  return;
		      	}
		      «ENDFOR»
		  }
		  
		  
		  
		  @override
		  void execRemoveEdgeCanvas(int id,String type) {
		      «FOR edge : g.edgesTopologically.filter[!isIsAbstract]»
		      	if(type=='«g.name.lowEscapeDart».«edge.name.escapeDart»'){
		      	  js.context.callMethod('remove_edge_«edge.name.lowEscapeDart»_«g.name.lowEscapeDart»',[
		      	    id
		      	  ]);
		      	}
		      «ENDFOR»
		  }
		  
		  @override
		  void execReconnectEdgeCommandCanvas(ReconnectEdgeCommand cmd) {
		        «FOR edge : g.edgesTopologically.filter[!isIsAbstract]»
		        	if(cmd.type=='«g.name.lowEscapeDart».«edge.name.escapeDart»'){
		        	  js.context.callMethod('reconnect_edge_«edge.name.lowEscapeDart»_«g.name.lowEscapeDart»',[
		        	    cmd.sourceId,cmd.targetId,cmd.delegateId
		        	  ]);
		        	}
		        «ENDFOR»
		  }
		  
		  @override
		  void execCreateNodeCommandCanvas(CreateNodeCommand cmd) {
		      core.ModelElement e = findElement(cmd.delegateId);
		      if(e == null) {
		      	return;
		      }
		      var x = cmd.x;
			  var y = cmd.y;
			  if(e.container is core.Container) {
				 var parent = e.container;
		 		 while(parent!=null&&parent is core.Container) {
		 			x += (parent as core.Container).x;
		 			y += (parent as core.Container).y;
		 			parent = (parent as core.Container).container;
		 		 }
			  }
		      «FOR node : g.nodesTopologically.filter[!isIsAbstract]»
		      	if(cmd.type=='«g.name.lowEscapeDart».«node.name.escapeDart»'){
		      		
		      	  js.context.callMethod('create_node_«node.name.lowEscapeDart»_«g.name.lowEscapeDart»',[
		      	    «{
	    				val isElliptic = node.isElliptic(styles)
	    		    	'''
	    		    	x«IF isElliptic»+(cmd.width~/2)«ENDIF»,
	    		    	y«IF isElliptic»+(cmd.height~/2)«ENDIF»'''
	    		    }»,cmd.width,cmd.height,cmd.delegateId,cmd.containerId,cmd.dywaName,cmd.dywaVersion,js.JsObject.jsify(e.styleArgs()),e.$information(),e.$label()«IF node.prime»,cmd.primeId«ENDIF»
		      	  ]);
		      	  return;
		      	}
		      «ENDFOR»
		  }
		  
		  @override
		  void execMoveNodeCanvas(MoveNodeCommand cmd) {
		  	_moveNodeCanvas(cmd.type,cmd.delegateId,cmd.containerId,cmd.x,cmd.y);
		  	_moveEmbeddedNodes(cmd.delegateId);
		  }
		  
		  void _moveNodeCanvas(String type,int delegateId, int containerId, int x, int y) {
		  	var elem = findElement(delegateId) as core.Node;
		  	if(elem == null) {
		  		return;
		  	}
  			if(elem.container is core.Container) {
  				var parent = elem.container;
  				while(parent!=null&&parent is core.Container) {
  					x += (parent as core.Container).x;
  					y += (parent as core.Container).y;
  					parent = (parent as core.Container).container;
  				}
  			}
		  	«FOR node : g.nodesTopologically.filter[!isIsAbstract]»
		  		if(type=='«g.name.lowEscapeDart».«node.name.escapeDart»'){
		  		  js.context.callMethod('move_node_«node.name.lowEscapeDart»_«g.name.lowEscapeDart»',[
		  		    «{
 	    				val isElliptic = node.isElliptic(styles)
 	    		    	'''
 	    		    	x«IF isElliptic»+(elem.width~/2)«ENDIF»,
 	    		    	y«IF isElliptic»+(elem.height~/2)«ENDIF»'''
 	    		    }»,delegateId,containerId
		  		  ]);
		  		  return;
		  		}
		  	«ENDFOR»
		  }
		  
		  void _moveEmbeddedNodes(int dywaId) {
		  	var elem = findElement(dywaId) as core.Node;
		  	if(elem == null) {
		  		return;
		  	}
		  	elem.allElements().where((n)=>n.dywaId!=dywaId).where((n)=>n is core.Node).map((n)=>n as core.Node).forEach((n)=>_moveNodeCanvas(n.$type(),n.dywaId,n.container.dywaId,n.x,n.y));
		  }
		  
		  @override
		  void execRemoveNodeCanvas(int dywaId, String type) {
		      «FOR node : g.nodesTopologically.filter[!isIsAbstract]»
		  	if(type=='«g.name.lowEscapeDart».«node.name.escapeDart»'){
		  		js.context.callMethod('remove_node_«node.name.lowEscapeDart»_«g.name.lowEscapeDart»',[
		  			dywaId
		  		]);
		  		return;
		  	}
		  	«ENDFOR»
		  	}
		  	
		  	@override
		  	void execResizeNodeCommandCanvas(ResizeNodeCommand cmd) {
		  	    «FOR node : g.nodesTopologically.filter[!isIsAbstract]»
		  	    	if(cmd.type=='«g.name.lowEscapeDart».«node.name.escapeDart»'){
		  	    	  js.context.callMethod('resize_node_«node.name.lowEscapeDart»_«g.name.lowEscapeDart»',[
		  	    	    cmd.width,cmd.height,cmd.direction,cmd.delegateId
		  	    	  ]);
		  	    	  return;
		  	    	}
		  	    «ENDFOR»
		  	}
		  	
		  	@override
		  	void execRotateNodeCommandCanvas(RotateNodeCommand cmd) {
		  	    «FOR node : g.nodesTopologically.filter[!isIsAbstract]»
		  	    	if(cmd.type=='«g.name.lowEscapeDart».«node.name.escapeDart»'){
		  	    	  js.context.callMethod('rotate_node_«node.name.lowEscapeDart»_«g.name.lowEscapeDart»',[
		  	    	    cmd.angle,cmd.delegateId
		  	    	  ]);
		  	    	}
		  	    «ENDFOR»
		  	}
		  	
		  	@override
		  	void execUpdateBendPointCanvas(UpdateBendPointCommand cmd) {
		  		var positions = new js.JsArray();
		  		cmd.positions.forEach((p){
		  	  		var pos = new js.JsArray();
		  		  	pos['x'] = p.x;
		  		  	pos['y'] = p.y;
		  		  	positions.add(pos);
		  		});
		  	    js.context.callMethod('update_bendpoint_«g.name.lowEscapeDart»',[
		  	      positions,cmd.delegateId
		  	    ]);
		  	}
		  	
		  	@override
		  	void execUpdateElementCanvas(UpdateCommand cmd) {
		  	    core.IdentifiableElement identifiableElement = cmd.element;
		  	          if(identifiableElement is core.ModelElement){
		  	            js.context.callMethod('update_element_«g.name.lowEscapeDart»',[
		  	              null,
		  	              identifiableElement.dywaId,
		  	              identifiableElement.dywaVersion,
		  	              identifiableElement.dywaName,
		  	              js.JsObject.jsify(identifiableElement.styleArgs()),
		  	              identifiableElement.$information(),
		  	              identifiableElement.$label()
		  	            ]);
		  	    
		  	    }
		  	}
		  	
		  	@override
			void execAppearanceCanvas(AppearanceCommand cmd) {
				js.context.callMethod('update_element_appearance_«g.name.lowEscapeDart»',[
					cmd.delegateId,
					cmd.shapeId,
					cmd.background_r,cmd.background_g,cmd.background_b,
					cmd.foreground_r,cmd.foreground_g,cmd.foreground_b,
					cmd.lineInVisible,
					cmd.lineStyle,
					cmd.transparency,
					cmd.lineWidth,
					cmd.filled,
					cmd.angle,
					cmd.fontName,
					cmd.fontSize,
					cmd.fontBold,
					cmd.fontItalic,
					cmd.imagePath
				]);
			}
			
			@override
			void execHighlightCanvas(HighlightCommand cmd) {
				var preColor = js.context.callMethod('update_element_highlight_«g.name.lowEscapeDart»',[
					cmd.dywaId,
					cmd.background_r,cmd.background_g,cmd.background_b,
					cmd.foreground_r,cmd.foreground_g,cmd.foreground_b
				]);
				Map<String,dynamic> pc = new Map();
				pc['background'] = preColor['background'];
				pc['foreground'] = preColor['foreground'];
				cmd.setPre(pc);
				super.highlightings.add(cmd);
			}
			
			@override
			void revertHighlightCanvas(HighlightCommand cmd) {
				if(findElement(cmd.dywaId)==null) {
					return;
				}
				js.context.callMethod('update_element_highlight_«g.name.lowEscapeDart»',[
					cmd.dywaId,
					cmd.pre_background_r,cmd.pre_background_g,cmd.pre_background_b,
					cmd.pre_foreground_r,cmd.pre_foreground_g,cmd.pre_foreground_b
				]);
			}
			
		}
		'''
		
	def propagation(CharSequence s)
	'''
	startPropagation().then((_){
		«s»
	}).then((_)=>endPropagation());
	'''
	

	def fileNameGraphModelComponent(String graphmodelName) '''«graphmodelName.lowEscapeDart»_component.dart'''

	def contentGraphModelComponent(GraphModel g,Styles styles) '''
		import 'dart:async';
		import 'dart:html' as html;
		import 'dart:convert' as convert;
		import 'package:angular/angular.dart';
		
		import 'package:«gc.projectName.escapeDart»/src/model/core.dart' as core;
		import 'package:«gc.projectName.escapeDart»/src/model/message.dart';
		import 'package:«gc.projectName.escapeDart»/src/model/command.dart';
		import 'package:«gc.projectName.escapeDart»/src/model/check.dart';
		import 'package:«gc.projectName.escapeDart»/src/service/check_service.dart';
		import 'package:«gc.projectName.escapeDart»/src/service/editor_data_service.dart';
		import 'package:«gc.projectName.escapeDart»/src/model/«g.name.lowEscapeDart».dart' as «g.name.lowEscapeDart»;
		import '«g.name.lowEscapeDart»_command_graph.dart';
		import 'package:«gc.projectName.escapeDart»/src/pages/editor/palette/graphs/«g.name.lowEscapeDart»/palette_builder.dart';
		import 'package:«gc.projectName.escapeDart»/src/pages/editor/palette/list/list_view.dart';
		
		import 'package:«gc.projectName.escapeDart»/src/pages/shared/context_menu/context_menu.dart';
		import 'package:«gc.projectName.escapeDart»/src/service/context_menu_service.dart';
		
		«FOR pr:g.primeReferencedGraphModels.filter[!equals(g)]»
		//prime referenced graphmodel «pr.name»
		import 'package:«gc.projectName.escapeDart»/src/model/«pr.name.lowEscapeDart».dart' as «pr.name.lowEscapeDart»;
		«ENDFOR»
		«FOR pr:gc.ecores»
		//prime referenced ecore «pr.name»
		import 'package:«gc.projectName.escapeDart»/src/model/«pr.name.lowEscapeDart».dart' as «pr.name.lowEscapeDart»;
		«ENDFOR»
		
		import 'package:«gc.projectName.escapeDart»/src/service/graph_service.dart';
		import '../../dialog/message_dialog.dart';
		import '../../dialog/display_dialog.dart';
		
		import 'dart:js' as js;
		
		@Component(
		    selector: '«g.name.lowEscapeDart»-canvas',
		    templateUrl: '«g.name.lowEscapeDart»_component.html',
		    styleUrls: const [],
		    directives: const [coreDirectives,MessageDialog,DisplayDialog]
		)
		class «g.name.fuEscapeDart»CanvasComponent implements OnInit, OnChanges, OnDestroy {
		
		
		  final selectionChangedSC = new StreamController();
		  @Output() Stream get selectionChanged => selectionChangedSC.stream;
		  
		  final selectionChangedModalSC = new StreamController();
		  @Output() Stream get selectionChangedModal => selectionChangedModalSC.stream;
		  
		  final hasChangedSC = new StreamController();
		  @Output() Stream get hasChanged => hasChangedSC.stream;
		  
		  final closeSC = new StreamController();
		  @Output() Stream get close => closeSC.stream;
		  
		  final jumpToSC = new StreamController();
		  @Output() Stream get jumpTo => jumpToSC.stream;
		
		  @Input()
		  «g.name.lowEscapeDart».«g.name.fuEscapeDart» currentGraphModel;
		  
		  @Input()
		  core.PyroUser user;
		  
		  @Input()
		  core.PyroProject project;
		  
		  @Input()
		  bool isFullScreen;
		  
		  @Input()
		  core.LocalGraphModelSettings currentLocalSettings;
		  
		  @Input()
		  bool canEdit = false;
		
		  «g.name.fuEscapeDart»CommandGraph commandGraph;
		  
		  html.WebSocket webSocketGraphModel;
		
		  final GraphService graphService;
		  
		  final CheckService checkService;
		  final EditorDataService _editorDataService;
		  final ContextMenuService _contextMenuService;
		  
		  bool showMessageDialog = false;
		  dynamic dialogMessage = null;
		  String messageDialogType = null;
		  
		  bool showDisplayDialog = false;
		  DisplayMessages displayMessages = null;
		  
		  List<HighlightCommand> highlightings = new List();
		  
		  core.PyroElement currentSelection = null;
		  
		  bool loading = true;
		  
		  ///check leve
		  @Input()
		  bool isError = true;
		  @Input()
		  bool isWarning = true;
		  @Input()
		  bool isInfo = true;
		  
«««		  dynamic paperMousemoveSubscription;
		
		  «g.name.fuEscapeDart»CanvasComponent(
		  	GraphService this.graphService,
		  	this.checkService,
		  	this._editorDataService,
		  	this._contextMenuService) {
		  }
		
		  @override
		  void ngOnInit() {
		  	loading = true;
		  	initCanvas();
		  	activateWebSocket();
«««		  	paperMousemoveSubscription = html.document.on["paper:mousemove"].listen(cb_cursor_moved);
		  }
		  
		  @override
		  ngOnDestroy() {
		      this.closeWebSocket();
«««		      paperMousemoveSubscription.cancel();
		  }
		  
«««		  openContextMenu(html.MouseEvent e) {    
«««	        js.JsObject paper = js.context['\$paper_«g.name.lowEscapeDart»']; 
«««	    	var pointOnPaper = paper.callMethod('clientToLocalPoint', [e.client.x, e.client.y]);    
«««	    	
«««	    	double x = pointOnPaper['x'];
«««	    	double y = pointOnPaper['y'];
«««	    	
«««	    	List<MapList> mapList = «g.name.fuEscapeDart»PaletteBuilder.build(currentGraphModel);
«««			List<ContextMenuEntry> menuItems = new List();
«««			mapList.forEach((ml) {
«««		      menuItems.add(ContextMenuGroup(ml.name));
«««				ml.values.forEach((val){
«««				  menuItems.add(ContextMenuItem(null, val.name, true, () {
«««				    js.context.callMethod("create_node_«g.name.lowEscapeDart»_after_drop", [x,y,val.identifier]);	
«««				  }));  
«««			  });
«««			});
«««				
«««			_contextMenuService.show(ContextMenu(e.client.x, e.client.y, List.of([
«««			  ContextMenuItem(null, 'Create here...', true, null, subItems: menuItems)
«««			])));
«««	      }
		  
		  void cb_cursor_moved(int x,int y) {
		      if (webSocketGraphModel != null) {
		        	  
	        	  var wsMessage = {
	        	    'senderId': user.dywaId, 
	        	    'event': 'updateCursorPosition',
	        	    'content': {
	        	      'dywaRuntimeType': 'info.scce.pyro.sync.UpdateCursorPosition',
	        	      'graphModelId': currentGraphModel.dywaId,
	        	      'x': x,
	        	      'y': y
	        	    }
	        	  };
	        	  
	        	  webSocketGraphModel.send(convert.jsonEncode(wsMessage));
		      }
		  }
		  
		  void closeMessgeDialog(dynamic e) {
			showDisplayDialog = false;
			dialogMessage = null;
			if(messageDialogType=='one_answer'){
				e['senderId']=user.dywaId;
				e['dywaRuntimeType']='info.scce.pyro.message.DialogAnswer';
				this.webSocketGraphModel.send(convert.jsonEncode(e));
			}
			messageDialogType=null;
		  }
		  
			void closeDisplayDialog(dynamic e) {
			  	showDisplayDialog = false;
			  	displayMessages = null;
			}
			void saveDisplayDialog(String content) {
				graphService.createblob("result_${DateTime.now().millisecondsSinceEpoch}.html",content,project);
			  	showDisplayDialog = false;
			  	displayMessages = null;
			}
			void downloadDisplayDialog(dynamic e) {
			  	showDisplayDialog = false;
			  	displayMessages = null;
			}
		  
		  void closeWebSocket() {
		  		if(this.webSocketGraphModel != null && this.webSocketGraphModel.readyState == html.WebSocket.OPEN) {
		  			html.window.console.debug("Closing Websocket webSocketCurrentUser");
		  			this.webSocketGraphModel.close();
		  			this.webSocketGraphModel = null;
		  			_editorDataService.graphModelWebSocketSC.add(null);
		  		}
		  	}
		  	
		  void executeCommands(CompoundCommandMessage m,bool forceExecute) => commandGraph.receiveCommand(m,forceExecute: forceExecute);
		  
		  void refreshChecks(CheckResults crs) {
		  	if (crs != null) {
		  	  js.context.callMethod("refresh_checks_«g.name.lowEscapeDart»",[
  		  		crs.toJS()
  		  	  ]);	
		  	}
		  }
		  
		  void executeGraphmodelButton(String key) {
		  	graphService.executeGraphmodelButton«g.name.escapeDart»(currentGraphModel.dywaId, currentGraphModel,key,highlightings).then((m){
	 	 		«'''
	 	 			«'''
	 	 			commandGraph.receiveCommand(m,forceExecute: true);
	 	 			'''.checkCommand("basic_valid_answer",false)»
	 	 		'''.propagation»
	 	 	});
		  }
		  
		  void activateWebSocket() {
		  		if(this.currentGraphModel != null && user != null && this.webSocketGraphModel == null) {
					this.webSocketGraphModel = new html.WebSocket('${graphService.getBaseUrl(protocol: 'ws')}/ws/graphmodel/${currentGraphModel.dywaId}/private');
					_editorDataService.graphModelWebSocketSC.add(webSocketGraphModel);
		  			
		  			// Callbacks for currentUser
		  			this.webSocketGraphModel.onOpen.listen((e) {
		  				html.window.console.debug("[PYRO] onOpen GraphModel Websocket");
		  			});
		  			
		  			this.webSocketGraphModel.onMessage.listen((html.MessageEvent e) {
		  				html.window.console.debug("[PYRO] onMessage GraphModel Websocket");
		  				if(e.data != null) {
		  					var jsog = convert.jsonDecode(e.data);
		  					
		  					String event = jsog['event'];
		  					if(event == 'display') {
		  						var senderId = jsog['senderId'];
		  						if (senderId.toString() == user.dywaId.toString()) {
		  							displayMessages = DisplayMessages.fromJSOG(jsog["content"]);
		  							showDisplayDialog = true;		  							
		  						}
		  					}
		  					else if (event == 'dialog') {
		  						//message for current user
		  						if(jsog['content']['messageType']=='message_dialog_no_answer') {
		  							print(jsog['content']['message']);
		  							dialogMessage = jsog['content'];
		  							messageDialogType = 'no_answer';
		  							showMessageDialog = true;
		  						}
		  						if(jsog['content']['messageType']=='message_dialog_one_answer') {
		  							print(jsog['content']['message']);
		  							dialogMessage = jsog['content'];
		  							messageDialogType = 'one_answer';
		  							showMessageDialog = true;
		  						}
		  					} else if (event == 'updateCursorPosition') {
		  						// update cursor
		  					  	var senderId = jsog['senderId'];
		  					    if (senderId.toString()!=user.dywaId.toString()) {
		  					      var x = jsog['content']['x'];
		  					      var y = jsog['content']['y'];
		  					      
		  					      List<core.PyroUser> users = _editorDataService.organization.users;
		  					      int i = users.indexWhere((u) => u.dywaId == senderId);
  					      		  core.PyroUser sender = users[i];
		  					      		  					      		  					      
		  					      js.JsObject cursorManager = js.context['\$cursor_manager_«g.name.lowEscapeDart»']; 
		  					      cursorManager.callMethod('update_cursor', [senderId, sender.username, x, y]);
		  					    }
		  					} else {
		  						// update graph model changed by another user		  					
		  						if(jsog['senderId'].toString()!=user.dywaId.toString()){
			  						if(jsog['content']['messageType']=='graphmodel'){
			  							startPropagation().then((_) {
			  								var _g = «g.name.lowEscapeDart».«g.name.fuEscapeDart».fromJSOG(jsog['content'],new Map());
			  								currentGraphModel.merge(_g,structureOnly:true);
											currentGraphModel.connector = _g.connector;
											currentGraphModel.router = _g.router;
											currentGraphModel.filename = _g.filename;
											currentGraphModel.height = _g.height;
											currentGraphModel.width = _g.width;
			  								js.context.callMethod('update_routing_«g.name.lowEscapeDart»',[currentGraphModel.router,currentGraphModel.connector]);
			  							}).then((_) => endPropagation());
			  						}
			  						else {
			  							var m = Message.fromJSOG(jsog['content']);
			  							startPropagation().then((_) {
			  								if (m is CompoundCommandMessage) {
			  									executeCommands(m,true);
			  								}
			  							}).then((_) => endPropagation());
			  						}
			  					}
		  					}
		  				}
		  			});
		  			
		  			this.webSocketGraphModel.onClose.listen((html.CloseEvent e) {
		  				if(e.code == 4001) {
		  					//graphmodel has been deleted or access denied
		  					closeSC.add({});
		  				}
		  				html.window.console.debug("[PYRO] onClose GraphModel Websocket");
		  			});
		  			this.webSocketGraphModel.onError.listen((e) {
		  				html.window.console.debug("[PYRO] Error on GraphModel Websocket: ${e.toString()}");
		  			});
		  		}
		  	}
		  
		  void initCanvas() {
		  	graphService.loadCommandGraph«g.name.fuEscapeDart»(currentGraphModel,highlightings).then((cg){
		  	  	commandGraph = cg;
		  	  	commandGraph.editorCommand.listen((c){
	            	if(c is OpenFileCommand) {
	            		Map m = new Map();
	            		m['graphmodel_id'] = c.dywaId;
	      	      	jumpToSC.add(m);
	            	}
	            });
		  	  	currentGraphModel.merge(cg.currentGraphModel);
		  	  	graphService.update(currentGraphModel.dywaId);
			  	 	js.context.callMethod("load_«g.name.lowEscapeDart»",[
		  	 	      currentGraphModel.width,
		  	 	      currentGraphModel.height,
		  	 	      currentGraphModel.scale,
		  	 	      currentGraphModel.dywaId,
		  	 	      currentGraphModel.router,
		  	 	      currentGraphModel.connector,
		  	 	      //callback afert initialization
		  	 	      initialized,
		  	 	      //message callbacks
		  	 	      cb_element_selected,
		  	 	      cb_graphmodel_selected,
		  	 	      cb_update_bendpoint,
		  	 	      cb_can_move_node,
		  	 	      cb_can_connect_edge,
		  	 	      cb_get_valid_targets,
		  	 	      cb_is_valid_connection,
		  	 	      cb_get_valid_containers,
		  	 	      cb_is_valid_container,
		  	 	      cb_get_custom_actions,
		  	 	      cb_fire_dbc_actions,
		  	 	      cb_delete_selected,
		  	 	      cb_cursor_moved,
		  	 	      cb_property_persist,
		  	 	      «FOR elem : g.elements.filter[!isIsAbstract] SEPARATOR ","»
		  	 	      	«IF elem instanceof Node»
		  	 	      		cb_create_node_«elem.name.lowEscapeDart»,
		  	 	      		cb_remove_node_«elem.name.lowEscapeDart»,
		  	 	      		cb_move_node_«elem.name.lowEscapeDart»,
		  	 	      		cb_resize_node_«elem.name.lowEscapeDart»,
		  	 	      		cb_rotate_node_«elem.name.lowEscapeDart»
		  	 	      	«ENDIF»
		  	 	      	«IF elem instanceof Edge»
		  	 	      		cb_create_edge_«elem.name.lowEscapeDart»,
		  	 	      		cb_remove_edge_«elem.name.lowEscapeDart»,
		  	 	      		cb_reconnect_edge_«elem.name.lowEscapeDart»
		  	 	      	«ENDIF»
		  	 	      «ENDFOR»
				]);
	  	 	    «IF g.hasAppearanceProvider(styles)»
 	      		graphService.appearances«g.name.fuEscapeDart»(currentGraphModel).then((data){
 	      			var jsog = convert.jsonDecode(data);
 	      			var m = Message.fromJSOG(jsog);
 	      			startPropagation().then((_) {
 	      				if (m is CompoundCommandMessage) {
 	      					executeCommands(m,true);
 	      				}
 	      			}).then((_) => endPropagation());
 	      		});
 	      		«ENDIF»
				
			}).whenComplete(()=>loading = false);
		  }
		  	 
		  @override
		   ngOnChanges(Map<String, SimpleChange> changes) {
		     if(changes.containsKey('currentGraphModel')) {
		       var newGraph = changes['currentGraphModel'].currentValue;
		       var preGraph = changes['currentGraphModel'].previousValue;
		       if(newGraph != null&& newGraph is «g.name.lowEscapeDart».«g.name.fuEscapeDart» && preGraph != null && preGraph is «g.name.lowEscapeDart».«g.name.fuEscapeDart» && newGraph!=preGraph) {
		         //desroy
		         loading = true;
		         try {
		         	js.context.callMethod('destroy_«g.name.lowEscapeDart»',[]);
		         } catch(e){}
		         closeWebSocket();
		         //rebuild
		         initCanvas();
		         activateWebSocket();
		       }
		     }
		   }
		   
		   
		   Future<Null> startPropagation(){
		    js.context.callMethod('start_propagation_«g.name.lowEscapeDart»',[]);
		    return new Future.value(null);
		   }
		   
		   void endPropagation(){
		     js.context.callMethod('end_propagation_«g.name.lowEscapeDart»',[]);
		     //reload checks
		     fetchChecks();
		   }
		   
		   void updateCheckLevel(bool isError,bool isWarning,bool isInfo) {
		   	this.isError = isError;
		    this.isWarning = isWarning;
		   	this.isInfo = isInfo;
		   	fetchChecks();
		   }
		   
		   void updateGlueline(bool isGlueline) {
		     	js.context.callMethod("refresh_gluelines_«g.name.lowEscapeDart»",[
	  		  		isGlueline
	  		  	  ]);
		   }
		   
		   void fetchChecks() {
		   	  checkService.read("«g.name.lowEscapeDart»",currentGraphModel).then((crs){
		   	  	CheckResults filteredCRS = CheckResults.filterChecks(CheckResults.copy(crs),isError,isWarning,isInfo);
		   	  	refreshChecks(filteredCRS);
		   	  });
		   }
		   
		   void export(String type) {
		   		if(type=='svg'){
			   	   	js.context.callMethod('download_svg',[currentGraphModel.filename]);
		   		}
		   		if(type=='png'){
		   		   	js.context.callMethod('download_png',[currentGraphModel.filename]);
		   		}
		   }
		   
		   void updateScale(double factor,{bool persist:true}) {
		   	if(factor!=0.0) {
		   		currentGraphModel.scale = factor;
		   	}
		   	if(persist) {
		   		graphService.updateGraphModel(currentGraphModel).then((_){
		   			js.context.callMethod('update_scale_«g.name.lowEscapeDart»',[currentGraphModel.scale]);
		   		});
		   	} else {
		   		js.context.callMethod('update_scale_«g.name.lowEscapeDart»',[currentGraphModel.scale]);
		   	}
		   	
		   }
		   
		   void updateRouting() {
		   	graphService.updateGraphModel(currentGraphModel).then((_){
		   		js.context.callMethod('update_routing_«g.name.lowEscapeDart»',[currentGraphModel.router,currentGraphModel.connector]);
		   	});
		   }
		
		   void updateProperties(core.IdentifiableElement ie) {
		   	if(ie is! core.GraphModel){
				currentGraphModel.allElements().where((n)=>n is core.ModelElement).forEach((n)=>updateElement(n));
			} else {
				if(ie is «g.name.lowEscapeDart».«g.name.fuEscapeDart»){
					currentGraphModel.merge(ie,structureOnly:false);
				}
			}
			//check for prime referencable element in same graph and update
			«FOR pr:g.primeRefs.filter[referencedElement.graphModel.equals(g)]»
				if(ie is «g.name.lowEscapeDart».«pr.referencedElement.name.fuEscapeDart») {
					//update all prime nodes for this element
					
					currentGraphModel.allElements()
						.where((n)=>n is «g.name.lowEscapeDart».«(pr.eContainer as ModelElement).name.fuEscapeDart»)
						.forEach((n)=>updateElement(n));
				}
			«ENDFOR»
		   }
		   
		  void cb_property_persist(int dywaId,String value) {
		  	
		  	var elem = currentGraphModel.allElements().where((n)=>n.dywaId == dywaId).first;
		  	PropertyMessage pm = new PropertyMessage(
	  	        elem.dywaId,
	  	        elem.runtimeType.toString(),
	  	        elem,
	  	        user.dywaId
	  	    );
		  	«FOR e:g.nodesTopologically.filter[directlyEditable] SEPARATOR "else "»
		  	if(elem.$type() == "«g.name.lowEscapeDart».«e.name.fuEscapeDart»") {
			  	(elem as «g.name.lowEscapeDart».«e.name.fuEscapeDart»).«e.directlyEditableAttribute.name.escapeDart» = value;
		  	}
		  	«ENDFOR»
		  	graphService.sendMessage(pm,"«g.name.lowEscapeDart»",currentGraphModel.dywaId).then((m){
		  		«'''
			  	    if (m is CompoundCommandMessage) {
			  	    	executeCommands(m,true);
			  	    }
			  	'''.propagation»
		  	    
		  	});
		  }
		   
		  void undo() {
		  	var ccm = commandGraph.undo(user);
		  	if(ccm!=null) {
		  	  graphService.sendMessage(ccm,"«g.name.lowEscapeDart»",currentGraphModel.dywaId).then((m) {
		  	«'''
		  	    «'''
		  	      commandGraph.receiveCommand(m);
		  	    '''.checkCommand("undo_valid_answer",false)»
		  	'''.propagation»
		  	  });
		  	}
		  }
		  
		  void redo() {
		  var ccm = commandGraph.redo(user);
		    if(ccm!=null) {
		      graphService.sendMessage(ccm,"«g.name.lowEscapeDart»",currentGraphModel.dywaId).then((m) {
		 	«'''
		      	«'''
		        commandGraph.receiveCommand(m);
		      	'''.checkCommand("redo_valid_answer",false)»
		  	'''.propagation»
		      });
		    }
		  }
		  
		  /// create the current graphmodel initailly
		  void initialized() {
		  	//add nodes and container bottom up
		  	«'''
			initContainerDeeply(currentGraphModel);
			//connect by edges
			currentGraphModel.modelElements.where((n)=>n is core.Edge).forEach((e){
				«FOR edge : g.edgesTopologically.filter[!isIsAbstract] SEPARATOR " else "»
					if(e is «g.name.lowEscapeDart».«edge.name.escapeDart»){
						create_edge_«edge.name.lowEscapeDart»(e);
					}
				«ENDFOR»
			});
			updateCheckLevel(isError,isWarning,isInfo);
		  	'''.propagation»
			 }
			 
			 void initContainerDeeply(core.ModelElementContainer container) {
			     for(var node in container.modelElements) {
			     	«FOR node : g.nodesTopologically.filter[!isIsAbstract]»
			     		if(node is «g.name.lowEscapeDart».«node.name.escapeDart»){
			     		  create_node_«node.name.lowEscapeDart»(node);
			     		  «IF node instanceof NodeContainer»
			     		  	initContainerDeeply(node);
			     		  «ENDIF»
			     		  continue;
			     		}
			     	«ENDFOR»
			     }
			 }
			 
		 	 core.IdentifiableElement findElement(int dywaId) {
		 		 if(dywaId==currentGraphModel.dywaId){
		 			 return currentGraphModel;
		 		 }
		 		 return currentGraphModel.allElements().firstWhere((n)=>n.dywaId==dywaId, orElse: () => null);
		 	 }
		 	 
		 	 void cb_get_custom_actions(int dywaId,int x,int y,int canvasX, int canvasY) {
	 	 		«'''
	 	 		graphService.fetchCustomActionsFor«g.name.escapeDart»(dywaId,currentGraphModel).then((map){
	 	 			
	 	 			List<MapList> mapList = «g.name.fuEscapeDart»PaletteBuilder.build(currentGraphModel);
					List<ContextMenuEntry> mainMenuItems = new List();
					List<ContextMenuEntry> menuItems = new List();
					
					mapList.forEach((ml) {
						ml.values.forEach((val){
						  if(cb_can_move_node(-1,dywaId,tmpNode:val.instance) == true) {
							  menuItems.add(ContextMenuItem(null, val.name, true, () {
							    js.context.callMethod("create_node_«g.name.lowEscapeDart»_after_drop", [canvasX,canvasY,val.identifier]);	
							  }));
						  }
					  });
					});
					if(menuItems.isNotEmpty) {
						mainMenuItems.add(
							ContextMenuItem(null, 'Create here...', true, null, subItems: menuItems)
						);
					}
					mainMenuItems.add(
						ContextMenuItem(null, 'Edit...', true, (){
							cb_element_selected(dywaId,openModal:true);
						})
					);
	 	 			if(map.isNotEmpty){
	 	 				map.forEach((k,v){
	 	 					mainMenuItems.add(
	 	 						ContextMenuItem(null, v, true, (){
									  	cb_fire_cm_action(k,dywaId);
								})
							);
 	 					});
	 	 			}
		 	 		_contextMenuService.show(
						ContextMenu(
							x, y, mainMenuItems
						)
					);
	 	 		});
		 	 	'''.propagation»
		 	 }
		 	 
		 	 void cb_delete_selected() {
		 	 	if(currentSelection == null || currentSelection is core.GraphModel) {
		 	 		return;
		 	 	}
		 	 	//prevent for disbale remove
		 	 	«FOR e:g.elements.filter[!removable]»
		 	 	if(currentSelection.$type()=='«g.name.lowEscapeDart».«e.name.fuEscapeDart»') {
 					return;
	 	 		}
		 	 	«ENDFOR»
		 	 	if(currentSelection is core.Node) {
			 	 	var node = currentSelection as core.Node;
			 	 	«'''
			 	 	var container = findElement(node.container.dywaId) as core.ModelElementContainer;
			 	 	var ccm = commandGraph.sendRemoveNodeCommand(node.dywaId,user);
			 	 	graphService.sendMessage(ccm,"«g.name.lowEscapeDart»",currentGraphModel.dywaId).then((m){
			 	 		if(m is CompoundCommandMessage){
			 	 			if(m.type=='basic_valid_answer'){
			 	 			 	selectionChangedSC.add(currentGraphModel);
			 	 			 	node.container = null;
			 	 			 	«'''
			 	 			 	 commandGraph.receiveCommand(m,forceExecute: true);
			 	 			 	 commandGraph.storeCommand(m.cmd);
			 	 			 	 if(container is! core.GraphModel){
			 	 			 	    updateElement(container as core.ModelElement);
			 	 			 	 }
			 	 			 	 '''.propagation»
			 	 			 }
			 	 		}
			 	 	});
			 	 	'''.propagation»
		 	 	}
		 	 	if(currentSelection is core.Edge) {
		 	 		var edge = currentSelection as core.Edge;
		 	 		«'''
		 	 		var ccm = commandGraph.sendRemoveEdgeCommand(edge.dywaId,edge.source.dywaId,edge.target.dywaId,edge.$type(),user);
		 	 		graphService.sendMessage(ccm,"«g.name.lowEscapeDart»",currentGraphModel.dywaId).then((m){
		 	 		if(m is CompoundCommandMessage){
		 	 		  if(m.type=='basic_valid_answer'){
		 	 		  	«'''
		 	 		    selectionChangedSC.add(currentGraphModel);
		 	 		    commandGraph.receiveCommand(m,forceExecute: true);
		 	 		    commandGraph.storeCommand(m.cmd);
		 	 		    '''.propagation»
		 	 		  }
		 	 		}
		 	 		});
 	 			 	'''.propagation»
		 	 	}
		 	 }
		 	 
		 	 void cb_fire_cm_action(String fqn,int dywaId) {
		 	 	graphService.triggerCustomActionsFor«g.name.escapeDart»(dywaId, currentGraphModel,fqn,highlightings).then((m){
		 	 		«'''
		 	 			«'''
		 	 			commandGraph.receiveCommand(m,forceExecute: true);
		 	 			'''.checkCommand("basic_valid_answer",false)»
		 	 		'''.propagation»
		 	 	});
		 	 }
		 	 
		 	 void cb_fire_dbc_actions(int dywaId) {
		 	 	graphService.triggerDoubleClickActionsFor«g.name.escapeDart»(dywaId, currentGraphModel,highlightings).then((m){
		 	 	«'''
		 	 		«'''
		 	 		commandGraph.receiveCommand(m,forceExecute: true);
		 	 		'''.checkCommand("basic_valid_answer",false)»
		 	 	'''.propagation»
		 	 	}).then((_){
		 	 		//Execute jumpToDefinition
		 	 		var elem = findElement(dywaId);
		 	 		«FOR n:g.nodesTopologically.filter[prime].filter[hasJumpToAnnotation] SEPARATOR " else"»
		 	 		if(elem is «g.name.lowEscapeDart».«n.name.escapeDart») {
		 	 			graphService.jumpToPrime('«g.name.lowEscapeDart»','«n.name.lowEscapeDart»',currentGraphModel.dywaId,dywaId).then((m){
		 	 				jumpToSC.add(m);
		 	 			});
		 	 		}
		 	 		«ENDFOR»
		 	 	});
		 	 	
		 	 }
			 
			 void cb_element_selected(int dywaId,{bool openModal:false}) {
			 	 if (dywaId != null) { // cursors have no dywaId and should be ignored
			 	 	if(dywaId<0){
 				       if(openModal){
			 	 			selectionChangedModalSC.add(currentGraphModel);
			 	 	   } else {
				 	 		selectionChangedSC.add(currentGraphModel);
			 	 	   }
			 	 	   «IF g.hasPostSelect»
			 	 	   graphService.triggerPostSelectFor«g.name.escapeDart»(dywaId, currentGraphModel,"«g.name.lowEscapeDart».«g.name.fuEscapeDart»",highlightings).then((m){
				 	 		«'''
				 	 			«'''
				 	 			commandGraph.receiveCommand(m,forceExecute: true);
				 	 			'''.checkCommand("basic_valid_answer",false)»
				 	 		'''.propagation»
				 	 	});
			 	 	   «ENDIF»
			 	 	   
			 	    } else {
			 	 		//find element
			 	 		var newSelection = findElement(dywaId);
			 	 		currentSelection = newSelection;
			 	 		if(openModal){
			 	 			selectionChangedModalSC.add(newSelection);
			 	 		} else {
				 	 		selectionChangedSC.add(newSelection);
			 	 		}
			 	 		«FOR e:g.elements.filter[hasPostSelect]»
			 	 		if(newSelection.$type() == "«g.name.lowEscapeDart».«e.name.fuEscapeDart»") {
			 	 			graphService.triggerPostSelectFor«g.name.escapeDart»(dywaId, currentGraphModel,"«g.name.lowEscapeDart».«e.name.fuEscapeDart»",highlightings).then((m){
 					 	 		«'''
 					 	 			«'''
 					 	 			commandGraph.receiveCommand(m,forceExecute: true);
 					 	 			'''.checkCommand("basic_valid_answer",false)»
 					 	 		'''.propagation»
 					 	 	});
			 	 		}
			 	 		«ENDFOR»
			 	 	}
			 	 	
			 	 }
			 }
			 
			 bool check_bendpoint_update(core.Edge edge,List positions) {
			 	if(positions==null) {
			 		return edge.bendingPoints.isNotEmpty;
			 	}
			 	 if(positions.length==edge.bendingPoints.length) {
			 		 for(var pos in positions){
			 			 var x = pos['x'];
			 			 var y = pos['y'];
			 			 var found = false;
			 			 for(var b in edge.bendingPoints) {
			 				 if(b.x==x&&b.y==y){
			 					 found = true;
			 					 break;
			 				 }
			 			 }
			 			 if(found==false){
			 				 return true;
			 			 }
			 		 }
			 	 } else {
			 	 return true;
			 	}
			 	return false;
			 }
			 
			 void cb_graphmodel_selected() {
			 	currentSelection = null;
			    selectionChangedSC.add(currentGraphModel);
			    «IF g.hasPostSelect»
	 	 	    graphService.triggerPostSelectFor«g.name.escapeDart»(dywaId, currentGraphModel,"«g.name.lowEscapeDart».«g.name.fuEscapeDart»",highlightings).then((m){
		 	 		«'''
		 	 			«'''
		 	 			commandGraph.receiveCommand(m,forceExecute: true);
		 	 			'''.checkCommand("basic_valid_answer",false)»
		 	 		'''.propagation»
		 	 	});
	 	 	   «ENDIF»
			 }
			 
			 void updateElement(core.ModelElement elem,{String cellId}) {
			 	if(elem==null) return;
		   		js.context.callMethod('update_element_«g.name.lowEscapeDart»',[
					cellId,
		 			elem.dywaId,
		 			elem.dywaVersion,
		 			elem.dywaName,
		 			elem.styleArgs(),
		 			elem.$information(),
		 			elem.$label()
		   		]);
		 		
		 	}
		 	
		 	bool cb_is_valid_container(int nodeId,int containerId) {
		 		var resp = cb_can_move_node(nodeId,containerId);
		 		return resp is bool && resp == true;
		 	}
		 	
		 	js.JsArray cb_get_valid_containers(int dywaId,String type) {
		 		core.Node node = null;
		 		if(dywaId==-1) {
			 		switch(type) {
			 		«FOR n:g.nodesTopologically.filter[!isAbstract]»
			 			«IF n.isPrime»
			 			case '«n.primeReference.type.graphModel.name.lowEscapeDart».«n.primeReference.type.name.escapeDart»':node = new «g.name.lowEscapeDart».«n.name.escapeDart»();break;
			 			«ELSE»
			 			case '«g.name.lowEscapeDart».«n.name.escapeDart»':node = new «g.name.lowEscapeDart».«n.name.escapeDart»();break;
			 			«ENDIF»
			 		«ENDFOR»
			 		}
		 		}
		 		var valids = new js.JsArray();
		 		currentGraphModel.allElements().where((n)=>n is core.Container).forEach((n) {
		 			var re = null;
		 			if(dywaId==-1) {
			 			re = cb_can_move_node(-1,n.dywaId,tmpNode:node);
		 			} else {
		 				re = cb_can_move_node(dywaId,n.dywaId);
		 			}
		 			if(re is bool && re == true) {
		 				valids.add(n.dywaId);
		 			}
		 		});
		 		return valids;
		 	}
		 	
		 	dynamic cb_can_move_node(int dywaId,int containerId,{core.Node tmpNode:null}) {
		 		var node = null;
		 		if(tmpNode == null) {
		 			node = findElement(dywaId) as core.Node;
		 		} else {
		 			node = tmpNode;
		 		}
		 		var c = findElement(containerId);
		 		
		 		var x = node.x;
				var y = node.y;
				
				var parent = node.container;
				while(parent!=null&&parent is core.Container) {
					x += (parent as core.Container).x;
					y += (parent as core.Container).y;
					parent = (parent as core.Container).container;
				}
		 		
		 		if(c is! core.ModelElementContainer){
		 			var arr = js.JsArray();
		 			arr['x'] = x;
		 			arr['y'] = y;
		 			if(node.container != null) {
			 			arr['containerId'] = node.container.dywaId;
		 			}
		 			return arr;
		 		}
		 		var container = c as core.ModelElementContainer;
		 		if(node.container ==null || node.container.dywaId!=container.dywaId)
		 		{
		 		  if(container is «g.name.lowEscapeDart».«g.name.fuEscapeDart»)
		 		  {
		 			«g.containmemntCheck(g,"g")»
		 		  }
		 			«FOR container:g.nodesTopologically.filter(NodeContainer) SEPARATOR "else "»
		 		  if(container is «g.name.lowEscapeDart».«container.name.fuEscapeDart»)
		 		  {	
		 		  	«FOR sub: container.name.parentTypes(g).filter(NodeContainer)»
		 				«sub.containmemntCheck(g,sub.name)»
	 			  	«ENDFOR»
		 		  }
		 			«ENDFOR»
		 		 
		 		  var arr = js.JsArray();
		 		  arr['x'] = x;
		 		  arr['y'] = y;
		 		  if(node.container != null) {
		 		  	arr['containerId'] = node.container.dywaId;
		 		  }
		 		  return arr;
		 		}
		 		return true;
		 	}
		 	
		 	js.JsArray cb_get_valid_targets(int dywaId) {
	 	 		var valids = new js.JsArray();
	 	 		currentGraphModel.allElements().where((n)=>n is core.Node).forEach((n){
	 	 			var re = cb_can_connect_edge(-1,dywaId,n.dywaId);
	 	 			if(re is bool && re == true) {
	 	 				valids.add(n.dywaId);
	 	 			}
	 	 		});
	 			return valids;
	 	 	}
	 	 	
	 	 	bool cb_is_valid_connection(int edgeId, int sourceId, int targetId) {
	 	 		var re = cb_can_connect_edge(edgeId,sourceId,targetId);
	 	 		return re is bool && re == true;
	 	 	}
	 	 	
	 	 	bool can_connect_source(core.Edge edge,core.Node source,core.Node target) {
	 	 		if(edge.source == null || edge.source.dywaId!=source.dywaId) {
		 			//source changed
		 			«FOR n:g.nodesTopologically.filter[!isIsAbstract] SEPARATOR "else "»
			 			if(source is «g.name.lowEscapeDart».«n.name.fuEscapeDart»){
			 				«FOR edge:g.edgesTopologically»
			 				if(edge == null || edge is «g.name.lowEscapeDart».«edge.name.fuEscapeDart»)
			 				{
				 				«IF !n.name.parentTypes(g).filter(Node).map[outgoingEdgeConnections].flatten.filter[containesEdge(edge)].empty»
				 					var outgoing = source.outgoing;
				 				«ENDIF»
				 				
				 				«FOR group:n.name.parentTypes(g).filter(Node).map[outgoingEdgeConnections].flatten.filter[containesEdge(edge)].indexed»
						 				int groupSize«group.key» = 0;
				 							«FOR incomingEdge:group.value.connectingEdges»
						 				groupSize«group.key» += outgoing.where((n)=>n is «g.name.lowEscapeDart».«incomingEdge.name.fuEscapeDart»).length;
				 							«ENDFOR»
				 							«IF group.value.connectingEdges.empty»
						 				groupSize«group.key» += outgoing.length;
				 							«ENDIF»
						 				if(«IF group.value.upperBound < 0»true«ELSE»groupSize«group.key»<«group.value.upperBound»«ENDIF»)
						 				{
						 					return true;
						 				}
				 				«ENDFOR»
			 				}
			 				«ENDFOR»
			 			}
		 			«ENDFOR»
		 		}
		 		return false;
	 	 	}
	 	 	
	 	 	bool can_connect_target(core.Edge edge,core.Node source,core.Node target) {
		 		if(edge.target == null || edge.target.dywaId!=target.dywaId){
		 			//target changed
		 			«FOR n:g.nodesTopologically.filter[!isIsAbstract] SEPARATOR "else "»
		 			if(target is «g.name.lowEscapeDart».«n.name.fuEscapeDart»){
		 				
		 				«FOR edge:g.edgesTopologically»
			 				if(edge == null || edge is «g.name.lowEscapeDart».«edge.name.fuEscapeDart»)
			 				{
		 					«IF !n.name.parentTypes(g).filter(Node).map[incomingEdgeConnections].flatten.filter[containesEdge(edge)].empty»
			 				var incoming = target.incoming;
			 				«ENDIF»
			 				«FOR group:n.name.parentTypes(g).filter(Node).map[incomingEdgeConnections].flatten.filter[containesEdge(edge)].indexed»
					 				int groupSize«group.key» = 0;
			 							«FOR outgoingEdge:group.value.connectingEdges»
					 				groupSize«group.key» += incoming.where((n)=>n is «g.name.lowEscapeDart».«outgoingEdge.name.fuEscapeDart»).length;
			 							«ENDFOR»
			 							«IF group.value.connectingEdges.nullOrEmpty»
					 				groupSize«group.key» += incoming.length;
			 							«ENDIF»
					 				if(«IF group.value.upperBound < 0»true«ELSE»groupSize«group.key»<«group.value.upperBound»«ENDIF»)
					 				{
					 					return true;
					 				}
			 				«ENDFOR»
			 				}
		 				«ENDFOR»
		 			}
		 			«ENDFOR»
		 		}
		 		return false;
	 	 	}
		 	
		 	dynamic cb_can_connect_edge(int dywaId,int sourceId,int targetId) {
		 	 	var edge = dywaId==-1?null:findElement(dywaId) as core.Edge;
		 		List<core.Edge> edgeTypes = dywaId==-1?[
		 			«FOR e:g.edgesTopologically.filter[!isAbstract] SEPARATOR ","»
		 			new «g.name.lowEscapeDart».«e.name.fuEscapeDart»()
		 			«ENDFOR»
		 		]:[findElement(dywaId)];
		 	 	
		 		var sourceElem = findElement(sourceId);
		 		var targetElem = findElement(targetId);
		 		if(sourceElem is! core.Node || targetElem is! core.Node) {
		 			return false;
		 		}
		 
		 		var source = sourceElem as core.Node;
		 		var target = targetElem as core.Node;
		 
		 		for(var e in edgeTypes)
		 		{
			 		var targetResult = can_connect_source(e,source,target);
			 		var sourceResult = can_connect_target(e,source,target);
			 		if(edge==null) {
			 			if(targetResult && sourceResult) {
			 				return true;
			 			}
			 		}
			 		else {
			 			if(targetResult || sourceResult) {
			 				return true;
			 			}
			 		}
		 		}
		 		if(edge == null) {
		 			return false;
		 		}
		 		var arr = js.JsArray();
		 		arr['target'] = edge.target.dywaId;
		 		arr['source'] = edge.source.dywaId;
		 		return arr;
		 	}
		 	
		 	void remove_node_cascade(core.Node node) {
		 		if(node == null){ return; }
		 		«FOR node : g.nodesTopologically.filter[!isIsAbstract]»
		 		else if(node is «g.name.lowEscapeDart».«node.name.fuEscapeDart») { remove_node_cascade_«node.name.lowEscapeDart»(node); }
		 		«ENDFOR»
		 	}
			 
			 //for each node
			 «FOR node : g.nodesTopologically.filter[!isIsAbstract]»
			 	void create_node_«node.name.lowEscapeDart»(«g.name.lowEscapeDart».«node.name.escapeDart» node) {
			 		var x = node.x;
			 		var y = node.y;
			 		
			 		if(node.container is core.Container) {
			 			core.ModelElementContainer parent = node.container;
			 			while(parent!=null&&parent is core.Container) {
			 				x += (parent as core.Container).x;
			 				y += (parent as core.Container).y;
			 				parent = (parent as core.Container).container;
			 			}
			 		}
			 	    js.context.callMethod('create_node_«node.name.lowEscapeDart»_«g.name.lowEscapeDart»',
			 	    [   «{
	 	    				val isElliptic = node.isElliptic(styles)
	 	    		    	'''
	 	    		    	x«IF isElliptic»+(node.width~/2)«ENDIF»,
	 	    		    	y«IF isElliptic»+(node.height~/2)«ENDIF»,'''
	 	    		    }»
			 	    	node.width,
			 	    	node.height,
			 	    	node.dywaId,
			 	    	node.container.dywaId,
			 	    	node.dywaName,
			 	    	node.dywaVersion,
			 	    	node.styleArgs(),
			 	    	node.$information(),
			 	    	node.$label()
			 	    	]);
			 	}
			 	
			 	void cb_create_node_«node.name.lowEscapeDart»(int x,int y,int width,int height,String cellId,int containerId«IF node.isPrime»,int primeId«ENDIF») {
			 		
			 		var container = findElement(containerId) as core.ModelElementContainer;
			 	    var ccm = commandGraph.sendCreateNodeCommand("«g.name.lowEscapeDart».«node.name.escapeDart»",«{
			 	    				val isElliptic = node.isElliptic(styles)
			 	    		    	'''
			 	    		    	x«IF isElliptic»-(width~/2)«ENDIF»,
			 	    		    	y«IF isElliptic»-(height~/2)«ENDIF»'''
			 	    		    }»,containerId,width,height,user«IF node.isPrime»,primeId:primeId«ENDIF»);
			 	    startPropagation();
			 	    graphService.sendMessage(ccm,"«g.name.lowEscapeDart»",currentGraphModel.dywaId).then((m){
		 	    		«'''
			 		   		var cmd = m.cmd.queue.first;
			 		   		if(cmd is CreateNodeCommand){
			 		   			var node = new «g.name.lowEscapeDart».«node.name.escapeDart»();
			 		   			node.x = x;
			 		   			node.y = y;
			 		   			node.dywaId = cmd.delegateId;
			 		   			node.dywaName = cmd.dywaName;
			 		   			node.dywaVersion = cmd.dywaVersion;
			 		   			node.container = container;
			 		   			container.modelElements.add(node);
			 		   			«IF node.prime»
									//prime node -> update element properties
									var primeElem = cmd.primeElement;
									var elements = currentGraphModel.allElements().where((n)=>n.dywaId==primeElem.dywaId);
									if(elements.isNotEmpty){
										node.«node.primeReference.name.escapeDart» = elements.first as «node.primeReference.type.graphModel.name.lowEscapeDart».«node.primeReference.type.name.fuEscapeDart»;
									} else {
										node.«node.primeReference.name.escapeDart» = primeElem as «node.primeReference.type.graphModel.name.lowEscapeDart».«node.primeReference.type.name.fuEscapeDart»;
									}
			 		   			«ENDIF»
			 		   			updateElement(node,cellId: cellId);
			 		   			commandGraph.receiveCommand(m.customCommands(),forceExecute: true);
			 		   			commandGraph.storeCommand(m.cmd);
			 		   			if(container is! core.GraphModel){
			 		   				updateElement(container as core.ModelElement);
			 		   			}
			 		   			selectionChangedSC.add(node);
			 		   		}
		 	    	  	'''.checkCommand("basic_valid_answer")»
			 	    }).whenComplete(()=>endPropagation());
			 	}
			 	
			 	void remove_node_cascade_«node.name.lowEscapeDart»(«g.name.lowEscapeDart».«node.name.fuEscapeDart» node) {
			 		«IF node instanceof NodeContainer»
			 		node.modelElements.where((n)=>n is core.Node).forEach((n)=>remove_node_cascade(n));
			 		«ENDIF»
			 		//remove connected edges
			 		node.outgoing.forEach((e) {
			 			e.target.incoming.remove(e);
			 			e.container.modelElements.remove(e);
			 			e.container = null;
			 			js.context.callMethod('remove_edge__«g.name.lowEscapeDart»', [e.dywaId]);
			 		});
			 		node.outgoing.clear();
			 		node.incoming.forEach((e) {
			 			e.source.outgoing.remove(e);
			 			e.container.modelElements.remove(e);
			 			e.container = null;
			 			js.context.callMethod('remove_edge__«g.name.lowEscapeDart»', [e.dywaId]);
			 		});
			 		node.incoming.clear();
			 		js.context.callMethod('remove_node_«node.name.lowEscapeDart»_«g.name.lowEscapeDart»', [node.dywaId]);
			 	}
			 	
			 	void cb_remove_node_«node.name.lowEscapeDart»(int dywaId) {
			 		var node = findElement(dywaId) as «g.name.lowEscapeDart».«node.name.fuEscapeDart»;
			 		var container = findElement(node.container.dywaId) as core.ModelElementContainer;
			 	    var ccm = commandGraph.sendRemoveNodeCommand(dywaId,user);
			 		startPropagation();
			 	    graphService.sendMessage(ccm,"«g.name.lowEscapeDart»",currentGraphModel.dywaId).then((m){
			 	    		selectionChangedSC.add(currentGraphModel);
«««			 	    		container.modelElements.remove(node);
			 	    		node.container = null;
			 	    		«'''
«««			 	    			remove_node_cascade_«node.name.lowEscapeDart»(node);
«««			 	    			commandGraph.receiveCommand(m.customCommands(),forceExecute: true);
			 	    			commandGraph.receiveCommand(m,forceExecute: true);
			 	    			commandGraph.storeCommand(m.cmd);
			 	    			if(container is! core.GraphModel){
			 	    			updateElement(container as core.ModelElement);
			 	    			}
		 	    	  		'''.checkCommand("basic_valid_answer")»
			 	    }).whenComplete(()=>endPropagation());
			 	}
			 	
			 	void cb_move_node_«node.name.lowEscapeDart»(int x,int y,int dywaId,containerId) {
			 		var container = findElement(containerId) as core.ModelElementContainer;
			 		var node = findElement(dywaId) as core.Node;
			 		if(node.x==x&&node.y==y){
			 			return;
			 		}
			 	    var ccm = commandGraph.sendMoveNodeCommand(dywaId,«{
			 	    			 	    				val isElliptic = node.isElliptic(styles)
			 	    			 	    		    	'''
			 	    			 	    		    	x«IF isElliptic»-(node.width~/2)«ENDIF»,
			 	    			 	    		    	y«IF isElliptic»-(node.height~/2)«ENDIF»'''
			 	    			 	    		    }»,containerId,user);
			 		startPropagation();
			 	    graphService.sendMessage(ccm,"«g.name.lowEscapeDart»",currentGraphModel.dywaId).then((m){
		 	    		«'''
			 	    		if(!container.modelElements.contains(node)){
			 	    			node.container.modelElements.remove(node);
			 	    			node.container = container;
			 	    			container.modelElements.add(node);
			 	    			if(container is! core.GraphModel){
			 	    				updateElement(container as core.ModelElement);
			 	    			}
			 	    		}
			 	    		node.x = x;
			 	    		node.y = y;
			 	    		updateElement(node);
			 	    		commandGraph.receiveCommand(m.customCommands(),forceExecute: true);
			 	    		commandGraph.storeCommand(m.cmd);
		 	    	  	'''.checkCommand("basic_valid_answer")»
			 	   }).whenComplete(()=>endPropagation());
			 	}
			 	
			 	void cb_resize_node_«node.name.lowEscapeDart»(int width,int height,String direction,int dywaId) {
			 		«IF node.resizable»
			 		var node = findElement(dywaId) as core.Node;
			 		if(node.width!=width||node.height!=height) {
				 	  startPropagation();
			 	      var ccm = commandGraph.sendResizeNodeCommand(dywaId,width,height,direction,user);
			 	      graphService.sendMessage(ccm,"«g.name.lowEscapeDart»",currentGraphModel.dywaId).then((m){
		 	    		«'''
			 	    		node.width = width;
			 	    		node.height = height;
			 	    		updateElement(node);
			 	    		commandGraph.receiveCommand(m.customCommands(),forceExecute: true);
			 	    		commandGraph.storeCommand(m.cmd);
		 	    	  	'''.checkCommand("basic_valid_answer")»
			 	     }).whenComplete(()=>endPropagation());
			 	   }
			 	   «ENDIF»
			 	}
			 	
			 	void cb_rotate_node_«node.name.lowEscapeDart»(int angle,int dywaId) {
			 		var node = findElement(dywaId) as core.Node;
			 	    var ccm =commandGraph.sendRotateNodeCommand(dywaId,angle,user);
			 		startPropagation();
			 	    graphService.sendMessage(ccm,"«g.name.lowEscapeDart»",currentGraphModel.dywaId).then((m){
		 	    		«'''
			 	    		node.angle = angle;
			 	    		updateElement(node);
			 	    		commandGraph.receiveCommand(m.customCommands(),forceExecute: true);
			 	    		commandGraph.storeCommand(m.cmd);
		 	    	  	'''.checkCommand("basic_valid_answer")»
			 	    }).whenComplete(()=>endPropagation());
			 	}
			 «ENDFOR»
			 // for each edge
			 «FOR edge : g.edgesTopologically.filter[!isIsAbstract]»
			 	void create_edge_«edge.name.lowEscapeDart»(«g.name.lowEscapeDart».«edge.name.escapeDart» edge) {
			 		
			 	    js.context.callMethod('create_edge_«edge.name.lowEscapeDart»_«g.name.lowEscapeDart»',
			 	    [ edge.source.dywaId,
			 	    edge.target.dywaId,
			 	    edge.dywaId,
			 	    edge.dywaName,
			 	    edge.dywaVersion,
			 	    new js.JsArray.from(edge.bendingPoints.map((n){
			 	    	     	var arr = new js.JsArray();
			 	    			arr['x'] = n.x;
			 	    			arr['y'] = n.y;
			 	    			return arr;
			 	    	     }).toList()),
			 	    edge.styleArgs(),edge.$information(),edge.$label() ]);
			 	}
			 	
			 	void cb_create_edge_«edge.name.lowEscapeDart»(int sourceId,int targetId,String cellId, List positions) {
			 		var source = findElement(sourceId) as core.Node;
			 		var target = findElement(targetId) as core.Node;
			 		var currentBendpoints = new List<core.BendingPoint>();
			 		if(positions!=null){
			 		  positions.forEach((p){
			 			var b = new core.BendingPoint();
			 			b.x = p['x'];
			 			b.y = p['y'];
			 			currentBendpoints.add(b);
			 		  });
			 		}
			 	    var ccm = commandGraph.sendCreateEdgeCommand("«g.name.lowEscapeDart».«edge.name.escapeDart»",targetId,sourceId,currentBendpoints,user);
			 		startPropagation();
			 	    graphService.sendMessage(ccm,"«g.name.lowEscapeDart»",currentGraphModel.dywaId).then((m){
			 			«'''
			 			var cmd = m.cmd.queue.first;
			 			if(cmd is CreateEdgeCommand){
			 				var edge = new «g.name.lowEscapeDart».«edge.name.escapeDart»();
			 				edge.dywaId = cmd.delegateId;
			 				edge.dywaVersion = cmd.dywaVersion;
			 				edge.dywaName = cmd.dywaName;
			 				edge.container = currentGraphModel;
			 				currentGraphModel.modelElements.add(edge);
			 				edge.source = source;
			 				source.outgoing.add(edge);
			 				edge.target = target;
			 				target.incoming.add(edge);
			 				edge.bendingPoints = new List.from(currentBendpoints);
			 				updateElement(edge,cellId: cellId);
			 				updateElement(source);
			 				updateElement(target);
			 				ccm.cmd.queue.first.delegateId=edge.dywaId;
			 				commandGraph.receiveCommand(m.customCommands(),forceExecute: true);
			 				commandGraph.storeCommand(m.cmd);
			 				selectionChangedSC.add(edge);
			 			}
			 			'''.checkCommand("basic_valid_answer")»
			 	    }).whenComplete(()=>endPropagation());
			 	}
			 	
			 	void cb_remove_edge_«edge.name.lowEscapeDart»(int dywaId) {
			 		var edge = findElement(dywaId) as core.Edge;
			 	    var ccm = commandGraph.sendRemoveEdgeCommand(dywaId,edge.source.dywaId,edge.target.dywaId,"«g.name.lowEscapeDart».«edge.name.escapeDart»",user);
			 		startPropagation();
			 	    graphService.sendMessage(ccm,"«g.name.lowEscapeDart»",currentGraphModel.dywaId).then((m){
			 	    		«'''
			 	    		selectionChangedSC.add(currentGraphModel);
			 	    		var source = edge.source;
			 	    		source.outgoing.remove(edge);
			 	    		updateElement(source);
			 	    		edge.source = null;
			 	    		var target = edge.target;
			 	    		target.incoming.remove(edge);
			 	    		updateElement(target);
			 	    		edge.target = null;
			 	    		edge.container.modelElements.remove(edge);
			 	    		edge.container = null;
			 	    		commandGraph.receiveCommand(m.customCommands(),forceExecute: true);
			 	    		commandGraph.storeCommand(m.cmd);
			 	    		'''.checkCommand("basic_valid_answer")»
			 		}).whenComplete(()=>endPropagation());
			 	}
			 	
			 	void cb_reconnect_edge_«edge.name.lowEscapeDart»(int sourceId,int targetId,int dywaId) {
			 		var edge = findElement(dywaId) as core.Edge;
			 		var source = findElement(sourceId) as core.Node;
			 		var target = findElement(targetId) as core.Node;
			 		if(edge.source.dywaId!=sourceId||edge.target.dywaId!=targetId) {
			 	    	var ccm = commandGraph.sendReconnectEdgeCommand(dywaId,sourceId,targetId,user);
				 		startPropagation();
			 	    	graphService.sendMessage(ccm,"«g.name.lowEscapeDart»",currentGraphModel.dywaId).then((m){
				 	    		«'''
			 	    			if(edge.target != target){
			 	    				edge.target.incoming.remove(edge);
			 	    				edge.target = target;
			 	    				target.incoming.add(edge);
			 	    				updateElement(target);
			 	    			}
			 	    			if(edge.source != source){
			 	    				edge.source.outgoing.remove(edge);
			 	    				edge.source = source;
			 	    				source.outgoing.add(edge);
			 	    				updateElement(source);
			 	    			}
			 	    			updateElement(edge);
			 	    			commandGraph.receiveCommand(m.customCommands(),forceExecute: true);
			 	    			commandGraph.storeCommand(m.cmd);
				 	    	  	'''.checkCommand("basic_valid_answer")»
			 	    	}).whenComplete(()=>endPropagation());
			 	    }
			 	}
			 	
		«ENDFOR»
		 	void cb_update_bendpoint(List positions,int dywaId) {
		 		var edge = findElement(dywaId) as core.Edge;
		 		//check if update is present
		 		if(check_bendpoint_update(edge,positions)) {
		 		var currentBendpoints = new List<core.BendingPoint>();
		 		positions.forEach((p){
		 			var b = new core.BendingPoint();
					b.x = p['x'];
					b.y = p['y'];
					currentBendpoints.add(b);
		 		});
		 	    var ccm = commandGraph.sendUpdateBendPointCommand(dywaId,currentBendpoints,new List.from(edge.bendingPoints),user);
		 		startPropagation();
		 	    graphService.sendMessage(ccm,"«g.name.lowEscapeDart»",currentGraphModel.dywaId).then((m){
		 	    	  	«'''
		 	    		edge.bendingPoints = new List();
		 	    		positions.forEach((p){
		 	    			var b = new core.BendingPoint();
		 	    			b.x = p['x'];
		 	    			b.y = p['y'];
		 	    			edge.bendingPoints.add(b);
		 	    		});
		 	    		commandGraph.receiveCommand(m.customCommands(),forceExecute: true);
		 	    		commandGraph.storeCommand(m.cmd);
		 	    	  	'''.checkCommand("basic_valid_answer")»
		 	      }).whenComplete(()=>endPropagation());
		 		}
		 	}
		
«««		  void prepareCommandGraph()
«««		  {
«««		    if(currentGraphModel != null) {
«««		      commandGraph = new «g.name.fuEscapeDart»CommandGraph(currentGraphModel,highlightings);
«««		      commandGraph.editorCommand.listen((c){
«««	            	if(c is OpenFileCommand) {
«««	            		Map m = new Map();
«««	            		m['graphmodel_id'] = c.dywaId;
«««	      	      	jumpToSC.add(m);
«««	            	}
«««	            });
«««		    }
«««		  }
		
«««		  /// action triggered by the server
«««		  /// after a valid command has been propagated
«««		  /// and the local business models has to be modified
«««		  /// if propagation is enabled, the canvas is updated as well
«««		  Node execCreateNodeType(String type)
«««		  {
«««		    Node newNode;
«««		    // for each node type
«««		    «FOR elem : g.nodesTopologically.filter[!isIsAbstract]»
«««		    	if(type == '«elem.name.fuEscapeDart»'){
«««		    	  newNode = new «g.name.lowEscapeDart».«elem.name.fuEscapeDart»();
«««		    	}
«««		    «ENDFOR»
«««		    return newNode;
«««		  }
«««		
«««		  Edge execCreateEdgeType(String type)
«««		  {
«««		    Edge edge;
«««		    «FOR elem : g.edgesTopologically.filter[!isIsAbstract]»
«««		    	if(type == '«elem.name.fuEscapeDart»')
«««		    	{
«««		    	  edge = new «g.name.lowEscapeDart».«elem.name.fuEscapeDart»();
«««		    	}
«««		    «ENDFOR»
«««		    return edge;
«««		
«««		  }
		
«««		  void sendCommand(CompoundCommandMessage ccm) {
«««		    hasChangedSC.add(ccm);
«««		  }
		}
		
	'''
	
	def boolean containesEdge(EdgeElementConnection c, Edge edge) {
		c.connectingEdges.contains(edge)||c.connectingEdges.nullOrEmpty
	}

	def checkCommand(CharSequence s,String type)
	{
		return checkCommand(s,type,true)
	}

	def checkCommand(CharSequence s,String type,boolean revert)
	'''
	if(m is CompoundCommandMessage){
		if(m.type=='«type»'){
			«s»
		}
		«IF revert»
		else {
			//revert
			commandGraph.revert(m);
		}
		«ENDIF»
	}
	'''

	def fileNameGraphModelComponentTemplate(String graphmodelName) '''«graphmodelName.lowEscapeDart»_component.html'''

	def contentGraphModelComponentTemplate(
		GraphModel g) '''
		<message-dialog
			*ngIf="showMessageDialog==true"
			[content]="dialogMessage"
			[dialog]="messageDialogType"
			(close)="closeMessgeDialog($event)"
		>
		</message-dialog>
		<display-dialog
			*ngIf="showDisplayDialog==true"
			[messages]="displayMessages"
			(close)="closeDisplayDialog($event)"
			(save)="saveDisplayDialog($event)"
			(download)="downloadDisplayDialog($event)"
		>
		</display-dialog>
		<div style="overflow: hidden;">
			<template [ngIf]="loading">
				<h3 style="text-align: center;">Loading  «g.name»..</h3>
				<div class="progress">
	    	    	<div class="progress-bar progress-bar-striped active" style="width: 100%;background-color: #be0101;"></div>
	    	    </div>
			</template>
			<div ondragover="confirm_drop_«g.name.lowEscapeDart»(event)" ondrop="drop_on_canvas_«g.name.lowEscapeDart»(event)" id="paper_«g.name.lowEscapeDart»" style="margin:auto" [ngClass]="{'pointer-events-none': !canEdit}"></div>
		</div>
	'''
	
	def containmemntCheck(ContainingElement container,GraphModel g,String suffix)
	{
	'''
		«IF container.containableElements.empty»
			«IF g.containableElements.equals(container)»
			return true;
			«ENDIF»
		«ELSE»
			«FOR group:container.containableElements.get(0).containingElement.containableElements.indexed»
	 			«{
	 				val containableTypes = group.value.getGroupContainables(g).toSet
	 				'''
	 				 «IF !containableTypes.empty»
						//check if type can be contained in group
						if(
	 					«FOR containableTypeName: containableTypes SEPARATOR "||"»
							node is «g.name.lowEscapeDart».«containableTypeName.name.fuEscapeDart»
	 					«ENDFOR»
						) {
	 					«IF group.value.upperBound>-1»
		 					int group«group.key»Size = 0;
		 					«FOR containableType:containableTypes»
		 						group«group.key»Size += container.modelElements.where((n)=>n is «g.name.lowEscapeDart».«containableType.name.fuEscapeDart»).length;
		 					«ENDFOR»
		 					if(«IF group.value.upperBound>-1»group«group.key»Size<«group.value.upperBound»«ELSE»true«ENDIF»){
		 					 	return true;
		 					}
	 					«ELSE»
							return true;
	 					«ENDIF»
						}
					«ENDIF»
	 				'''
	 			}»
	 			
			«ENDFOR»
			var arr_«suffix» = new js.JsArray();
			arr_«suffix»['x'] = x;
			arr_«suffix»['y'] = y;
			if(node.container != null) {
				arr_«suffix»['containerId'] = node.container.dywaId;
			}
			return arr_«suffix»;
		«ENDIF»
	'''
	}
}
