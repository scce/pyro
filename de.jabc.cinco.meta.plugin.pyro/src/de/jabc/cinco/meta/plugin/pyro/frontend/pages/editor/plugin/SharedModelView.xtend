package de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.plugin

import de.jabc.cinco.meta.plugin.pyro.util.EditorViewPlugin
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import de.jabc.cinco.meta.plugin.pyro.util.PluginComponent
import de.jabc.cinco.meta.plugin.pyro.util.EditorViewPluginRestController
import org.eclipse.emf.ecore.EClass

class SharedModelView extends EditorViewPlugin {
	
	private PluginComponent pc
	
	new(GeneratorCompound gc) {
		super(gc)
		pc = new PluginComponent
		pc.tab = "Shared"
		pc.key = "plugin_shared"
		pc.fetchURL = "sharedview/read/private"	
	}
	
	override getPluginComponent() {
		pc
	}
	
	override getRestController(){
		
	val rc = new EditorViewPluginRestController()
	rc.filename="SharedRestController.java"
	rc.content = '''
	package info.scce.pyro.plugin.controller;
	import de.ls5.dywa.generated.controller.info.scce.pyro.core.*;
	import de.ls5.dywa.generated.entity.info.scce.pyro.core.*;
	import javax.ws.rs.core.Response;
	import info.scce.pyro.plugin.rest.TreeViewRest;
	import info.scce.pyro.plugin.rest.TreeViewNodeRest;
	import java.util.Collections;
	import java.util.LinkedList;
	import java.util.List;
	import java.util.Optional;
	import java.util.stream.Collectors;
	
	@javax.transaction.Transactional
	@javax.ws.rs.Path("/sharedview")
	public class SharedRestController {
	
	    @javax.inject.Inject
	    private info.scce.pyro.rest.ObjectCache objectCache;
	    @javax.inject.Inject
	    private GraphModelController graphModelController;
	    @javax.inject.Inject
	    private PyroOrganizationController organizationController;
	    @javax.inject.Inject
	    private PyroFolderController folderController;
	    @javax.inject.Inject
	    private PyroProjectController projectController;
	    	    
	    	    
	    	    
	    @javax.ws.rs.GET
	    @javax.ws.rs.Path("read/private")
	    @javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	    @org.jboss.resteasy.annotations.GZIP
	    public Response load() {
	    	GraphModel searchGraph = graphModelController.createSearchObject("search_shared");
	    	searchGraph.setisPublic(true);
	    	TreeViewRest tvr = new TreeViewRest();
	    	tvr.setlayer(new LinkedList<>());
	        final java.util.List<GraphModel> list = graphModelController
	                .findByProperties(searchGraph);
	        //find all projects
	        List<TreeViewNodeRest> roots = new LinkedList<>();
	        for(GraphModel g:list) {
	        	//check for known organization
	        	PyroProject project = getProject(g);
	        	PyroOrganization org = getOrganization(project);
	        	//check root is known
	        	Optional<TreeViewNodeRest> optOrg = roots.stream().filter(n->n.getDywaId() == org.getDywaId()).findFirst();
	        	TreeViewNodeRest orgLevel = null;
	        	if(optOrg.isPresent()) {
	        		orgLevel = optOrg.get();
	        	} else {
	        		orgLevel = TreeViewNodeRest.fromDywaEntity(
						org
						,objectCache,
						org.getname(),
						null,
						"core.PyroOrganization",
						false,
						false,
						false,
						new LinkedList<>()
	        			
	        		);
	        		roots.add(orgLevel);
	        	}
	        	//check if project is known
	        	Optional<TreeViewNodeRest> optProject = orgLevel.getchildren().stream().filter(n->n.getDywaId() == project.getDywaId()).findFirst();
				TreeViewNodeRest projectLevel = null;
				if(optProject.isPresent()) {
					projectLevel = optProject.get();
				} else {
					projectLevel = TreeViewNodeRest.fromDywaEntity(
						project
						,objectCache,
						project.getname(),
						null,
						"core.PyroProject",
						false,
						false,
						false,
						new LinkedList<>()
					);
					orgLevel.getchildren().add(projectLevel);
				}
				«FOR g:gc.graphMopdels»
				if(g instanceof de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«g.name.fuEscapeJava») {
					de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«g.name.fuEscapeJava» gc = (de.ls5.dywa.generated.entity.info.scce.pyro.«g.name.fuEscapeJava».«g.name.fuEscapeJava»)g;
					
					projectLevel.getchildren().add(TreeViewNodeRest.fromDywaEntity(
					        gc
					        ,objectCache,
					        g.getfilename(),
					        null,
					        "«g.name.lowEscapeJava».«g.name.fuEscapeJava»",
					        false,
					        false,
					        true,
					        Collections.EMPTY_LIST
					));
				}
		        «ENDFOR»
	        }
	        
	        tvr.getlayer().addAll(roots);
	        return Response.ok(tvr).build();
	    }
	    
	    PyroProject getProject(GraphModel graph){
		    PyroFolder so = folderController.createSearchObject("search_folder");
		    so.setfiles_PyroFile(new LinkedList<>());
		    so.getfiles_PyroFile().add(graph);
		    java.util.List<PyroFolder> parents = folderController.findByProperties(so);
		    if(parents.isEmpty()){
		        throw new IllegalStateException("Graph without parent detected");
		    }
		    return getProject(parents.get(0));
		}
		
		PyroOrganization getOrganization(PyroProject project) {
			PyroOrganization so = organizationController.createSearchObject("search_organization");
		    so.setprojects_PyroProject(new LinkedList<>());
		    so.getprojects_PyroProject().add(project);
		    java.util.List<PyroOrganization> parents = organizationController.findByProperties(so);
		    if(parents.isEmpty()){
		        throw new IllegalStateException("Poject without parent detected");
		    }
		    return parents.get(0);
		}
	    
	    PyroProject getProject(PyroFolder folder){
		    if(folder instanceof PyroProject){
		        return (PyroProject) folder;
		    }
		    return getProject(getParent(folder));
		}
		
		PyroFolder getParent(PyroFolder folder) {
			PyroFolder so = folderController.createSearchObject("search_project");
		    so.setinnerFolders_PyroFolder(new LinkedList<>());
		    so.getinnerFolders_PyroFolder().add(folder);
		    java.util.List<PyroFolder> parents = folderController.findByProperties(so);
		    if(parents.isEmpty()){
		        throw new IllegalStateException("Folder without parent detected");
		    }
		    return parents.get(0);
		}
	
		
	}
	
	'''
	
	rc
	}
	
	
	
}