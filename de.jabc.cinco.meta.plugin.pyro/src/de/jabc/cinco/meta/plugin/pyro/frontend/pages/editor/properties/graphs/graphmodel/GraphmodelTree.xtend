package de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.properties.graphs.graphmodel

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import mgl.GraphModel
import mgl.ModelElement
import mgl.UserDefinedType

class GraphmodelTree extends Generatable {

	new(GeneratorCompound gc) {
		super(gc)
	}

	def fileNameGraphmodelTree(String graphModelName) '''«graphModelName.lowEscapeDart»_tree.dart'''

	def elementProperties(ModelElement gme, GraphModel g) '''
		class «gme.name.fuEscapeDart»TreeNode extends TreeNode {
			  String name;
			  
			
			  «gme.name.fuEscapeDart»TreeNode(IdentifiableElement root,«g.name.lowEscapeDart».«gme.name.fuEscapeDart» element,{String this.name,TreeNode parent})  : super(root)
			  {
			    if(name==null){
			      name = "«gme.name.fuEscapeDart»";
			    }
			    if(parent!=null){
			      super.parent = parent;
			    }
			    delegate = element;
			    «FOR attr : gme.attributesExtended.filter[!isPrimitive(g)].filter[!isModelElement(g)].filter[!hidden]»
			    	//complex «IF attr.isList»list «ENDIF»attributes
			    	if(element.«attr.name.escapeDart»!=null) {
			    	«IF attr.isList»
			    		children.add(new «gme.name.fuEscapeJava»«attr.name.fuEscapeDart»TreeListNode(root,element.«attr.name.escapeDart»,name: "«attr.name.escapeDart»",parent: this));
			    	«ELSE»
			    		children.add(new «attr.type.fuEscapeDart»TreeNode(root,element.«attr.name.escapeDart»,name: "«attr.name.escapeDart»",parent: this));
			    	«ENDIF»
			    	}
			    «ENDFOR»
			  }
			
			  «FOR attr : gme.attributesExtended.filter[!isPrimitive(g)].filter[!isModelElement(g)]»
			  	bool canRemove«attr.name.escapeDart»() {
			  		«IF attr.readOnly»
			  		return false;
			  		«ELSE»
				  		«IF attr.isList»
				  			return delegate.«attr.name.escapeDart».length > «attr.lowerBound»;
				  		«ELSE»
				  			return true;
				  		«ENDIF»
			  		«ENDIF»
			  		
			  	}
			«ENDFOR»
			@override
			TreeNode createChildren(String child) {
			  //for all complex not list attributes
			  «FOR attr : gme.attributesExtended.filter[!isPrimitive(g)].filter[!isModelElement(g)].filter[!isList]»
				  «FOR sub:attr.type.subTypesAndType(g).filter(ModelElement).filter[!isAbstract]»
				  	if(child == "«sub.name.fuEscapeDart»")
				  	{
				  	  print("«gme.name.fuEscapeDart» create children ${child}");
				  	  //create pyro element
				  	  var element = new «g.name.lowEscapeDart».«sub.name.fuEscapeDart»();
				  	  //create tree node
				  	  «attr.type.fuEscapeDart»TreeNode node = new «attr.type.fuEscapeDart»TreeNode(root,element,name:"«attr.name.escapeDart»",parent: this);
				  	  // update business model;
				  	  this.delegate.«attr.name.escapeDart» = element;
				  	  //add to tree
				  	  children.add(node);
				  	  return node;
				  	}
				  «ENDFOR»
			  «ENDFOR»
			  return null;
			  }
			
			  @override
			  List<String> getPossibleChildren() {
			    List<String> possibleElements = new List<String>();
			    //for all complex not list attributes
			    //check upper bound for single value
			    «FOR attr : gme.attributesExtended.filter[!isPrimitive(g)].filter[!isModelElement(g)].filter[!hidden].filter[!list]»
			    	«FOR sub:attr.type.subTypesAndType(g).filter(ModelElement).filter[!isAbstract]»
				    	«IF attr.isList»
				    		if(«IF attr.upperBound<=-1»true«ELSE»delegate.«attr.name.escapeDart».length < «attr.upperBound»«ENDIF»){
				    	«ELSE»
				    		if(delegate.«attr.name.escapeDart»==null){
				    	«ENDIF»
				    		//add type name
				    		possibleElements.add("«sub.name.fuEscapeDart»");
				    	}
			    	«ENDFOR»
			    «ENDFOR»
			    return possibleElements;
			  }
			  
				@override
				bool isChildRemovable(TreeNode node)
				{
					switch(node.name){
			«FOR attr : gme.attributesExtended.filter[!isPrimitive(g)].filter[!isModelElement(g)].filter[isList]»
					case '«attr.name.escapeDart»':return false;
		    «ENDFOR»
					}
				   return true;
				}
			
			  @override
			  bool isSelectable() => true;
			
			  @override
			  bool isRemovable() {
			    return canRemove();
			  }
			  @override
			  void removeAttribute(String name) {
			    //for each complex not list attribute
			    «FOR attr : gme.attributesExtended.filter[!isPrimitive(g)].filter[!isModelElement(g)].filter[!list]»
			    	if(name == '«attr.name.escapeDart»') {
			    	  delegate.«attr.name.escapeDart» = null;
			    	}
			    «ENDFOR»
			  }
			}
			«FOR attr:gme.attributesExtended.filter[!isPrimitive(g)].filter[!isModelElement(g)].filter[list]»
					class «gme.name.fuEscapeJava»«attr.name.fuEscapeDart»TreeListNode extends TreeNode {
						List<«g.name.lowEscapeDart».«attr.type.fuEscapeDart»> delegate;
						String name;
									
						«gme.name.fuEscapeJava»«attr.name.fuEscapeDart»TreeListNode(IdentifiableElement root,List<«g.name.lowEscapeDart».«attr.type.fuEscapeDart»> elements,{String this.name,TreeNode parent}) : super(root)
						{
						    if(name==null){
						      name = "«attr.name.fuEscapeDart»";
						    }
						    if(parent!=null){
						      super.parent = parent;
						    }
						    delegate = elements;
						    int i = 0;
							elements.forEach((n){
								children.add(new «attr.type.fuEscapeDart»TreeNode(root,n,name: "${i}",parent: this));
								i++;
							});
						}
						@override
						bool isSelectable() => false;
						@override
						bool isRemovable() {
						    return false;
						}
						@override
						bool canRemove() {
							return false;
						}
						@override
						TreeNode createChildren(String child) {
							«FOR sub:attr.type.subTypesAndType(g).filter(ModelElement).filter[!isAbstract]»
						  	if(child == "«sub.name.fuEscapeDart»")
						  	{
								var element = new «g.name.lowEscapeDart».«sub.name.fuEscapeDart»();
								
								//create tree node
								var node = new «attr.type.fuEscapeDart»TreeNode(root,element,name:"${children.length}",parent: this);
								delegate.add(element);
								children.add(node);
								return node;
							}
							«ENDFOR»
							return null;
						}
						@override
						List<String> getPossibleChildren() {
							List<String> possibleElements = new List<String>();
							if(«IF attr.upperBound>0»
							this.delegate.length < «attr.upperBound»
							«ELSE»
							true
							«ENDIF») {
								«FOR sub:attr.type.subTypesAndType(g).filter(ModelElement).filter[!isAbstract]»
								possibleElements.add("«sub.name.fuEscapeDart»");
								«ENDFOR»
							}
							return possibleElements;
						}
						@override
						bool isChildRemovable(TreeNode node) {
							«IF attr.lowerBound>0»
							return (this.delegate.length > «attr.lowerBound»);
							«ELSE»
							return true;
							«ENDIF»
						}
						@override
						void removeAttribute(String name) {
							try{
							  var idx = int.parse(name);
							  delegate.removeAt(idx);
							} catch(e) {
								delegate.removeWhere((n)=>n.dywaName==name);
							}
						}
					}
			«ENDFOR»
			
	'''

	def contentGraphmodelTree(GraphModel g) '''
		import 'package:«gc.projectName.escapeDart»/src/model/«g.name.lowEscapeDart».dart' as «g.name.lowEscapeDart»;
		import 'package:«gc.projectName.escapeDart»/src/model/core.dart';
		import 'package:«gc.projectName.escapeDart»/src/model/tree_view.dart';
		
		class «g.name.fuEscapeDart»TreeBuilder
		{
		
		  Tree getTree(IdentifiableElement element)
		  {
		    Tree tree = new Tree();
		    //for every complex attribute
		    //for every type
		    if(element!=null) {
		    	
		    	 //instanceofs
		    	 if(element is «g.name.lowEscapeDart».«g.name.fuEscapeDart»){
		    	     tree.root = new «g.name.fuEscapeDart»TreeNode(element,element);
		    	   }
		    	   «FOR elem : g.elements»
if(element.$type() == '«g.name.lowEscapeDart».«elem.name.fuEscapeDart»'){
	tree.root = new «elem.name.fuEscapeDart»TreeNode(element,element);
}
		    	 «ENDFOR»
		    }
		    return tree;
		  }
		}
		
		/// node, edge, container, graphmodel type
		«g.elementProperties(g)»
		
		«g.elementsAndTypes.map[elementProperties(g)].join("\n")»
		
	'''
}
