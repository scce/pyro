package de.jabc.cinco.meta.plugin.pyro.frontend.pages.editor.properties.property

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound

class PropertyComponent extends Generatable {
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def fileNamePropertyComponent()'''property_component.dart'''
	
	def contentPropertyComponent()
	'''
	import 'package:angular/angular.dart';
	import 'dart:async';
	
	import 'package:«gc.projectName.escapeDart»/src/model/core.dart';
	«FOR g:gc.graphMopdels»
	import 'package:«gc.projectName.escapeDart»/src/model/«g.name.lowEscapeDart».dart' as «g.name.lowEscapeDart»;
	import 'package:«gc.projectName.escapeDart»/src/pages/editor/properties/graphs/«g.name.lowEscapeDart»/property_component.dart' as «g.name.lowEscapeDart»Property;
	«ENDFOR»
	
	@Component(
	    selector: 'property',
	    templateUrl: 'property_component.html',
	    directives: const [
	    coreDirectives
	    «FOR g:gc.graphMopdels BEFORE "," SEPARATOR ","»
	      «g.name.lowEscapeDart»Property.PropertyComponent
	    «ENDFOR»
	      ]
	)
	class PropertyComponent {
	
	  @Input()
	  PyroElement currentElement;
	
	  @Input()
	  GraphModel currentGraph;
	
	  final hasChangedSC = new StreamController();
	  @Output() Stream get hasChanged => hasChangedSC.stream;
	 
	
	«FOR g:gc.graphMopdels»
	  /// checks if the given element belongs to
	  /// the «g.name.fuEscapeDart»
	  bool check«g.name.fuEscapeDart»(GraphModel element)
	  {
	    return element is «g.name.lowEscapeDart».«g.name.fuEscapeDart»;
	  }
	«ENDFOR»
	
	}
	'''
	
	def fileNamePropertyComponentTemplate()'''property_component.html'''
	
	def contentPropertyComponentTemplate()
	'''
	«FOR g:gc.graphMopdels»
	<«g.name.lowEscapeDart»
	    *ngIf="check«g.name.fuEscapeDart»(currentGraph)"
	    [currentElement]="currentElement"
	    [currentGraphModel]="currentGraph"
	    (hasChanged)="hasChangedSC.add($event)"
	></«g.name.lowEscapeDart»>
	«ENDFOR»
	'''
}