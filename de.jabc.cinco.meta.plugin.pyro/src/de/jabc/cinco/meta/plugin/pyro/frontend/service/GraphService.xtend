
package de.jabc.cinco.meta.plugin.pyro.frontend.service

import de.jabc.cinco.meta.plugin.pyro.util.Generatable
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import de.jabc.cinco.meta.core.utils.CincoUtil

class GraphService extends Generatable {
	
	new(GeneratorCompound gc) {
		super(gc)
	}
	
	def fileNameGraphServcie()'''graph_service.dart'''
	
	def contentGraphService()
	'''
	import 'dart:async';
	
	import 'package:angular_router/angular_router.dart';
	import 'package:«gc.projectName.escapeDart»/src/filesupport/fileuploader.dart';
	import '../model/core.dart';
	import '../model/message.dart';
	import '../model/command.dart';
	import 'package:«gc.projectName.escapeDart»/src/pages/main/routes.dart';
	import 'package:«gc.projectName.escapeDart»/src/pages/editor/canvas/canvas_component.dart';
	import 'base_service.dart';
	«FOR g:gc.ecores»
		import 'package:«gc.projectName.escapeDart»/src/model/«g.name.lowEscapeDart».dart' as «g.name.lowEscapeDart»;
	«ENDFOR»
	«FOR g:gc.graphMopdels»
		import 'package:«gc.projectName.escapeDart»/src/model/«g.name.lowEscapeDart».dart' as «g.name.lowEscapeDart»;
		import 'package:«gc.projectName.escapeDart»/src/pages/editor/canvas/graphs/«g.name.lowEscapeDart»/«g.name.lowEscapeDart»_command_graph.dart' as «g.name.lowEscapeDart»CG;
	«ENDFOR»
	
	import 'dart:html' as html;
	import 'dart:convert';
	
	class GraphService extends BaseService {
		
	  CanvasComponent canvasComponent;
		
	  GraphService(Router router) : super(router);
	  
	  Map<int,StreamController> graphModelUpdate = new Map();
	  
	  Stream register(int dywaId) {
	      graphModelUpdate[dywaId] = new StreamController();
	      return graphModelUpdate[dywaId].stream;
	  }
	  
	  void update(int dywaId) {
	      if(graphModelUpdate.containsKey(dywaId)) {
	        graphModelUpdate[dywaId].add({});
	      } else {
	        print("NO UPDATE");
	      }
	  }
		
	  
		
	  Future<Message> sendMessage(Message m,String graphModelType,int graphModelId) async{
	     print("[SEND] message ${m}");
	     return html.HttpRequest.request("${getBaseUrl()}/rest/${graphModelType}/message/${graphModelId.toString()}/private",sendData:m.toJSON(),method: "POST",requestHeaders: requestHeaders, withCredentials: true).then((response){
	       var p = Message.fromJSON(response.responseText);
	       print("[PYRO] send command ${p.messageType}");
	       return p;
	     }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	  }
	  
	  Future<Map> jumpToPrime(String graphModelType,String elementType,int graphModelId,int elementId) async{
	       print("[SEND] jump to prime message");
	       return html.HttpRequest.request("${getBaseUrl()}/rest/${graphModelType}/jumpto/${graphModelId.toString()}/${elementType}/${elementId.toString()}/private",method: "GET",requestHeaders: requestHeaders, withCredentials: true).then((response){
	       Map m = jsonDecode(response.responseText);
	       return m;
	     }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	  }
	
	  Future<PyroProject> loadProjectStructure(PyroProject project) async {
	    return html.HttpRequest.request("${getBaseUrl()}/rest/project/structure/${project.dywaId}/private",method: "GET",requestHeaders: requestHeaders, withCredentials: true).then((response){
	      var p = PyroProject.fromJSON(response.responseText);
	      print("[PYRO] load project ${p.name}");
	      return p;
	    }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	  }
	  
	  Future<dynamic> moveFolder(int folderId, int targetId) {
  		return html.HttpRequest.request("${getBaseUrl()}/rest/graph/move/folder/${folderId}/${targetId}/private", method: "POST",requestHeaders: requestHeaders, withCredentials: true).then((response){
	      print("[PYRO] folder moved");
	      return null;
	    }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	  }
	  
	  Future<dynamic> moveFile(int fileId, int targetId) {
  	  	return html.HttpRequest.request("${getBaseUrl()}/rest/pyrofile/move/${fileId}/${targetId}/private", method: "POST",requestHeaders: requestHeaders, withCredentials: true).then((response){
	        print("[PYRO] file moved");
	        return null;
        }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
  	  }
	  
	  Future<PyroProject> loadProjectStructureById(dywaId) async {
	      return html.HttpRequest.request("${getBaseUrl()}/rest/project/structure/${dywaId}/private",method: "GET",requestHeaders: requestHeaders, withCredentials: true).then((response){
	        var p = PyroProject.fromJSON(response.responseText);
	        print("[PYRO] load project ${p.name}");
	        return p;
	      }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	    }
	
	  Future<PyroFolder> createFolder(PyroFolder folder,dynamic parent) async {
	    var data = {
	      'parentId':parent.dywaId,
	      'name':folder.name
	    };
	    return html.HttpRequest.request("${getBaseUrl()}/rest/graph/create/folder/private",sendData:jsonEncode(data),method: "POST",requestHeaders: requestHeaders, withCredentials: true).then((response){
	      var newFolder = PyroFolder.fromJSON(response.responseText);
	      if(parent.innerFolders.where((f)=>f.dywaId==newFolder.dywaId).isEmpty) {
	         	parent.innerFolders.add(newFolder);
	      }
	      print("[PYRO] new folder ${folder.name} in folder ${parent.name}");
	      return newFolder;
	    }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	  }
	
	  Future<PyroFolder> updateFolder(PyroFolder folder) async {
	    var data = {
	      'dywaId':folder.dywaId,
	      'name':folder.name
	    };
	    return html.HttpRequest.request("${getBaseUrl()}/rest/graph/update/folder/private",sendData:jsonEncode(data),method: "POST",requestHeaders: requestHeaders, withCredentials: true).then((response){
	      print("[PYRO] update folder ${folder.name}");
	      return folder;
	    }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	  }
	
	  Future<Null> removeFolder(PyroFolder folder,PyroFolder parent) async {
	    return html.HttpRequest.request("${getBaseUrl()}/rest/graph/remove/folder/${folder.dywaId}/${parent.dywaId}/private", method: "GET",requestHeaders: requestHeaders, withCredentials: true).then((response){
	      print("[PYRO] remove folder ${folder.name}");
	      parent.innerFolders.remove(folder);
	    }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	  }
	  
	  Future<Null> removeFile(PyroFile file,PyroFolder parent) async {
	      return html.HttpRequest.request("${getBaseUrl()}/rest/pyrofile/remove/${file.dywaId}/${parent.dywaId}/private", method: "GET",requestHeaders: requestHeaders, withCredentials: true).then((response){
	        print("[PYRO] remove file ${file.filename}");
	        parent.files.remove(file);
	      }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	  }
	  
	  Future<PyroFile> updateFile(PyroFile file) async {
	        var data = {
	          'dywaId':file.dywaId,
	          'filename':file.filename,
	        };
	        return html.HttpRequest.request("${getBaseUrl()}/rest/pyrofile/update/file/private",sendData:jsonEncode(data),method: "POST",requestHeaders: requestHeaders, withCredentials: true).then((response){
	          print("[PYRO] update file ${file.filename}");
	          return file;
	        }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	    }
	  
	  Future<GraphModel> updateShareGraphModel(GraphModel g,bool newStatus) async {
		var data = {
		  'dywaId':g.dywaId,
		  'isPublic':newStatus,
		};
		return html.HttpRequest.request("${getBaseUrl()}/rest/graph/update/graphmodel/shared/private",sendData:jsonEncode(data),method: "POST",requestHeaders: requestHeaders, withCredentials: true).then((response){
		  print("[PYRO] update share status file ${g.filename}");
		  return g;
		}).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	  }
	
	  Future<void> removeGraph(GraphModel graph,PyroFolder parent) async {
	    await html.HttpRequest.request("${getBaseUrl()}/rest/${graph.$lower_type()}/remove/${graph.dywaId}/${parent.dywaId}/private", method: "GET",requestHeaders: requestHeaders, withCredentials: true).then((response){
	      print("[PYRO] remove graphmodel ${graph.filename}");
	      parent.files.remove(graph);
	    }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	  }
	  
	  Future<Null> generateGraph(GraphModel graph) async {
	      try {
	         html.window.open("${getBaseUrl()}/rest/${graph.$lower_type()}/generate/${graph.dywaId}/private",'file');
	         return new Future.value(null);
	      } catch(e) {
	         return new Future.error(null);
	      }
	  }
	
	  Future<GraphModel> updateGraphModel(GraphModel graph) async {
	    return html.HttpRequest.request("${getBaseUrl()}/rest/graph/update/graphmodel/private",sendData:jsonEncode(graph.toJSOG(new Map())),method: "POST",requestHeaders: requestHeaders, withCredentials: true).then((response){
	      print("[PYRO] update graphmodel ${graph.filename}");
	      return graph;
	    }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	  }
	  
	Future<PyroBinaryFile> createbinary(FileReference fr,PyroFolder parent) async {
	    var data = {
	        'parentId':parent.dywaId,
	        'file':fr.toJSOG(new Map())
	    };
	    return html.HttpRequest.request("${getBaseUrl()}/rest/pyrofile/create/binary/private",sendData:jsonEncode(data),method: "POST",requestHeaders: requestHeaders, withCredentials: true).then((response){
	        var newFile = PyroBinaryFile.fromJSOG(jsonDecode(response.responseText),new Map<String, dynamic>());
	        print("[PYRO] created PyroBinaryFile ${newFile.filename}");
	        if(parent.files.where((f)=>f.dywaId==newFile.dywaId).isEmpty) {
	          	parent.files.add(newFile);
	        }
	        return newFile;
	    }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	}
	
	Future<PyroBinaryFile> createblob(String name,String content,PyroFolder parent) async {
	    var data = {
	        'parentId':parent.dywaId,
	        'file':content,
	        'name':name
	    };
	    return html.HttpRequest.request("${getBaseUrl()}/rest/pyrofile/create/blob/private",sendData:jsonEncode(data),method: "POST",requestHeaders: requestHeaders, withCredentials: true).then((response){
	        var newFile = PyroBinaryFile.fromJSOG(jsonDecode(response.responseText),new Map<String, dynamic>());
	        print("[PYRO] created PyroBinaryFile ${newFile.filename}");
	        if(parent.files.where((f)=>f.dywaId==newFile.dywaId).isEmpty) {
	          	parent.files.add(newFile);
	        }
	        return newFile;
	    }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	}
	  
	Future<PyroTextualFile> createtextual(String filename,String extension,PyroFolder parent) async {
	    var data = {
	        'parentId':parent.dywaId,
	        'filename':filename,
	        'extension':extension
	    };
	    return html.HttpRequest.request("${getBaseUrl()}/rest/pyrofile/create/textual/private",sendData:jsonEncode(data),method: "POST",requestHeaders: requestHeaders, withCredentials: true).then((response){
	        var newFile = PyroTextualFile.fromJSOG(jsonDecode(response.responseText),new Map());
	        print("[PYRO] created PyroTextualFile ${newFile.filename}");
	        if(parent.files.where((f)=>f.dywaId==newFile.dywaId).isEmpty) {
	           	parent.files.add(newFile);
	        }
	        return newFile;
	    }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	  }
	  
	Future<PyroURLFile> createurl(String filename,String extension,String url,PyroFolder parent) async {
	      var data = {
	        'parentId':parent.dywaId,
	        'filename':filename,
	        'extension':extension,
	        'url':url
	      };
	      return html.HttpRequest.request("${getBaseUrl()}/rest/pyrofile/create/url/private",sendData:jsonEncode(data),method: "POST",requestHeaders: requestHeaders, withCredentials: true).then((response){
	        var newFile = PyroURLFile.fromJSOG(jsonDecode(response.responseText),new Map());
	        print("[PYRO] created PyroTURLFile ${newFile.filename}");
	        if(parent.files.where((f)=>f.dywaId==newFile.dywaId).isEmpty) {
	        	parent.files.add(newFile);
	        }
	        return newFile;
	      }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	}
	
	«FOR g:gc.ecores»
	Future<«g.name.lowEscapeDart».«g.name.fuEscapeDart»> create«g.name.escapeDart»(«g.name.lowEscapeDart».«g.name.fuEscapeDart» ecore,PyroFolder parent) async {
	    var data = {
	        'parentId':parent.dywaId,
	        'filename':ecore.filename
	    };
	    return html.HttpRequest.request("${getBaseUrl()}/rest/«g.name.lowEscapeDart»/create/private",sendData:jsonEncode(data),method: "POST",requestHeaders: requestHeaders, withCredentials: true).then((response){
	        var newEcore = «g.name.lowEscapeDart».«g.name.fuEscapeDart».fromJSOG(jsonDecode(response.responseText),new Map());
	        print("[PYRO] created «g.name.fuEscapeDart» ${ecore.filename}");
	        ecore.dywaId=newEcore.dywaId;
	        if(parent.files.where((f)=>f.dywaId==ecore.dywaId).isEmpty) {
	           parent.files.add(ecore);
	        }
	        return newEcore;
	    }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	  }
	«ENDFOR»
	
	«FOR g:gc.graphMopdels»
	«{
		val styles = CincoUtil.getStyles(g, gc.iProject)
		'''
		«IF g.hasAppearanceProvider(styles)»
		Future<String> appearances«g.name.fuEscapeDart»(«g.name.lowEscapeDart».«g.name.fuEscapeDart» graph) async{
		      return html.HttpRequest.request("${getBaseUrl()}/rest/«g.name.lowEscapeDart»/appearance/${graph.dywaId}/private",method: "GET",requestHeaders: requestHeaders, withCredentials: true).then((response){
		          print("[PYRO] load «g.name.lowEscapeDart» appearance proiveders ${graph.filename}");
		          return response.responseText;
		      }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
		  }
		«ENDIF»
		'''
	}»
	  Future<«g.name.lowEscapeDart».«g.name.fuEscapeDart»> create«g.name.escapeDart»(«g.name.lowEscapeDart».«g.name.fuEscapeDart» graph,PyroFolder parent) async {
	    var data = {
	        'parentId':parent.dywaId,
	        'filename':graph.filename
	    };
	    return html.HttpRequest.request("${getBaseUrl()}/rest/«g.name.lowEscapeDart»/create/private",sendData:jsonEncode(data),method: "POST",requestHeaders: requestHeaders, withCredentials: true).then((response){
	        var newGraph = «g.name.lowEscapeDart».«g.name.fuEscapeDart».fromJSOG(jsonDecode(response.responseText),new Map<String, dynamic>());
	        print("[PYRO] created «g.name.fuEscapeDart» ${graph.filename}");
	        graph.dywaId=newGraph.dywaId;
	        graph.merge(newGraph);
	        if(parent.files.where((f)=>f.dywaId==graph.dywaId).isEmpty) {
	           parent.files.add(graph);
	        }
	        return newGraph;
	    }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	  }
	  
	  Future<Map<String,String>> fetchCustomActionsFor«g.name.escapeDart»(int dywaId,«g.name.lowEscapeDart».«g.name.fuEscapeDart» graph) async {
	  	    return html.HttpRequest.request("${getBaseUrl()}/rest/«g.name.lowEscapeDart»/${graph.dywaId}/customaction/${dywaId}/fetch/private",method: "GET",requestHeaders: requestHeaders, withCredentials: true).then((response){
	  	        Map<String, dynamic> map = jsonDecode(response.responseText);
      	        Map<String, String> res = new Map();
      	        map.forEach((k,v){res[k] = v.toString();});	        
      	        print("[PYRO] fetched custom action for «g.name.fuEscapeDart» ${graph.filename}");
      	        return res;
	  	    }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	  }
	  Future<Message> triggerCustomActionsFor«g.name.escapeDart»(int dywaId,«g.name.lowEscapeDart».«g.name.fuEscapeDart» graph,String fqn,List<HighlightCommand> highlightings) async {
	  	  	var data = {
	  	  		'fqn':fqn,
	  	  		'highlightings':highlightings.map((n)=>n.toJSOG()).toList(),
	  	  		'dywaRuntimeType':'info.scce.pyro.core.command.types.Action'
	  	    };
	  	  	return html.HttpRequest.request("${getBaseUrl()}/rest/«g.name.lowEscapeDart»/${graph.dywaId}/customaction/${dywaId}/trigger/private",sendData:jsonEncode(data),method: "POST",requestHeaders: requestHeaders, withCredentials: true).then((response){
	  	  	   return Message.fromJSON(response.responseText);
	  	  	}).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	  }
	  
	   Future<Message> executeGraphmodelButton«g.name.escapeDart»(int dywaId,«g.name.lowEscapeDart».«g.name.fuEscapeDart» graph,String key,List<HighlightCommand> highlightings) async {
	   	 var data = {
   		  		'fqn':null,
   		  		'highlightings':highlightings.map((n)=>n.toJSOG()).toList(),
   		  		'dywaRuntimeType':'info.scce.pyro.core.command.types.Action'
   		  	  };
  	  	  	return html.HttpRequest.request("${getBaseUrl()}/rest/«g.name.lowEscapeDart»/${graph.dywaId}/button/${key}/${dywaId}/trigger/private",sendData:jsonEncode(data),method: "POST",requestHeaders: requestHeaders, withCredentials: true).then((response){
  	  	  	   return Message.fromJSON(response.responseText);
  	  	  	}).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
  	  }
	  
	  Future<Message> triggerDoubleClickActionsFor«g.name.escapeDart»(int dywaId,«g.name.lowEscapeDart».«g.name.fuEscapeDart» graph,List<HighlightCommand> highlightings) async {
	  	  var data = {
	  		'fqn':null,
	  		'highlightings':highlightings.map((n)=>n.toJSOG()).toList(),
	  		'dywaRuntimeType':'info.scce.pyro.core.command.types.Action'
	  	  };
	  	  return html.HttpRequest.request("${getBaseUrl()}/rest/«g.name.lowEscapeDart»/${graph.dywaId}/dbaction/${dywaId}/trigger/private",sendData:jsonEncode(data),method: "POST",requestHeaders: requestHeaders, withCredentials: true).then((response){
	  	      return Message.fromJSON(response.responseText);
	  	  }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	  }
	  
	  Future<Message> triggerPostSelectFor«g.name.escapeDart»(int dywaId,«g.name.lowEscapeDart».«g.name.fuEscapeDart» graph,String fqn,List<HighlightCommand> highlightings) async {
	  	  	  var data = {
	  	  		'fqn':fqn,
	  	  		'highlightings':highlightings.map((n)=>n.toJSOG()).toList(),
	  	  		'dywaRuntimeType':'info.scce.pyro.core.command.types.Action'
	  	  	  };
	  	  	  return html.HttpRequest.request("${getBaseUrl()}/rest/«g.name.lowEscapeDart»/${graph.dywaId}/psaction/${dywaId}/trigger/private",sendData:jsonEncode(data),method: "POST",requestHeaders: requestHeaders, withCredentials: true).then((response){
	  	  	      return Message.fromJSON(response.responseText);
	  	  	  }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	  }

	  
	  Future<«g.name.lowEscapeDart»CG.«g.name.fuEscapeDart»CommandGraph> loadCommandGraph«g.name.fuEscapeDart»(«g.name.lowEscapeDart».«g.name.fuEscapeDart» graph,List<HighlightCommand> highlightings) async{
	      return html.HttpRequest.request("${getBaseUrl()}/rest/«g.name.lowEscapeDart»/read/${graph.dywaId}/private",method: "GET",requestHeaders: requestHeaders, withCredentials: true).then((response){
	          var newGraph = «g.name.lowEscapeDart».«g.name.fuEscapeDart».fromJSOG(jsonDecode(response.responseText),new Map<String, dynamic>());
	          print("[PYRO] load «g.name.lowEscapeDart» ${newGraph.filename}");
	          graph.merge(newGraph);
	          var cg = new «g.name.lowEscapeDart»CG.«g.name.fuEscapeDart»CommandGraph(graph,highlightings);
	          return cg;
	      }).catchError(super.handleProgressEvent,test: (e) => e is html.ProgressEvent);
	  }
	«ENDFOR»
	
	}
	'''
	
}