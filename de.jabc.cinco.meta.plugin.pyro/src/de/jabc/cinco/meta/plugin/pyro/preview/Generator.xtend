package de.jabc.cinco.meta.plugin.pyro.preview

import de.jabc.cinco.meta.core.utils.CincoUtil
import de.jabc.cinco.meta.plugin.pyro.canvas.Shapes
import de.jabc.cinco.meta.plugin.pyro.util.FileGenerator
import de.jabc.cinco.meta.plugin.pyro.util.GeneratorCompound
import org.eclipse.core.resources.IProject
import org.eclipse.core.runtime.IPath

class Generator extends FileGenerator {
	
	new(IPath base) {
		super(base)
	}
	
	def generate(GeneratorCompound gc,IProject iProject) {
		//generate html file
		{
			//preview
			val path = "preview"
			val gen = new IndexHTML(gc)
			generateFile(path,
				gen.filename,
				gen.content
			)
		}
		//generate canvas model
		{
			//preview
			gc.graphMopdels.forEach[g|{
				val styles = CincoUtil.getStyles(g, iProject)
				val path = "preview/js"
				val gen = new Shapes(gc,g)
				generateFile(path,
					gen.fileNameShapes(g),
					gen.contentShapes(styles)
				)
			}]
		}
		
		//copy static resources
		de.jabc.cinco.meta.plugin.pyro.Generator.copyResources("frontend/app/web",basePath.append("/preview/vendor"))
	}
	
	
}