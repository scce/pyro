package de.jabc.cinco.meta.plugin.pyro.util

import de.jabc.cinco.meta.core.utils.CincoUtil
import de.jabc.cinco.meta.core.utils.PathValidator
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.util.Properties
import mgl.Annotation
import mgl.GraphModel
import org.apache.commons.io.FileUtils
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IProject
import org.eclipse.core.resources.IResource
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.FileLocator
import org.eclipse.core.runtime.Platform
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.osgi.framework.Bundle
import style.AbstractShape
import style.ConnectionDecorator
import style.ContainerShape
import style.EdgeStyle
import style.GraphicsAlgorithm
import style.Image
import style.NodeStyle
import style.Style
import style.Styles

class FileHandler {
	def static void copyResources(String bundleId, String target) {
		var Bundle b = Platform.getBundle(bundleId)
		try {
			var File source = FileLocator.getBundleFile(b)
			FileUtils.copyDirectoryToDirectory(new File('''«source.getAbsolutePath()»/resourcedywa/app-business'''),
				new File(target))
			FileUtils.copyDirectoryToDirectory(new File('''«source.getAbsolutePath()»/resourcedywa/app-presentation'''),
				new File(target))
			FileUtils.copyDirectoryToDirectory(new File('''«source.getAbsolutePath()»/resourcedywa/app-preconfig'''),
				new File(target))
			FileUtils.copyDirectoryToDirectory(new File('''«source.getAbsolutePath()»/resourcedywa/app-persistence'''),
				new File(target))
			FileUtils.copyFileToDirectory(new File('''«source.getAbsolutePath()»/resourcedywa/pom.xml'''),
				new File(target))
		} catch (IOException e) {
			e.printStackTrace()
		}

	}
	
	def static void copyClasse(GraphModel g,String path, IProject iProject, String pyroAppSourcePath) {
		val classPath = path.replaceAll("\\.","/")
		val folders = #["src","src-gen","xtend-gen","model-src-gen"]
		try {
			val String target = '''«pyroAppSourcePath»/«classPath.substring(0,classPath.lastIndexOf("/"))»'''
			val pathJava = '''/«classPath».java'''
			val pathXtend = '''/«classPath».xtend'''
			var found = false
			for(f:folders) {
				if(PathValidator.getURIForString(g, '''«f»/«pathXtend»''')!==null&&!found){
					copyFile(g,'''«f»/«pathXtend»''',target,false)
					found=true
				}
				if(PathValidator.getURIForString(g, '''«f»/«pathJava»''')!==null&&!found){
					copyFile(g,'''«f»/«pathJava»''',target,false)
					found=true
				}
				if(!found) {
				println('''[ERROR] could not find annotated file: «classPath»''')
			}
			}
		} catch (IOException e) {
			e.printStackTrace()
		}

	}
	
	
	
	def static Properties getPropertiesFile(productDefinition.Annotation annotation, IProject iProject) {
		val target = annotation.value.get(0)
		var String[] splitedName = target.split("/")
    	val String pathName = target.substring(splitedName.get(0).length+1)
           
			try {
				if(!pathName.isEmpty){
					val p = PathValidator.getURIForString(annotation, pathName)
					var IFile ir = (ResourcesPlugin.getWorkspace().getRoot().findMember(
					p.toPlatformString(true)) as IFile)
					val f = new File(ir.getRawLocation().toOSString())
					val prop = new Properties();
					val stream = new FileInputStream(f);
					prop.load(stream);
					return prop
				}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null
	}
	
	def static void copyAnnotatedClasses(Annotation annotation, IProject iProject, String pyroAppSourcePath) {
		val classPath = annotation.value.get(0).replaceAll("\\.","/")
		val folders = #["src","src-gen","xtend-gen","model-src-gen"]
		try {
			val String target = '''«pyroAppSourcePath»/«classPath.substring(0,classPath.lastIndexOf("/"))»'''
			val pathJava = '''/«classPath».java'''
			val pathXtend = '''/«classPath».xtend'''
			var found = false
			for(f:folders) {
				if(PathValidator.getURIForString(annotation, '''«f»/«pathXtend»''')!==null&&!found){
					copyFile(annotation,'''«f»/«pathXtend»''',target,true)
					found=true
				}
				if(PathValidator.getURIForString(annotation, '''«f»/«pathJava»''')!==null&&!found){
					copyFile(annotation,'''«f»/«pathJava»''',target,true)
					found=true
				}
				if(!found) {
				println('''[ERROR] could not find annotated file: «classPath»''')
			}
			}
		} catch (IOException e) {
			e.printStackTrace()
		}

	}
	
	def static void copyAnnotatedClasses(productDefinition.Annotation annotation, IProject iProject, String pyroAppSourcePath) {
		val classPath = annotation.value.get(0).replaceAll("\\.","/")
		val folders = #["src","src-gen","xtend-gen","model-src-gen"]
		try {
			val String target = '''«pyroAppSourcePath»/«classPath.substring(0,classPath.lastIndexOf("/"))»'''
			val pathJava = '''/«classPath».java'''
			val pathXtend = '''/«classPath».xtend'''
			var found = false
			for(f:folders) {
				if(PathValidator.getURIForString(annotation, '''«f»/«pathXtend»''')!==null&&!found){
					copyFile(annotation,'''«f»/«pathXtend»''',target,true)
					found=true
				}
				if(PathValidator.getURIForString(annotation, '''«f»/«pathJava»''')!==null&&!found){
					copyFile(annotation,'''«f»/«pathJava»''',target,true)
					found=true
				}
				if(!found) {
				println('''[ERROR] could not find annotated file: «classPath»''')
			}
			}
		} catch (IOException e) {
			e.printStackTrace()
		}
	}
	
	def static void copyAppearanceProviderClasses(Style style, IProject iProject, String pyroAppSourcePath) {
		val classPath = style.appearanceProvider.substring(1,style.appearanceProvider.length-1).replaceAll("\\.","/")
		val folders = #["src","src-gen","xtend-gen","model-src-gen"]
		try {
			val String target = '''«pyroAppSourcePath»/«classPath.substring(0,classPath.lastIndexOf("/"))»'''
			val javaPath = '''/«classPath».java'''
			val xtendPath = '''/«classPath».xtend'''
			var found = false
			for(f:folders) {
				if(PathValidator.getURIForString(style, '''«f»/«xtendPath»''')!==null&&!found){
					copyFile(style,'''«f»/«xtendPath»''',target,false)
					found=true
				}
				if(PathValidator.getURIForString(style, '''«f»/«javaPath»''')!==null&&!found){
					copyFile(style,'''«f»/«javaPath»''',target,false)
					found=true
				}
			}
			if(!found) {
				println('''[ERROR] could not find appearance provider file: «classPath»''')
			}
		} catch (IOException e) {
			e.printStackTrace()
		}

	}

	def static void copyImages(GraphModel graphModel, String resourcePath, IProject iProject, String imgBasePath) {
		try {
			var String path = '''«resourcePath»«imgBasePath»«graphModel.getName().toLowerCase()»/'''
			var Styles styles = CincoUtil.getStyles(graphModel, iProject)
			for (Style style : styles.getStyles()) {
				if (style instanceof NodeStyle) {
					copyImage(((style as NodeStyle)).getMainShape(), path)
				} else if (style instanceof EdgeStyle) {
					for (ConnectionDecorator connectionDecorator : ((style as EdgeStyle)).getDecorator()) {
						copyImage(connectionDecorator.getDecoratorShape(), path)
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace()
		}

	}

	def private static void copyImage(AbstractShape shape, String target) throws IOException {
		if (shape instanceof Image) {
			copyImageFile((shape as Image), target)
		} else if (shape instanceof ContainerShape) {
			for (AbstractShape abstractShape : ((shape as ContainerShape)).getChildren()) {
				if (abstractShape instanceof Image) {
					copyImageFile((abstractShape as Image), target)
				}
			}
		}
	}

	def private static void copyImage(GraphicsAlgorithm shape, String target) throws IOException {
		if (shape instanceof Image) {
			copyImageFile((shape as Image), target)
		}
	}

	def private static void copyImageFile(Image image, String target) throws IOException {
		copyFile(image,image.path,target,true)
	}
	
	def static void copyClasses(EObject res, String resourcePath, String targetPath) {
		try {
			val f = new File(resourcePath)
			for (file:f.list) {
				copyFile(res,file,targetPath,true)
			}
		} catch (IOException e) {
			e.printStackTrace()
		}

	}
	
	def static void copyFile(EObject res,String path, String target,boolean replaceExisiting) throws IOException {
			copyFile(res,path,target,replaceExisiting,null)
	}
	
	
	def static void copyFile(EObject res,String path, String target,boolean replaceExisiting,String newName) throws IOException {
		var URI uriForString = PathValidator.getURIForString(res, path)
		var IFile ir = (ResourcesPlugin.getWorkspace().getRoot().findMember(
			uriForString.toPlatformString(true)) as IFile)
		var File targetFolder = new File(target)
		val targetFile = if(newName===null) {
			new File(target+"/"+new File(ir.getRawLocation().toOSString()).name)
		} else {
			new File(target+"/"+newName)
		}
		if(targetFile.exists&&!replaceExisiting){
			return
		}
		targetFolder.mkdirs()
		try {
			FileUtils.copyFileToDirectory(new File(ir.getRawLocation().toOSString()), targetFolder)
		} catch(IOException e) {
			e.printStackTrace
		}
		if(newName !== null && !targetFile.exists) {
			FileUtils.moveFile(new File(target+"/"+new File(ir.getRawLocation().toOSString()).name),targetFile)
		}
	}
	
	def static getFileOrFolder(EObject res,String path) {
		var URI uriForString = PathValidator.getURIForString(res, path)
		var IResource ir = (ResourcesPlugin.getWorkspace().getRoot().findMember(uriForString.toPlatformString(true)) as IResource)
		new File(ir.getRawLocation().toOSString())
	}
	
	def static getAllFiles(EObject res,String path) {
		val root = getFileOrFolder(res,path)
		root.collectFiles
	}
	
	def private static Iterable<File> collectFiles(File file) {
		if(file.isDirectory) {
			return file.listFiles.map[collectFiles].flatten
		}
		return #[file]
	}
	
	def static void copyFileOrFolder(EObject res,String path, String target) {
		var source = getFileOrFolder(res,path)
		if(source.isDirectory) {
			FileUtils.copyDirectoryToDirectory(source,new File(target))							
		} else {
			FileUtils.copyFile(source,new File(target))
		}
	}
	
	
}
